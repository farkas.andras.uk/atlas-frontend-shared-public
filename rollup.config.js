import commonjs from '@rollup/plugin-commonjs';
import resolve from '@rollup/plugin-node-resolve';
import peerDepsExternal from 'rollup-plugin-peer-deps-external';
import typescript from 'rollup-plugin-typescript2';
import image from '@rollup/plugin-image';
import json from '@rollup/plugin-json';
import styles from 'rollup-plugin-import-css';

import packageJson from './package.json' assert { type: 'json' };

export default {
  input: './src/index.ts',
  makeAbsoluteExternalsRelative: true,
  preserveEntrySignatures: 'strict',
  output: [
    {
      esModule: false,
      generatedCode: {
        reservedNamesAsProps: false,
      },
      interop: 'compat',
      systemNullSetters: false,
      file: packageJson.main,
      format: 'cjs',
      sourcemap: true,
      inlineDynamicImports: true,
    },
    {
      esModule: true,
      generatedCode: {
        reservedNamesAsProps: false,
      },
      interop: 'compat',
      systemNullSetters: false,
      file: packageJson.module,
      format: 'esm',
      sourcemap: true,
      inlineDynamicImports: true,
    },
  ],
  plugins: [
    peerDepsExternal(),
    resolve(),
    commonjs(),
    typescript({
      tsconfigOverride: {
        exclude: ['node_modules', 'build', 'src/**/*.stories.tsx', 'src/**/*.test.tsx'],
      },
    }),
    image(),
    json(),
    styles(),
  ],
};
