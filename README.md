To develop

    Install

        1. At first install run this: vsts-npm-auth -config .npmrc
        2. yarn
        3. yarn storybook

    Publish Beta
        Always use this if develop a new feature or fix something and first use it in a project as test, and featre branch is not approved and merged to main.

        1. yarn build
        2. stage all modified files
        3. yarn publish --tag beta
        4. To current version type: {version}-beta.{betaVersion}, e.g.: 1.0.61-beta.0 (if increase beta version, increase just 0 to 1 and so on...)

    Publish
        Use this just on main branch, if beta test was successfully and code was approved and merged to main.

        1. yarn build
        2. stage all modified files
        3. yarn publish (publish new version of package and commit with version number in message like: v1.0.61)

To use

    Storybook

        Here you can see all storybook for each components: https://zltdevsharedfrontend.z16.web.core.windows.net/?path=/story/htmleditor--default

    Install

        1. At first install run this: vsts-npm-auth -config .npmrc
        2. yarn add atlas-frontend-shared
