import React from 'react';
import { Router } from 'react-router-dom';
import { combineReducers, configureStore } from '@reduxjs/toolkit';
import { Provider } from 'react-redux';
import { createBrowserHistory } from 'history';
import { ThemeProvider, StyledEngineProvider } from '@mui/material/styles';
import { LocalizationProvider } from '@mui/x-date-pickers';
import { AdapterMoment } from '@mui/x-date-pickers/AdapterMoment';
import 'moment/locale/hu';
import { theme } from '../src/theme';
import { commonReducer } from '../src/store';

export const rootReducer = combineReducers({
  common: commonReducer,
});

const store = configureStore({
  reducer: rootReducer,
});

const history = createBrowserHistory();

export const parameters = {
  actions: { argTypesRegex: '^on[A-Z].*' },
  controls: { expanded: true },
  layout: 'fullscreen',
};

export const decorators = [
  (Story) => (
    <Provider store={store}>
      <StyledEngineProvider injectFirst>
        <ThemeProvider theme={theme}>
          <LocalizationProvider dateAdapter={AdapterMoment} adapterLocale="hu">
            <Router history={history}>
              <Story />
            </Router>
          </LocalizationProvider>
        </ThemeProvider>
      </StyledEngineProvider>
    </Provider>
  ),
];
