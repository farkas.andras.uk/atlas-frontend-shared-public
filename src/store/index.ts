import { CommonState } from './common';

declare global {
  type SharedApplicationState = { common: CommonState };
}

export * from './common';
