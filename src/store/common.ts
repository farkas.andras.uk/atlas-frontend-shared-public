import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import uniqueId from 'lodash/uniqueId';

import { NotificationContent, NotificationModel, NotificationType, QueryParamModel } from '../models';

export interface SideBarState {
  open: boolean;
}

export interface SubDrawerState {
  open: string | null;
}

export interface LoaderState {
  show: boolean;
}

export interface CommonState {
  loader: LoaderState;
  notification: NotificationModel | null;
  sideBar: SideBarState;
  subDrawer: SubDrawerState;
  filterParams: QueryParamModel;
}

const commonSlice = createSlice({
  name: 'common',
  initialState: {
    loader: {
      show: false,
    },
    notification: null,
    sideBar: {
      open: localStorage.getItem('sideBarOpen') !== 'false',
    },
    subDrawer: {
      open: null,
    },
    filterParams: {},
  } as CommonState,
  reducers: {
    showLoader: (state) => {
      state.loader.show = true;
    },
    hideLoader: (state) => {
      state.loader.show = false;
    },
    showNotification: (
      state,
      action: PayloadAction<{
        content: NotificationContent;
        type: NotificationType;
      }>
    ) => {
      state.notification = {
        id: uniqueId('notification-'),
        content: action.payload.content,
        type: action.payload.type,
      };
    },
    removeNotification: (state) => {
      state.notification = null;
    },
    toggleSidebar: (state) => {
      const open = !state.sideBar.open;

      state.sideBar = {
        ...state.sideBar,
        open,
      };

      localStorage.setItem('sideBarOpen', String(open));
    },
    closeToggleSidebar: (state) => {
      const open = false;
      state.sideBar = {
        ...state.sideBar,
        open,
      };

      localStorage.setItem('sideBarOpen', String(open));
    },
    setFilterParams: (state, action: PayloadAction<QueryParamModel>) => {
      state.filterParams = action.payload;
    },
    setSubDrawer: (state, action: PayloadAction<string | null>) => {
      state.subDrawer.open = action.payload;
    },
  },
});

const { reducer, actions } = commonSlice;

export const {
  showLoader,
  hideLoader,
  showNotification,
  removeNotification,
  toggleSidebar,
  closeToggleSidebar,
  setFilterParams,
  setSubDrawer,
} = actions;

export const commonReducer = reducer;

export const selectNotification = (state: SharedApplicationState) => state.common.notification;

export const selectSideBarState = (state: SharedApplicationState) => state.common.sideBar;

export const selectFilterParams = (state: SharedApplicationState) => state.common.filterParams;

export const selectIsLoading = (state: SharedApplicationState) => state.common.loader.show;

export const selectSubDrawerState = (state: SharedApplicationState) => state.common.subDrawer;
