import { FieldInputProps, FormikProps } from 'formik';
import get from 'lodash/get';

type FieldStatusFunction<R> = (form?: FormikProps<any> | null, field?: FieldInputProps<any> | null) => R;

export const isSubmitted: FieldStatusFunction<boolean> = (form) => {
  return Boolean(form && form.submitCount);
};

export const isTouched: FieldStatusFunction<boolean> = (form, field) => {
  return Boolean(form && field && get(form.touched, field.name));
};

export const getErrorMessage: FieldStatusFunction<string> = (form, field) => {
  return form && field ? (get(form.errors, field.name) as string) || '' : '';
};

export const hasErrorMessage: FieldStatusFunction<boolean> = (form, field) => {
  return Boolean(getErrorMessage(form, field));
};

export const inErrorState: FieldStatusFunction<boolean> = (form, field) => {
  const shouldShowErrorMessage = isTouched(form, field) || isSubmitted(form);

  return shouldShowErrorMessage && hasErrorMessage(form, field);
};
