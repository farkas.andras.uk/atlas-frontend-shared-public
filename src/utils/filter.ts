import qs from 'query-string';

export const getFilterValue = (filter?: string[] | null) => {
  return Array.isArray(filter) ? (Object.values(qs.parse(filter[0]))[0] as string) : '';
};
