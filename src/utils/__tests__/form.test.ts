import { isSubmitted, isTouched, getErrorMessage, hasErrorMessage, inErrorState } from '../form';

describe('isSubmitted function', () => {
  test('decide whether the form is submitted', () => {
    expect(isSubmitted({ submitCount: 0 } as any)).toEqual(false);
    expect(isSubmitted({ submitCount: 1 } as any)).toEqual(true);
    expect(isSubmitted()).toEqual(false);
  });

  test('decide whether the field is touched', () => {
    expect(isTouched()).toEqual(false);
    expect(isTouched({} as any)).toEqual(false);

    expect(isTouched({ touched: { test: true } } as any, { name: 'test' } as any)).toEqual(true);
    expect(isTouched({ touched: { test1: true } } as any, { name: 'test' } as any)).toEqual(false);
  });

  test('get the error message associated to the field', () => {
    expect(getErrorMessage()).toEqual('');
    expect(getErrorMessage({} as any)).toEqual('');

    expect(getErrorMessage({ errors: { test: 'Error' } } as any, { name: 'test' } as any)).toEqual('Error');
    expect(getErrorMessage({ errors: { test1: 'Error' } } as any, { name: 'test' } as any)).toEqual('');
  });

  test('decide whether the field has error message', () => {
    expect(hasErrorMessage()).toEqual(false);
    expect(hasErrorMessage({} as any)).toEqual(false);

    expect(hasErrorMessage({ errors: { test: 'Error' } } as any, { name: 'test' } as any)).toEqual(true);
    expect(hasErrorMessage({ errors: { test1: 'Error' } } as any, { name: 'test' } as any)).toEqual(false);
  });

  test('decide whether the field is in error state', () => {
    expect(
      inErrorState({ errors: { test: 'Error' }, touched: { test: true } } as any, { name: 'test' } as any)
    ).toEqual(true);
    expect(inErrorState({ errors: { test: 'Error' }, submitCount: 1 } as any, { name: 'test' } as any)).toEqual(true);
    expect(inErrorState({ errors: { test: 'Error' }, submitCount: 0 } as any, { name: 'test' } as any)).toEqual(false);
    expect(
      inErrorState({ errors: { test: 'Error' }, touched: { test1: true } } as any, { name: 'test' } as any)
    ).toEqual(false);
  });
});
