import { compose } from '../functions';

describe('compose', () => {
  test('composes from right to left', () => {
    const double = (x: number) => x * 2;
    const square = (x: number) => x * x;

    expect(compose(square)(5)).toBe(25);
    expect(compose(square, double)(5)).toBe(100);
    expect(compose(double, square, double)(5)).toBe(200);

    expect(compose()(5)).toBe(5);
  });
});
