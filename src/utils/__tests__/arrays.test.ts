import { removeDuplicates, getDifferenceBy, arrayFilter, getPage, arraySort, arrayify, removeEmpty } from '../arrays';
import { SortDirection } from '../../models';

describe('removeDuplicates', () => {
  test('remove duplicates from array of objects by a given key', () => {
    const array = [{ name: 'Test' }, { name: 'Test2' }, { name: 'Test' }];

    expect(removeDuplicates(array, 'name')).toEqual([{ name: 'Test' }, { name: 'Test2' }]);
  });
});

describe('getDifferenceBy', () => {
  test('extracts the second array values from the first one by a given key', () => {
    const array1 = [
      { id: 1, name: '1' },
      { id: 2, name: '2' },
      { id: 3, name: '3' },
    ];
    const array2 = [
      { id: 1, name: '1' },
      { id: 4, name: '4' },
      { id: 5, name: '5' },
    ];

    expect(getDifferenceBy(array1, array2, 'id')).toEqual([
      { id: 2, name: '2' },
      { id: 3, name: '3' },
    ]);
  });
});

describe('arrayFilter', () => {
  const array = [
    { name: 'Test User', email: 'test@user.com' },
    { name: 'Production User', email: 'production@user.com' },
    { name: 'Developer user', email: 'developer@user.com' },
  ];

  test('filters the vales of an array of object by term for given keys', () => {
    expect(arrayFilter(array, ['name', 'email'], 'test')).toEqual([{ name: 'Test User', email: 'test@user.com' }]);
    expect(arrayFilter(array, ['name', 'email'], 'staging')).toEqual([]);
    expect(arrayFilter(array, ['name', 'email'])).toEqual([
      { name: 'Test User', email: 'test@user.com' },
      { name: 'Production User', email: 'production@user.com' },
      { name: 'Developer user', email: 'developer@user.com' },
    ]);
  });

  test('filters the vales of an array of object by term for given keys', () => {
    expect(arrayFilter(array, ['id'] as any, 'test')).toEqual([]);
  });
});

describe('getPage', () => {
  jest.mock('config', () => {
    return {
      ROWS_PER_PAGE_OPTIONS: [10, 25, 50],
    };
  });

  test('gets a slice from an array by a page size and a page number', () => {
    const array = Array.from({ length: 100 }).map((_, i) => i + 1);

    expect(getPage(array, 1, 5)).toEqual([1, 2, 3, 4, 5]);
    expect(getPage(array, 10, 100)).toEqual([]);
    expect(getPage(array, 5, 5).length).toEqual(10);
    expect(getPage(array, 2, 3).length).toEqual(10);
  });
});

describe('arraySort', () => {
  test('sorts an array of objects by a key in a direction', () => {
    const numberArray = [
      { id: 1, name: '1' },
      { id: 1, name: '1' },
      { id: 4, name: '4' },
      { id: 3, name: '3' },
      { id: 2, name: '2' },
      { id: 5, name: '5' },
    ];

    expect(arraySort(numberArray, 'id', SortDirection.ASC)).toEqual([
      { id: 1, name: '1' },
      { id: 1, name: '1' },
      { id: 2, name: '2' },
      { id: 3, name: '3' },
      { id: 4, name: '4' },
      { id: 5, name: '5' },
    ]);

    expect(arraySort(numberArray, 'id', SortDirection.DESC)).toEqual([
      { id: 5, name: '5' },
      { id: 4, name: '4' },
      { id: 3, name: '3' },
      { id: 2, name: '2' },
      { id: 1, name: '1' },
      { id: 1, name: '1' },
    ]);

    const userArray = [
      { id: 1, name: 'User' },
      { id: 4, name: 'Developer' },
      { id: 3, name: 'Admin' },
    ];

    expect(arraySort(userArray, 'name', SortDirection.ASC)).toEqual([
      { id: 3, name: 'Admin' },
      { id: 4, name: 'Developer' },
      { id: 1, name: 'User' },
    ]);
  });
});

describe('arrayify', () => {
  test('returns the array if given one', () => {
    const array = [{ name: 'Test' }, { name: 'Test2' }, { name: 'Test' }];

    expect(arrayify(array)).toEqual(array);
  });

  test('returns an empty array is falsy value is given', () => {
    expect(arrayify()).toEqual([]);
    expect(arrayify('')).toEqual([]);
    expect(arrayify(null)).toEqual([]);
  });

  test('returns an array with the single value if a single value is given', () => {
    expect(arrayify('one')).toEqual(['one']);
  });
});

describe('removeEmpty', () => {
  test('removes falsy values from array', () => {
    expect(removeEmpty([null, '', false, 'item'])).toEqual(['item']);
  });

  test('return an array if single value is given', () => {
    expect(removeEmpty(null)).toEqual([]);
    expect(removeEmpty('one' as any)).toEqual(['one']);
    expect(removeEmpty()).toEqual([]);
  });
});
