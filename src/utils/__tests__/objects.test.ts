import { removeProp, removeFalsy } from '../objects';

describe('removeProp', () => {
  test('removes the given property from the object', () => {
    const test = {
      key1: 'value1',
      key2: 'value2',
    };

    expect(removeProp(test, 'key1')).toEqual({ key2: 'value2' });
    expect(removeProp(test, 'key3')).toEqual({ key1: 'value1', key2: 'value2' });
  });
});

describe('removeFalsy', () => {
  test('removes falsy values from the object but leaves 0', () => {
    const test = {
      truthy: 'truthy',
      false: false,
      zero: 0,
      falsyString: '',
      null: null,
    };

    expect(removeFalsy(test)).toEqual({ truthy: 'truthy', zero: 0 });
  });
});
