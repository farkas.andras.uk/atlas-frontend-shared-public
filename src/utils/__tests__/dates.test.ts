import moment from 'moment';
import { formatDate, getRawDateValue, isValidDate } from '../dates';

describe('formatDate', () => {
  test('if there is no date provided', () => {
    expect(formatDate()).toEqual('');
    expect(formatDate(null)).toEqual('');
  });

  test('wrong input', () => {
    expect(formatDate({} as any)).toEqual('');
    expect(formatDate([] as any)).toEqual('');
    expect(formatDate('Test test test')).toEqual('');
  });

  test('input formatting', () => {
    expect(formatDate(new Date(2000, 1, 1))).toEqual('2000-01-01 12:00:00');
    expect(formatDate(new Date(2000, 1, 1), 'YYYY MM DD')).toEqual('2000 02 01');
    expect(formatDate(new Date(2000, 1, 1), 'YYYY MM DD', { utc: false })).toEqual('2000 02 01');
  });
});

describe('getRawDateValue', () => {
  test('gets the raw value from a moment object', () => {
    expect(getRawDateValue()).toEqual('');
    expect(getRawDateValue(null)).toEqual('');
    expect(getRawDateValue({} as any)).toEqual('');
    expect(getRawDateValue([] as any)).toEqual('');
    expect(getRawDateValue(1234 as any)).toEqual('');
    expect(getRawDateValue('Test' as any)).toEqual('');

    expect(getRawDateValue(moment(new Date(2000, 1, 1)))).toEqual(new Date(2000, 1, 1));
  });
});

describe('isValidDate', () => {
  test('determine if the given input is a valid date', () => {
    expect(isValidDate()).toEqual(true);
    expect(isValidDate(null)).toEqual(true);
    expect(isValidDate({} as any)).toEqual(false);
    expect(isValidDate([] as any)).toEqual(false);
    expect(isValidDate(moment(new Date(2000, 1, 1)))).toEqual(true);
    expect(isValidDate(new Date() as any)).toEqual(true);
  });
});
