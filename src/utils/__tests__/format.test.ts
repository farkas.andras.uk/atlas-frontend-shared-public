import i18n from 'i18next';

import { formatNumber, toInteger, parseStringAsInt, floorTo, ceilTo } from '../format';

jest.mock('i18n', () => ({
  __esModule: true,
  default: {
    language: 'hu',
  },
}));

describe('formatNumber', () => {
  test('formats the number to string in a human readable format', () => {
    expect(formatNumber(100)).toEqual('100');
    expect(formatNumber(null)).toEqual('0');
    expect(formatNumber()).toEqual('0');
    expect(formatNumber('100')).toEqual('100');
  });

  test('formats the number with a postfix value', () => {
    expect(formatNumber(100, { postfix: 'db' })).toEqual('100 db');
  });

  test('formats the number as a currency', () => {
    try {
      formatNumber(100, { currency: 'Ft' });
    } catch (e) {
      expect(e).toBeDefined();
    }

    expect(formatNumber(100, { currency: 'EUR' })).toContain('EUR');
    expect(formatNumber(100, { currency: 'EUR' })).toContain('100');
  });

  test('format number without browser Intl', () => {
    const { Intl } = global;

    delete global.Intl;

    expect(formatNumber(100, { postfix: 'db' })).toEqual('100 db');
    expect(formatNumber(100, { currency: 'EUR' })).toEqual('100 EUR');
    expect(formatNumber(100, { maximumFractionDigits: 2 })).toEqual('100,00');

    global.Intl = Intl;
  });
});

describe('parseStringAsInt and toInteger', () => {
  test('parses values as integer', () => {
    expect(toInteger('asdf')).toEqual(0);
    expect(toInteger(new Error('Exception'))).toEqual(0);
    expect(toInteger(0)).toEqual(0);
    expect(toInteger(100.01)).toEqual(100);
    expect(toInteger('10asdfadf')).toEqual(10);
  });

  test('parses strings or string arrays as integer', () => {
    expect(parseStringAsInt('100')).toEqual(100);
    expect(parseStringAsInt(['100', '200'])).toEqual([100, 200]);
  });
});

describe('floorTo and ceilTo', () => {
  test('floors a value to a given base', () => {
    expect(floorTo(16)).toEqual(10);
    expect(floorTo(13, 5)).toEqual(10);
    expect(floorTo(16, 5)).toEqual(15);
    expect(floorTo(270, 100)).toEqual(200);
  });

  test('ceils a value to a given base', () => {
    expect(ceilTo(16)).toEqual(20);
    expect(ceilTo(13, 5)).toEqual(15);
    expect(ceilTo(16, 5)).toEqual(20);
    expect(ceilTo(270, 100)).toEqual(300);
  });
});
