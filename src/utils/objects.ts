export function removeFalsy<P extends object>(obj: P) {
  const newObj = {} as P;
  const keys = Object.keys(obj) as unknown as Array<keyof P>;

  keys.forEach((prop) => {
    if (obj[prop] || (obj[prop] as unknown) === 0) {
      newObj[prop] = obj[prop];
    }
  });

  return newObj;
}

export function removeProp<P extends object>(obj: P, prop: keyof P | string) {
  return Object.keys(obj).reduce((result, key) => {
    if (key !== prop) {
      result[key as keyof P] = obj[key as keyof P];
    }

    return result;
  }, {} as P);
}
