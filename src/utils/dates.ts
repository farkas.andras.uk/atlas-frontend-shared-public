import moment, { Moment } from 'moment';

export const DATE_FORMAT = 'yyyy.MM.DD';
export const TIME_FORMAT = 'hh:mm';
export const DATE_TIME_FORMAT = `${DATE_FORMAT} ${TIME_FORMAT}`;

export type InputDateType = number | Date | string | Moment;
export type DateType = Moment;

interface Options {
  utc?: boolean;
}

export function formatDate(
  date?: InputDateType | null,
  formatString: string = DATE_TIME_FORMAT,
  { utc }: Options = { utc: true }
) {
  if (!date || !isValidDate(date)) {
    return '';
  }

  const momentDate = utc ? moment.utc(date) : moment(date);

  return momentDate.format(formatString);
}

export function getRawDateValue(value?: Moment | null) {
  // Workaround to get the raw input even if it is an invalid date.
  if (moment.isMoment(value)) {
    // eslint-disable-next-line
    return (value as any)['_i'];
  }

  return '';
}

export function isValidDate(value?: InputDateType | null) {
  if (!value) {
    return true;
  }

  if (moment.isMoment(value)) {
    return value.isValid();
  }

  return (new Date(value) as any) !== 'Invalid Date' && !Number.isNaN(new Date(value).getTime());
}
