import { AxiosRequestConfig } from 'axios';
import { encode } from 'js-base64';
import qs, { StringifyOptions } from 'query-string';
import isEmpty from 'lodash/isEmpty';

import { QueryParamModel } from '../models';
import sharedConfig from '../config';

export const getUseKey = (key: string) => `${key}${sharedConfig.FILTER_USE_KEY}`;

export enum Methods {
  GET = 'GET',
  PUT = 'PUT',
  POST = 'POST',
  DELETE = 'DELETE',
  PATCH = 'PATCH',
}

export enum Status {
  OK = 200,
  BAD_REQUEST = 400,
  UNAUTHORIZED = 401,
  FORBIDDEN = 403,
  NOT_FOUND = 404,
  UNPROCESSABLE_ENTITY = 422,
  INTERNAL_SERVER_ERROR = 500,
  BAD_GATEWAY = 502,
  SERVICE_UNAVAILABLE = 503,
  GATEWAY_TIMEOUT = 504,
}

export enum ConfirmRequests {
  USER_CREATE = 'USER_CREATE',
  USER_EDIT = 'USER_EDIT',
}

export interface RequestConfig extends AxiosRequestConfig {
  resource: string;
  method?: Methods;
  id?: ConfirmRequests;
  formData?: boolean;
}

export interface RequestError {
  message: string;
  path: string;
  response: any;
  statusCode: number;
  timestamp: string;
}

export interface GenerateUrlSettings {
  baseURL?: string;
  resource?: string;
  params?: Record<string, any>;
}

export interface QueryFormatConfigModel {
  queryFormat: StringifyOptions;
}

export type QueryParamsFunction = (
  config: QueryFormatConfigModel,
  params?: QueryParamModel,
  excludeParams?: string[]
) => string;

export type GenerateUrlFunction = (config: QueryFormatConfigModel, settings: GenerateUrlSettings) => string;

export type AppendParamsToUrlFunction = (
  config: QueryFormatConfigModel,
  url: string,
  params?: Record<string, any>
) => string;

export const appendParamsToUrl: AppendParamsToUrlFunction = ({ queryFormat }, url, params) => {
  const query = params && !isEmpty(params) ? `?${qs.stringify(params, queryFormat)}` : '';

  return `${url}${query}`;
};

export const generateUrl: GenerateUrlFunction = (config, { baseURL, resource = '', params } = {}) => {
  const url = `${baseURL || ''}/api/${resource}`;

  return appendParamsToUrl(config, url, params);
};

export const getQueryParams: QueryParamsFunction = ({ queryFormat }, params, excludeParams) => {
  let excludedParams = {};

  if (params) {
    Object.keys(params).forEach((key) => {
      const use = params[getUseKey(key)];
      if (
        !key.includes(sharedConfig.FILTER_USE_KEY) &&
        !(excludeParams && excludeParams?.includes(key)) &&
        (typeof use === 'undefined' || use === true)
      ) {
        excludedParams = { ...excludedParams, [key]: params[key] };
      }
    });
  }

  return params ? qs.stringify({ query: encode(JSON.stringify(excludedParams)) }, queryFormat) : '';
};

export const getBodyData = (data: any, formData?: boolean) => {
  if (!formData) {
    return data;
  }

  const bodyFormData = new FormData();

  Object.keys(data).forEach((key) => {
    bodyFormData.append(key, data[key]);
  });

  return bodyFormData;
};

export function requestMethods(config: QueryFormatConfigModel = { queryFormat: sharedConfig.QUERY_FORMAT }) {
  return {
    appendParamsToUrl: (url: string, params?: Record<string, any>) => appendParamsToUrl(config, url, params),
    generateUrl: (settings: GenerateUrlSettings) => generateUrl(config, settings),
    getQueryParams: (params?: QueryParamModel, excludeParams?: string[]) =>
      getQueryParams(config, params, excludeParams),
  };
}
