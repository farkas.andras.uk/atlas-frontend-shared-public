import get from 'lodash/get';
import { SortDirection } from '../models';

function filter<P = any>(object: P, key: keyof P, term?: string) {
  const value = get(object, key) || '';

  return value.toLowerCase().indexOf((term || '').toString().toLowerCase()) > -1;
}

export function arrayFilter<P = any>(collection: P[], filterKeys: Array<keyof P>, term?: string) {
  return collection.filter((item) => {
    return filterKeys.reduce<boolean>((result, value) => {
      return result || filter(item, value, term);
    }, false);
  });
}

export function getArraySortComparer<P = any>(arr: P[], prop: keyof P, direction?: SortDirection) {
  const isAscending = !direction || direction === SortDirection.ASC;
  const sortModifier = isAscending ? 1 : -1;

  if (arr.length && get(arr[0], prop) && get(arr[0], prop).localeCompare) {
    return (a: P, b: P) => get(a, prop) && get(a, prop).localeCompare(get(b, prop)) * sortModifier;
  }

  return (a: P, b: P) => {
    if (a[prop] === b[prop]) {
      return 0;
    }

    return a[prop] > b[prop] ? 1 * sortModifier : -1 * sortModifier;
  };
}

export function arraySort<P extends object = any>(arr: P[], prop: keyof P, direction?: SortDirection) {
  const comparerFunction = getArraySortComparer(arr, prop, direction);

  return [...arr].sort(comparerFunction);
}

export function getPage<P = any>(arr: P[], page = 1, pageSize) {
  return arr.slice((page - 1) * pageSize, page * pageSize);
}

export function removeDuplicates<P extends object = any>(array: P[], key: keyof P) {
  const lookup = new Set();
  return array.filter((obj) => !lookup.has(obj[key]) && lookup.add(obj[key]));
}

export function getDifferenceBy<T>(array1: T[], array2: Partial<T>[], key: string | keyof T): T[] {
  return array1.filter((item1) => !array2.find((item2) => get(item1, key) === get(item2, key)));
}

export function intersectionBy<T>(arr1: T[], arr2: Partial<T>[], key: string | keyof T): T[] {
  return arr1.filter((item1) => arr2.some((item2) => get(item1, key) === get(item2, key)));
}

export function arrayify<T>(item?: T | T[] | null) {
  if (Array.isArray(item)) {
    return item;
  }

  if (item) {
    return [item];
  }

  return [];
}

export function removeEmpty<T>(items?: T[] | null) {
  const array = arrayify(items);

  return array.filter((item) => item) as NonNullable<T>[];
}
