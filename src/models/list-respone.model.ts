export interface ListResponseModel<D = any> {
  data: D[];
  meta: ListResponseMetaModel;
}

export interface ListResponseMetaModel {
  currentPage?: number;
  itemCount?: number;
  itemsPerPage?: number;
  totalItems?: number;
  totalPages?: number;
}
