export enum SortDirection {
  ASC = 'ASC',
  DESC = 'DESC',
}

export interface QueryParamModel {
  [key: string]: any;
  page?: number;
  pageSize?: number;
  direction?: SortDirection | string;
  orderBy?: string;
  from?: string;
  filter?: string[];
  search?: string;
}
