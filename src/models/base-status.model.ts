export interface BaseStatusModel {
  isActive: boolean;
  isDeleted?: boolean | null;
  deletedAt?: string | null;
}
