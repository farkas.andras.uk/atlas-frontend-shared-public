import { ComponentType, ForwardRefExoticComponent, RefAttributes, ReactNode } from 'react';
import { SvgIconProps } from '@mui/material';

export type AllowedFor = any[] | any;

export interface AccessRestriction {
  allowedFor?: AllowedFor;
}

interface BaseRouteModel {
  path?: string;
  title: () => string;
  onClick?: () => void;
}

export interface BaseRouteConfigurationModel extends AccessRestriction, BaseRouteModel {
  link?: string;
  component?: ComponentType<any>;
  render?: () => ReactNode;
  hideAtSideBar?: boolean;
  breadcrumbs?: BaseRouteModel[];
  partialIncludeToMatch?: boolean;
}

export type RouteChildrenConfigurationModel = BaseRouteConfigurationModel;

export interface RouteConfigurationModel extends BaseRouteConfigurationModel {
  icon?: ComponentType<SvgIconProps> | ForwardRefExoticComponent<any & RefAttributes<unknown>> | ComponentType<any>;
  children?: RouteChildrenConfigurationModel[];
  key?: string;
}
