export interface PaddingMixin {
  paddingLeft: string;
  paddingRight: string;
  paddingBottom: string;
  paddingTop: string;
}

export interface SubSideBarMixin extends PaddingMixin {
  width: number;
}

export interface SideBarMixin {
  closedWidth: number;
  fullWidth: number;
  icon: {
    paddingLeft: string;
    paddingRight: string;
    paddingBottom: string;
    paddingTop: string;
    size: number;
    borderWidth: number;
    borderRadius: number;
  };
}

export interface HeaderMixin {
  height: number;
}

export interface PageHeaderMixin {
  paddingLeft: string;
  paddingRight: string;
  paddingBottom: number;
  paddingTop: number;
  background: string;
  appBar: {
    height: number;
  };
}

export interface ProfileMenuMixin {
  avatarContainer: PaddingMixin;
  avatar: {
    width: string;
    height: string;
    backgroundColor?: string;
    marginRight: string;
  };
}

export interface PageMixin {
  paddingLeft: string;
  paddingRight: string;
  paddingBottom: number;
  paddingTop: number;
  titleHeight: number;
}

export interface BreadcrumbsMixin extends Partial<PaddingMixin> {
  height: number;
  iconColor: string;
}

export interface TabsMixin {
  paddingBottom: number;
  paddingTop: number;
  tabHeight: number;
  InactiveTabFontColor: string;
}

/* declare module '@mui/styles/defaultTheme' {
  interface DefaultTheme extends Theme {}
} */

declare module '@mui/material/styles/createMixins' {
  interface Mixins {
    sideBar: SideBarMixin;
    subSideBar: SubSideBarMixin;
    header: HeaderMixin;
    filter: SideBarMixin;
    pageHeader: PageHeaderMixin;
    profileMenu: ProfileMenuMixin;
    page: PageMixin;
    breadcrumbs: BreadcrumbsMixin;
    tabs: TabsMixin;
  }
}

declare module '@mui/material/styles/createPalette' {
  interface TypeBackground {
    dark: string;
  }

  interface SimplePaletteColorOptions {
    hover?: string;
  }

  interface TypeAction {
    main: string;
    hoverDark: string;
  }

  interface Palette {
    success: PaletteColor;
    warning: PaletteColor;
    dark: PaletteColor;
  }

  interface PaletteOptions {
    success?: PaletteColorOptions;
    warning?: PaletteColorOptions;
    dark?: PaletteColorOptions;
  }
}

declare module '@mui/material/styles/createPalette' {
  interface CommonColors {
    zltBlue: string;
    blueE5: string;
    greyE5: string;
    grey31: string;
    greyE0: string;
    greyF4: string;
    greyF5: string;
    grey7d: string;
    greyF7: string;
    black43: string;
    redFF: string;
    green89: string;
  }
}
