import { StringifyOptions } from 'query-string';

export type InputVariant = 'outlined' | 'filled' | 'standard';
export type InputMargin = 'none' | 'dense' | 'normal';

interface SharedConfigModel extends NodeJS.ProcessEnv {
  [key: string]: any;
  API_TOKEN_KEY: string;
  API_REFRESH_TOKEN_KEY: string;
  API_SECRET_TOKEN_KEY: string;
  API_XCSRF_TOKEN_KEY: string;
  QUERY_FORMAT: StringifyOptions;
  DEFAULT_INPUT_VARIANT: InputVariant;
  DEFAULT_INPUT_MARGIN: InputMargin;
  MINIMUM_PASSWORD_LENGTH: number;
  ROWS_PER_PAGE_OPTIONS: number[];
  CARD_NUMBER_MIN_LENGTH: number;
  CARD_NUMBER_MAX_LENGTH: number;
  PASSWORD_STRENGTH_PATTERN: RegExp;
  FILTER_USE_KEY: string;
  FILTER_DELAY: number;
}

const SharedConfig: SharedConfigModel = {
  API_TOKEN_KEY: 'access_token',
  API_REFRESH_TOKEN_KEY: 'refresh_token',
  API_SECRET_TOKEN_KEY: 'secret',
  API_XCSRF_TOKEN_KEY: 'X-CSRF-Token',
  QUERY_FORMAT: { arrayFormat: 'bracket' },
  DEFAULT_INPUT_VARIANT: 'standard',
  DEFAULT_INPUT_MARGIN: 'dense',
  DEFAULT_DATEPICKER_VARIANT: 'inline',
  MINIMUM_PASSWORD_LENGTH: 5,
  ROWS_PER_PAGE_OPTIONS: [10, 25, 50, 100],
  CARD_NUMBER_MIN_LENGTH: 4,
  CARD_NUMBER_MAX_LENGTH: 15,
  FILTER_USE_KEY: 'Use',
  PASSWORD_STRENGTH_PATTERN: /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)[A-Za-z\d@#$%&*+:;!.?()-_]{8,24}$/,
  FILTER_DELAY: 1000,
  ...process.env,
};

export default SharedConfig;
