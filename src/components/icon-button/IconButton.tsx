import React, { ReactNode } from 'react';
import Box from '@mui/material/Box';
import { Typography } from '../typography/Typography';
import { sx_config } from './IconButton.style';

export interface IconButtonProps {
  icon?: ReactNode;
  label?: ReactNode;
  onClick?: () => void;
  disabled?: boolean;
  size?: string;
}

export const IconButton = ({ icon, label, onClick, disabled }: IconButtonProps) => {
  return (
    <Box
      sx={{
        ...sx_config.iconButton,
        ...(disabled && { ...sx_config.disabled }),
      }}
      onClick={() => {
        if (onClick && !disabled) {
          onClick();
        }
      }}
    >
      {icon}
      <Box sx={sx_config.iconText}>
        <Typography variant="inherit">{label}</Typography>
      </Box>
    </Box>
  );
};
