import { Theme } from '@mui/material/styles';

export const sx_config = {
  iconButton: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    cursor: 'pointer',
    color: (theme: Theme) => theme.palette.common.grey31,
    fontSize: 12,
    fontWeight: 500,
  },
  iconText: {
    marginTop: '3px',
  },
  disabled: {
    cursor: 'default',
    opacity: 0.7,
  },
};
