import React, { useState } from 'react';

import ArrowDownIcon from '@mui/icons-material/KeyboardArrowDown';

import { IconButton } from './IconButton';

export default {
  title: 'IconButton',
  component: IconButton,
};

export const Default = (): React.ReactNode => {
  const [label, setLabel] = useState('IconButton');
  const onClickIconButton = () => {
    setLabel(label === 'IconButton' ? 'Icon' : 'IconButton');
  };

  return <IconButton icon={<ArrowDownIcon />} onClick={onClickIconButton} label={label} />;
};
