export enum pageSize {
  A4 = 'A4',
}

export enum pageOrientation {
  PORTRAIT = 'p',
  LANDSCAPE = 'l',
}
