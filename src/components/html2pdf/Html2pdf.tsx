import React, { Fragment, ReactNode } from 'react';
import html2canvas from 'html2canvas';
import { jsPDF } from 'jspdf';
import Button from '@mui/material/Button';
import PictureAsPdfIcon from '@mui/icons-material/PictureAsPdf';
import { pageSize, pageOrientation } from './Html2pdf.model';

export interface Html2pdfProps {
  pageSettings?: {
    size: pageSize;
    orientation: pageOrientation;
    mmToPixelMultiplier: number;
  };
  icon?: ReactNode;
  generatedPdfFileName?: string;
  contentForPdfCreation: ReactNode;
}

const calculatePageSize = (pageSettings: {
  size: pageSize;
  orientation: pageOrientation;
  mmToPixelMultiplier: number;
}): { height: { mm: number; pixel: number }; width: { mm: number; pixel: number } } => {
  const mmToPixel = 3.7795275591 * pageSettings.mmToPixelMultiplier;
  let width;
  let height;
  switch (pageSettings.size) {
    case pageSize.A4:
      width = 210;
      height = 297;
      break;
    default:
      break;
  }

  if (pageSettings.orientation === pageOrientation.LANDSCAPE) {
    return { height: { mm: width, pixel: width * mmToPixel }, width: { mm: height, pixel: height * mmToPixel } };
  } else {
    return { height: { mm: height, pixel: height * mmToPixel }, width: { mm: width, pixel: width * mmToPixel } };
  }
};

export const Html2pdf = ({
  pageSettings = {
    size: pageSize.A4,
    orientation: pageOrientation.PORTRAIT,
    mmToPixelMultiplier: 1.5,
  },
  icon,
  generatedPdfFileName,
  contentForPdfCreation,
}: Html2pdfProps) => {
  const printRef = React.useRef();
  const calculatedPageSize = calculatePageSize(pageSettings);

  const handleDownloadPdf = async () => {
    const element = printRef.current;
    const canvas = await html2canvas(element, {
      onclone: (document: Document) => {
        const div = document.getElementById('pdf-render');
        div.style.display = 'block';
      },
    });
    const data = canvas.toDataURL('image/png');

    const pdf = new jsPDF(
      pageSettings.orientation,
      'mm',
      [calculatedPageSize.height.mm, calculatedPageSize.width.mm],
      true
    );
    const imgProperties = pdf.getImageProperties(data);
    const pdfWidth = pdf.internal.pageSize.getWidth();
    const pdfHeight = (imgProperties.height * pdfWidth) / imgProperties.width;

    pdf.addImage(data, 'PNG', 0, 0, pdfWidth, pdfHeight);
    pdf.save(`${generatedPdfFileName ? generatedPdfFileName : 'generated'}.pdf`);
  };

  return (
    <Fragment>
      <Button color="primary" sx={{ padding: 0, minHeight: 0, minWidth: 0 }} onClick={() => handleDownloadPdf()}>
        {icon ? icon : <PictureAsPdfIcon color="primary" />}
      </Button>

      <div
        id="pdf-render"
        ref={printRef}
        style={{
          display: 'none',
          height: `${calculatedPageSize.height.pixel}px`,
          width: `${calculatedPageSize.width.pixel}px`,
        }}
      >
        {contentForPdfCreation}
      </div>
    </Fragment>
  );
};
