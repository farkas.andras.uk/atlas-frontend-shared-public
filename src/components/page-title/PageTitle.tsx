import React, { FC, ReactNode } from 'react';
import Box from '@mui/material/Box';
import { Typography } from '../typography/Typography';

export interface PageTitleProps {
  title?: ReactNode;
  breadcrumbs: ReactNode;
  rightContent?: ReactNode;
}

export const PageTitle: FC<PageTitleProps> = ({ title, breadcrumbs, rightContent }) => (
  <Box display="flex" alignItems="center" justifyContent="space-between">
    <Box>
      {breadcrumbs}
      <Box display="flex" justifyContent="space-between" alignItems="center">
        <Typography variant="h5" color="#1C5B95" uppercase>
          {title}
        </Typography>
      </Box>
    </Box>
    <Box>{rightContent}</Box>
  </Box>
);
