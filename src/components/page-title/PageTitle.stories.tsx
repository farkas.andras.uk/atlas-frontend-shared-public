import React from 'react';
import { Router } from 'react-router-dom';
import { createBrowserHistory } from 'history';

import { ThemeProvider, StyledEngineProvider } from '@mui/material/styles';

import { theme } from '../../theme';
import { Breadcrumbs } from '../breadcrumbs/Breadcrumbs';

import { PageTitle } from './PageTitle';

const history = createBrowserHistory();

export default {
  title: 'PageTitle',
  component: PageTitle,
};

export const Default = (): React.ReactNode => {
  return (
    <Router history={history}>
      <StyledEngineProvider injectFirst>
        <ThemeProvider theme={theme}>
          <PageTitle
            title="PageTitle"
            breadcrumbs={
              <Breadcrumbs
                homeLabel={'Home'}
                routes={[
                  {
                    path: '/',
                    title: () => 'Users',
                    breadcrumbs: [{ title: () => 'Partners', path: '/partners' }, { title: () => 'Users' }],
                  },
                ]}
              />
            }
          />
        </ThemeProvider>
      </StyledEngineProvider>
    </Router>
  );
};
