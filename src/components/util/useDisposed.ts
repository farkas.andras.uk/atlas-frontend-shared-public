import { useEffect, useRef } from 'react';

export function useDisposed(): boolean {
  const disposed = useRef(false);

  useEffect(() => {
    return () => {
      disposed.current = true;
    };
  }, []);

  return disposed.current;
}
