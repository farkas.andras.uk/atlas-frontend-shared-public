import { EffectCallback, useLayoutEffect } from 'react';
import throttle from 'lodash/throttle';

export function useScrollListener(effect: EffectCallback, wait = 250) {
  const handleScroll = throttle(effect, wait);

  useLayoutEffect(() => {
    window.addEventListener('scroll', handleScroll);

    return () => {
      window.removeEventListener('scroll', handleScroll);
    };
  }, [handleScroll]);
}
