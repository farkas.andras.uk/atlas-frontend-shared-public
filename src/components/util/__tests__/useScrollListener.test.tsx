import React from 'react';
import { render, fireEvent, cleanup } from '@testing-library/react';

import { useScrollListener } from '../useScrollListener';

describe('useScrollListener', () => {
  beforeEach(() => {
    cleanup();
  });

  test('the hook should be called on window resize', () => {
    const spy = jest.fn();

    const TestComponent = () => {
      useScrollListener(spy);

      return null;
    };

    render(<TestComponent />);

    const scrollEvent = window.document.createEvent('UIEvents');
    scrollEvent.initEvent('scroll', true, false);

    fireEvent(window, scrollEvent);

    expect(spy).toHaveBeenCalled();
  });
});
