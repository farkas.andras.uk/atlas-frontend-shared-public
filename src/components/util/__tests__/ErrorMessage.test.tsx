import React from 'react';
import { cleanup, render } from '@testing-library/react';

import { ErrorMessage } from '../ErrorMessage';

describe('ErrorMessage component', () => {
  beforeEach(() => {
    cleanup();
  });

  test('do not show anything if form and field is not defined', () => {
    const element = render(<ErrorMessage />);

    expect(element.container.firstChild).toBe(null);
  });

  test('do not show de message if field is not touched', () => {
    const element = render(
      <ErrorMessage field={{ name: 'test' } as any} form={{ touched: {}, errors: { test: 'Test Error' } } as any} />
    );

    expect(element.queryByText(/Test Error/i)).toBeNull();
  });

  test('do not show show the message if field does not have an error', () => {
    const element = render(
      <ErrorMessage field={{ name: 'test' } as any} form={{ touched: { test: true }, errors: {} } as any} />
    );

    expect(element.getByTestId('error-message-empty-state')).toBeInTheDocument();
  });

  test('show the message if field has an error and it is touched', () => {
    const element = render(
      <ErrorMessage
        field={{ name: 'test' } as any}
        form={{ touched: { test: true }, errors: { test: 'Test Error' } } as any}
      />
    );

    expect(element.queryByText(/Test Error/i)).toBeInTheDocument();
  });
});
