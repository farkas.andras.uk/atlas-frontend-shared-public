import React from 'react';
import { render, fireEvent, cleanup } from '@testing-library/react';

import { useWindowResize } from '../useWindowResize';

describe('useWindowResize', () => {
  beforeEach(() => {
    cleanup();
  });

  test('the hook should be called on window resize', () => {
    const spy = jest.fn();

    const TestComponent = () => {
      useWindowResize(spy);

      return null;
    };

    render(<TestComponent />);

    const resizeEvent = window.document.createEvent('UIEvents');
    resizeEvent.initEvent('resize', true, false);

    fireEvent(window, resizeEvent);

    expect(spy).toHaveBeenCalled();
  });
});
