import React from 'react';
import { cleanup, render } from '@testing-library/react';

import { ConditionalWrapper } from '../ConditionalWrapper';

describe('ConditionalWrapper component', () => {
  beforeEach(() => {
    cleanup();
  });

  test('wrap the children in the specified wrapper if the condition is true', () => {
    const element = render(
      <ConditionalWrapper condition={true} wrapper={(children) => <div data-testid="test-wrapper">{children}</div>}>
        Wrapped element
      </ConditionalWrapper>
    );

    expect(element.getByTestId('test-wrapper')).toBeInTheDocument();
  });

  test('does not wrap the children in the specified wrapper if the condition is false', () => {
    const element = render(
      <ConditionalWrapper condition={false} wrapper={(children) => <div data-testid="test-wrapper">{children}</div>}>
        Wrapped element
      </ConditionalWrapper>
    );

    expect(element.queryByTestId('test-wrapper')).toBe(null);
  });
});
