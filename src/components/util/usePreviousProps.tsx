import { useRef, useEffect } from 'react';

export function usePrevious<T>(value: T): T {
  const ref = useRef<T>();

  useEffect(() => {
    ref.current = value;
  }, [value]);

  return ref.current;
}

export function usePreviousProps<T>(value: T): T {
  const prev = usePrevious<T>(value || ({} as T));

  return prev || ({} as T);
}
