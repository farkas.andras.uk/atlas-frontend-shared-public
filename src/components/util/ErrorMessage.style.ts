import { Theme } from '@mui/material/styles';

export const sx_config = {
  root: {
    marginTop: (theme: Theme) => theme.spacing(-0.5),
  },
};
