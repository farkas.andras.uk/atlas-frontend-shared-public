import React, { FC } from 'react';
import { FieldProps } from 'formik';
import FormHelperText from '@mui/material/FormHelperText';
import { getErrorMessage, isTouched } from '../../utils/form';
import { sx_config } from './ErrorMessage.style';

interface ErrorMessageProps extends Partial<FieldProps> {
  variant?: 'standard' | 'outlined' | 'filled';
  className?: string;
  message?: string | null;
  notInForm?: boolean;
}

export const ErrorMessage: FC<ErrorMessageProps> = ({ variant, form, field, className, message, notInForm }) => {
  if ((!form || !field) && !notInForm) {
    return null;
  }

  const touched = isTouched(form, field);
  const errorMessage = getErrorMessage(form, field);

  const empty = <span data-testid="error-message-empty-state">&nbsp;</span>;

  return (
    <FormHelperText
      error
      variant={variant}
      sx={{
        ...sx_config.root,
        ...(className && { ...(className as React.CSSProperties) }),
      }}
      role="error"
    >
      {touched || notInForm ? errorMessage || message || empty : empty}
    </FormHelperText>
  );
};
