import React, { FC } from 'react';
import Viewer from 'react-viewer';
import './ImageViewer.css';

const Z_INDEX = 1400;

export interface ImageModel {
  src: string;
  alt: string;
}

export interface ImageViewerProps {
  /**
   * images array like [{"src": "http://...", "alt": "Test"}].
   */
  images: ImageModel[];
  /**
   * image index to show when open modal.
   */
  photoIndex: number | null;
  /**
   * Custom zIndex if it needed, default is 1400
   */
  zIndex?: number;

  setClose?: () => void;
}

export const ImageViewer: FC<ImageViewerProps> = ({ images, photoIndex, zIndex = Z_INDEX, setClose }) => {
  return images ? (
    <Viewer
      visible={photoIndex !== null}
      onClose={() => {
        setClose();
      }}
      zIndex={zIndex}
      activeIndex={photoIndex || 0}
      images={images}
    />
  ) : null;
};
