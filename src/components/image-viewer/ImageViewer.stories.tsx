import React from 'react';
import { Meta, Story } from '@storybook/react/types-6-0';

import { ImageViewer, ImageViewerProps } from './ImageViewer';

export default {
  title: 'ImageViewer',
  component: ImageViewer,
} as Meta;

export const Interactive: Story<ImageViewerProps> = (args) => {
  return <ImageViewer {...args} photoIndex={0} setClose={() => {}} />;
};

Interactive.args = {
  images: [
    { src: 'http://placekitten.com/200/300', alt: 'http://placekitten.com/200/300' },
    { src: 'http://placekitten.com/300/200', alt: 'http://placekitten.com/300/200' },
    { src: 'http://placekitten.com/300/300', alt: 'http://placekitten.com/300/300' },
  ],
};
