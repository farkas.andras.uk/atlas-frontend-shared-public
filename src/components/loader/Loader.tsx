import React, { ReactNode } from 'react';

import Box from '@mui/material/Box';

import { ProgressAnimation } from '../progress-animation/ProgressAnimation';
import { Typography, TypographyProps } from '../typography/Typography';

export interface LoaderProps {
  label: ReactNode | string;
  labelProps?: TypographyProps;
}

export const Loader = ({ label, labelProps }: LoaderProps) => {
  return (
    <Box display="flex" alignItems="center" justifyContent="center" flexDirection="column">
      <ProgressAnimation />
      <Typography {...labelProps}>{label}</Typography>
    </Box>
  );
};
