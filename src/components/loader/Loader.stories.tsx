import React from 'react';
import { Meta, Story } from '@storybook/react/types-6-0';

import { Loader, LoaderProps } from './Loader';

export default {
  title: 'Loader',
  component: Loader,
} as Meta;

export const Interactive: Story = (args: LoaderProps) => (
  <div style={{ backgroundColor: 'black' }}>
    <Loader {...args} />
  </div>
);

Interactive.args = {
  label: 'Loading...',
  labelProps: {
    variant: 'body1',
    fontWeight: '800',
    color: 'white',
  },
};
