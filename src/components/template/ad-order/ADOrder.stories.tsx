import React from 'react';
import { Meta, Story } from '@storybook/react/types-6-0';
import { ADOrderTemplate, ADOrderTemplateProps } from './ADOrder';

export default {
  title: 'Template/ADOrderTemplate',
  component: ADOrderTemplate,
} as Meta;

export const Interactive: Story<ADOrderTemplateProps> = (args) => {
  return <ADOrderTemplate {...args} />;
};

Interactive.args = {
  PageSettings: {
    OutsideMargin: 30,
    InsideMargin: {
      left: 5,
      right: 5,
      top: 5,
      bottom: 5,
    },
    TitleFontStyle: {
      size: 16,
      bold: true,
    },
    ValueFontStyle: {
      size: 14,
      bold: false,
    },
  },
  OrderType: 'Belföldi értékesítés WEB',
  OrderNumber: '2023-BÉW/ 000115',
  Copy: {
    Title: 'példány',
    actual: 1,
    of: 1,
  },
  Supplier: {
    Title: 'SU_Szállító',
    Value: 'SU_ZLT MAGYARORSZÁG KFT.',
  },
  Storage: {
    Title: 'ST_Raktár:',
    Value: {
      Name: 'ST_Fő raktár',
      Address: {
        CountryOrRegion: 'ST_Magyarország',
        ZIPOrPostalCode: 'ST_9011',
        City: 'ST_Győr-Győrszentiván',
        StreetAddress: 'ST_Törökverő 1.',
      },
      Contats: {
        PhoneAndFax: {
          Title: 'ST_Tel, Fax:',
          Phone: 'ST_3696550600',
          Fax: 'ST_3696550604',
        },
        email: {
          Title: 'ST_E-mail:',
          Value: 'ST_mail@zlt.hu',
        },
      },
    },
  },
  Customer: {
    Title: 'C_Vevő',
    Value: {
      Code: 'C_7304',
      Name: 'C_Rába Futómű Kft.',
      Address: {
        CountryOrRegion: 'C_Magyarország',
        ZIPOrPostalCode: 'C_9027',
        City: 'C_Győr',
        StreetAddress: 'C_Martin út 1.',
      },
    },
  },
  Site: {
    Title: 'S_Telephely:',
    Value: {
      Code: 'S_73041',
      Address: {
        ZIPOrPostalCode: 'S_9027',
        City: 'S_Győr',
        StreetAddress: 'S_Martin út 1.',
      },
      Contats: {
        PhoneAndFax: { Title: 'S_Tel, Fax:', Phone: 'S_3696550600', Fax: 'S_3696550604' },
        email: {
          Title: 'S_E-mail:',
          Value: 'S_mail@zlt.hu',
        },
      },
    },
  },
  Date: {
    Title: 'Dátum',
    Value: '2023-04-27',
  },
  CertificateOfOrigin: {
    Title: 'Alapbizonylat',
    Value: '211815/0',
  },
  Order: 'Rendelés',
  Reference: { Title: 'Hivatkozás', Value: '211815/0' },
  Note: {
    Title: 'Megjegyzés',
    Value: 'Megrendelő: Birkás Éva; Hivatkozás: 211815/0; Megjegyzés: 211815/0',
  },
  OrderItemHeader: {
    Code: 'Kód',
    Description: 'Megnevezés',
    Quantity: 'Menny .',
    Unit: 'M . e .',
    UnitPrice: 'Egys . ár',
    Value: 'Érték',
    Currency: 'Valuta',
  },
  OrderItem: [
    {
      id: 1,
      Code: 'SW022103242',
      Description: 'Peremes anya M8-as DIN6923',
      Quantity: 500,
      Unit: 'db',
      UnitPrice: 9,
      Value: 4500,
      Currency: 'HUF',
    },
    {
      id: 2,
      Code: 'ADG680167803',
      Description: 'M10 Peremes anya DIN7681',
      Quantity: 50,
      Unit: 'db',
      UnitPrice: 30,
      Value: 1500,
      Currency: 'HUF',
    },
  ],
  Total: {
    Title: 'Összesen:',
    Quantity: 550,
    Value: 6000,
    Currency: 'HUF',
  },
  Signature: 'cégszerű aláírása',
};
