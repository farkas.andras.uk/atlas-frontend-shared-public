import React from 'react';
import { ZLTLogo } from './zlt-logo';

interface TitlePropType {
  Title: string;
}

interface StandardFieldPropType extends TitlePropType {
  Value: string;
}

interface AddressType {
  CountryOrRegion?: string;
  ZIPOrPostalCode: string;
  City: string;
  StreetAddress: string;
}

interface ContactsType {
  PhoneAndFax: {
    Title: string;
    Phone?: string;
    Fax?: string;
  };
  email?: {
    Title: string;
    Value?: string;
  };
}

interface ContactsPropType extends TitlePropType {
  Value: {
    Code?: string;
    Name?: string;
    Address: AddressType;
    Contats?: ContactsType;
  };
}

export interface ADOrderTemplateProps {
  PageSettings?: {
    OutsideMargin?: number;
    InsideMargin?: {
      left?: number;
      right?: number;
      top?: number;
      bottom?: number;
    };
    TitleFontStyle?: {
      size?: number;
      bold?: boolean;
    };
    ValueFontStyle?: {
      size?: number;
      bold?: boolean;
    };
  };
  OrderType: string;
  OrderNumber: string;
  Copy: {
    Title: string;
    actual: number;
    of: number;
  };
  Supplier: StandardFieldPropType;
  Storage: ContactsPropType;
  Customer: ContactsPropType;
  Site: ContactsPropType;
  Date: StandardFieldPropType;
  CertificateOfOrigin: StandardFieldPropType;
  Order: string;
  Reference: StandardFieldPropType;
  Note: StandardFieldPropType;
  OrderItemHeader: {
    Code: string;
    Description: string;
    Quantity: string;
    Unit: string;
    UnitPrice: string;
    Value: string;
    Currency: string;
  };
  OrderItem: {
    id: number;
    Code: string;
    Description: string;
    Quantity: number;
    Unit: string;
    UnitPrice: number;
    Value: number;
    Currency: string;
  }[];
  Total: {
    Title: string;
    Quantity: number;
    Value: number;
    Currency: string;
  };
  Signature: string;
}

export const ADOrderTemplate = ({
  PageSettings = {
    OutsideMargin: 30,
    InsideMargin: {
      left: 5,
      right: 5,
      top: 5,
      bottom: 5,
    },
    TitleFontStyle: {
      size: 16,
      bold: true,
    },
    ValueFontStyle: {
      size: 14,
      bold: false,
    },
  },
  OrderType,
  OrderNumber,
  Copy,
  Supplier,
  Storage,
  Customer,
  Site,
  Date,
  CertificateOfOrigin,
  Order,
  Reference,
  Note,
  OrderItemHeader,
  OrderItem,
  Total,
  Signature,
}: ADOrderTemplateProps) => {
  return (
    <div id={'ad-order-container'} style={{ display: 'flex', flexDirection: 'column' }}>
      <div
        id={'ad-order-body'}
        style={{ margin: PageSettings.OutsideMargin, display: 'flex', flexDirection: 'column' }}
      >
        {/* Info */}
        <div id={'ad-order-info'} style={{ display: 'flex', flex: 1, flexDirection: 'row' }}>
          <div
            id={'ad-order-info-logo'}
            style={{
              display: 'flex',
              flex: 3,
              flexDirection: 'column',
              alignItems: 'center',
            }}
          >
            <ZLTLogo />
          </div>
          <div id={'ad-order-info-panel'} style={{ display: 'flex', flex: 7, flexDirection: 'column' }}>
            <div style={{ display: 'flex', flex: 1, flexDirection: 'row' }}></div>
            <div style={{ display: 'flex', flex: 1, flexDirection: 'column' }}>
              <div style={{ display: 'flex', flex: 1, flexDirection: 'row' }}>
                <div
                  style={{
                    borderBottom: '2px solid black',
                    display: 'flex',
                    flex: 6,
                    flexDirection: 'row',
                    alignItems: 'center',
                  }}
                >
                  <p
                    style={{
                      margin: `${PageSettings.InsideMargin.top}px ${PageSettings.InsideMargin.right}px ${PageSettings.InsideMargin.bottom}px ${PageSettings.InsideMargin.left}px`,
                      fontWeight: PageSettings.TitleFontStyle.bold ? 'bold' : 'normal',
                      fontSize: PageSettings.TitleFontStyle.size,
                    }}
                  >
                    {OrderType}
                  </p>
                </div>
                <div
                  style={{
                    border: '2px solid black',
                    display: 'flex',
                    flex: 4,
                    flexDirection: 'row',
                    alignItems: 'center',
                  }}
                >
                  <p
                    style={{
                      margin: `${PageSettings.InsideMargin.top}px ${PageSettings.InsideMargin.right}px ${PageSettings.InsideMargin.bottom}px ${PageSettings.InsideMargin.left}px`,
                      fontWeight: PageSettings.TitleFontStyle.bold ? 'bold' : 'normal',
                      fontSize: PageSettings.TitleFontStyle.size,
                    }}
                  >
                    {OrderNumber}
                  </p>
                </div>
              </div>
              <div
                style={{
                  display: 'flex',
                  flex: 1,
                  flexDirection: 'row',
                  alignItems: 'center',
                }}
              >
                <p
                  style={{
                    margin: `${PageSettings.InsideMargin.top}px ${PageSettings.InsideMargin.right}px ${PageSettings.InsideMargin.bottom}px ${PageSettings.InsideMargin.left}px`,
                    fontWeight: PageSettings.ValueFontStyle.bold ? 'bold' : 'normal',
                    fontSize: PageSettings.ValueFontStyle.size,
                  }}
                >{`${Copy.actual}/${Copy.of} ${Copy.Title}`}</p>
              </div>
            </div>
          </div>
        </div>
        {/* Header */}
        <div id={'ad-order-header'} style={{ display: 'flex', flex: 1, flexDirection: 'column' }}>
          {/* Header Panel */}

          <div
            id={'ad-order-header-panel'}
            style={{ border: '2px solid black', display: 'flex', flex: 1, flexDirection: 'row' }}
          >
            {/* Header Left Panel */}

            <div
              id={'ad-order-header-left-panel'}
              style={{ borderRight: '2px solid black', display: 'flex', flex: 1, flexDirection: 'column' }}
            >
              {/* Supplier */}

              <div
                id={'ad-order-header-left-panel-supplier'}
                style={{
                  display: 'flex',
                  flex: 1,
                  flexDirection: 'row',
                  alignItems: 'center',
                }}
              >
                <div style={{ display: 'flex', flex: 3, flexDirection: 'row' }}>
                  <p
                    style={{
                      margin: `${PageSettings.InsideMargin.top}px ${PageSettings.InsideMargin.right}px ${PageSettings.InsideMargin.bottom}px ${PageSettings.InsideMargin.left}px`,
                      fontWeight: PageSettings.TitleFontStyle.bold ? 'bold' : 'normal',
                      fontSize: PageSettings.TitleFontStyle.size,
                    }}
                  >{`${Supplier.Title}:`}</p>
                </div>
                <div style={{ display: 'flex', flex: 7, flexDirection: 'row' }}>
                  <p
                    style={{
                      margin: `${PageSettings.InsideMargin.top}px ${PageSettings.InsideMargin.right}px ${PageSettings.InsideMargin.bottom}px ${PageSettings.InsideMargin.left}px`,
                      fontWeight: PageSettings.ValueFontStyle.bold ? 'bold' : 'normal',
                      fontSize: PageSettings.ValueFontStyle.size,
                    }}
                  >
                    {Supplier.Value}
                  </p>
                </div>
              </div>

              {/* Storage */}

              <div
                id={'ad-order-header-left-panel-storage-title'}
                style={{
                  display: 'flex',
                  flex: 1,
                  flexDirection: 'row',
                  alignItems: 'center',
                }}
              >
                <div style={{ display: 'flex', flex: 3, flexDirection: 'row' }}>
                  <p
                    style={{
                      margin: `${PageSettings.InsideMargin.top}px ${PageSettings.InsideMargin.right}px ${PageSettings.InsideMargin.bottom}px ${PageSettings.InsideMargin.left}px`,
                      fontWeight: PageSettings.TitleFontStyle.bold ? 'bold' : 'normal',
                      fontSize: PageSettings.TitleFontStyle.size,
                    }}
                  >
                    {Storage.Title}
                  </p>
                </div>
                <div style={{ display: 'flex', flex: 7, flexDirection: 'row' }}>
                  <p
                    style={{
                      margin: `${PageSettings.InsideMargin.top}px ${PageSettings.InsideMargin.right}px ${PageSettings.InsideMargin.bottom}px ${PageSettings.InsideMargin.left}px`,
                      fontWeight: PageSettings.ValueFontStyle.bold ? 'bold' : 'normal',
                      fontSize: PageSettings.ValueFontStyle.size,
                    }}
                  ></p>
                </div>
              </div>

              <div
                id={'ad-order-header-left-panel-storage-value'}
                style={{ display: 'flex', flex: 1, flexDirection: 'row' }}
              >
                <div style={{ display: 'flex', flex: 3, flexDirection: 'row' }}>
                  <p
                    style={{
                      margin: `${PageSettings.InsideMargin.top}px ${PageSettings.InsideMargin.right}px ${PageSettings.InsideMargin.bottom}px ${PageSettings.InsideMargin.left}px`,
                      fontWeight: PageSettings.TitleFontStyle.bold ? 'bold' : 'normal',
                      fontSize: PageSettings.TitleFontStyle.size,
                    }}
                  ></p>
                </div>
                <div style={{ display: 'flex', flex: 7, flexDirection: 'row' }}>
                  <p
                    style={{
                      margin: `${PageSettings.InsideMargin.top}px ${PageSettings.InsideMargin.right}px ${PageSettings.InsideMargin.bottom}px ${PageSettings.InsideMargin.left}px`,
                      fontWeight: PageSettings.ValueFontStyle.bold ? 'bold' : 'normal',
                      fontSize: PageSettings.ValueFontStyle.size,
                    }}
                  >
                    {Storage.Value.Name}
                  </p>
                </div>
              </div>

              <div style={{ display: 'flex', flex: 1, flexDirection: 'row' }}>
                <div style={{ display: 'flex', flex: 3, flexDirection: 'row' }}>
                  <p
                    style={{
                      margin: `${PageSettings.InsideMargin.top}px ${PageSettings.InsideMargin.right}px ${PageSettings.InsideMargin.bottom}px ${PageSettings.InsideMargin.left}px`,
                      fontWeight: PageSettings.TitleFontStyle.bold ? 'bold' : 'normal',
                      fontSize: PageSettings.TitleFontStyle.size,
                    }}
                  ></p>
                </div>
                <div style={{ display: 'flex', flex: 7, flexDirection: 'row' }}>
                  <p
                    style={{
                      margin: `${PageSettings.InsideMargin.top}px ${PageSettings.InsideMargin.right}px ${PageSettings.InsideMargin.bottom}px ${PageSettings.InsideMargin.left}px`,
                      fontWeight: PageSettings.ValueFontStyle.bold ? 'bold' : 'normal',
                      fontSize: PageSettings.ValueFontStyle.size,
                    }}
                  ></p>
                </div>
              </div>

              <div
                id={'ad-order-header-left-panel-storage-address'}
                style={{ display: 'flex', flex: 1, flexDirection: 'row' }}
              >
                <div style={{ display: 'flex', flex: 3, flexDirection: 'row' }}>
                  <p
                    style={{
                      margin: `${PageSettings.InsideMargin.top}px ${PageSettings.InsideMargin.right}px ${PageSettings.InsideMargin.bottom}px ${PageSettings.InsideMargin.left}px`,
                      fontWeight: PageSettings.TitleFontStyle.bold ? 'bold' : 'normal',
                      fontSize: PageSettings.TitleFontStyle.size,
                    }}
                  ></p>
                </div>
                <div style={{ display: 'flex', flex: 7, flexDirection: 'row' }}>
                  <p
                    style={{
                      margin: `${PageSettings.InsideMargin.top}px ${PageSettings.InsideMargin.right}px ${PageSettings.InsideMargin.bottom}px ${PageSettings.InsideMargin.left}px`,
                      fontWeight: PageSettings.ValueFontStyle.bold ? 'bold' : 'normal',
                      fontSize: PageSettings.ValueFontStyle.size,
                    }}
                  >{`${Storage.Value.Address.ZIPOrPostalCode} ${Storage.Value.Address.City} ${Storage.Value.Address.StreetAddress}`}</p>
                </div>
              </div>

              <div style={{ display: 'flex', flex: 1, flexDirection: 'row' }}>
                <div style={{ display: 'flex', flex: 3, flexDirection: 'row' }}>
                  <p
                    style={{
                      margin: `${PageSettings.InsideMargin.top}px ${PageSettings.InsideMargin.right}px ${PageSettings.InsideMargin.bottom}px ${PageSettings.InsideMargin.left}px`,
                      fontWeight: PageSettings.TitleFontStyle.bold ? 'bold' : 'normal',
                      fontSize: PageSettings.TitleFontStyle.size,
                    }}
                  ></p>
                </div>
                <div style={{ display: 'flex', flex: 7, flexDirection: 'row' }}>
                  <p
                    style={{
                      margin: `${PageSettings.InsideMargin.top}px ${PageSettings.InsideMargin.right}px ${PageSettings.InsideMargin.bottom}px ${PageSettings.InsideMargin.left}px`,
                      fontWeight: PageSettings.ValueFontStyle.bold ? 'bold' : 'normal',
                      fontSize: PageSettings.ValueFontStyle.size,
                    }}
                  ></p>
                </div>
              </div>

              <div style={{ display: 'flex', flex: 1, flexDirection: 'row' }}>
                <div style={{ display: 'flex', flex: 3, flexDirection: 'row' }}>
                  <p
                    style={{
                      margin: `${PageSettings.InsideMargin.top}px ${PageSettings.InsideMargin.right}px ${PageSettings.InsideMargin.bottom}px ${PageSettings.InsideMargin.left}px`,
                      fontWeight: PageSettings.TitleFontStyle.bold ? 'bold' : 'normal',
                      fontSize: PageSettings.TitleFontStyle.size,
                    }}
                  ></p>
                </div>
                <div style={{ display: 'flex', flex: 7, flexDirection: 'row' }}>
                  <p
                    style={{
                      margin: `${PageSettings.InsideMargin.top}px ${PageSettings.InsideMargin.right}px ${PageSettings.InsideMargin.bottom}px ${PageSettings.InsideMargin.left}px`,
                      fontWeight: PageSettings.ValueFontStyle.bold ? 'bold' : 'normal',
                      fontSize: PageSettings.ValueFontStyle.size,
                    }}
                  ></p>
                </div>
              </div>

              <div style={{ display: 'flex', flex: 1, flexDirection: 'row' }}>
                <div style={{ display: 'flex', flex: 3, flexDirection: 'row' }}>
                  <p
                    style={{
                      margin: `${PageSettings.InsideMargin.top}px ${PageSettings.InsideMargin.right}px ${PageSettings.InsideMargin.bottom}px ${PageSettings.InsideMargin.left}px`,
                      fontWeight: PageSettings.TitleFontStyle.bold ? 'bold' : 'normal',
                      fontSize: PageSettings.TitleFontStyle.size,
                    }}
                  ></p>
                </div>
                <div style={{ display: 'flex', flex: 7, flexDirection: 'row' }}>
                  <p
                    style={{
                      margin: `${PageSettings.InsideMargin.top}px ${PageSettings.InsideMargin.right}px ${PageSettings.InsideMargin.bottom}px ${PageSettings.InsideMargin.left}px`,
                      fontWeight: PageSettings.ValueFontStyle.bold ? 'bold' : 'normal',
                      fontSize: PageSettings.ValueFontStyle.size,
                    }}
                  ></p>
                </div>
              </div>

              <div style={{ display: 'flex', flex: 1, flexDirection: 'row' }}>
                <div style={{ display: 'flex', flex: 3, flexDirection: 'row' }}>
                  <p
                    style={{
                      margin: `${PageSettings.InsideMargin.top}px ${PageSettings.InsideMargin.right}px ${PageSettings.InsideMargin.bottom}px ${PageSettings.InsideMargin.left}px`,
                      fontWeight: PageSettings.TitleFontStyle.bold ? 'bold' : 'normal',
                      fontSize: PageSettings.TitleFontStyle.size,
                    }}
                  ></p>
                </div>
                <div style={{ display: 'flex', flex: 7, flexDirection: 'row' }}>
                  <p
                    style={{
                      margin: `${PageSettings.InsideMargin.top}px ${PageSettings.InsideMargin.right}px ${PageSettings.InsideMargin.bottom}px ${PageSettings.InsideMargin.left}px`,
                      fontWeight: PageSettings.ValueFontStyle.bold ? 'bold' : 'normal',
                      fontSize: PageSettings.ValueFontStyle.size,
                    }}
                  ></p>
                </div>
              </div>

              <div style={{ display: 'flex', flex: 1, flexDirection: 'row' }}>
                <div style={{ display: 'flex', flex: 3, flexDirection: 'row' }}>
                  <p
                    style={{
                      margin: `${PageSettings.InsideMargin.top}px ${PageSettings.InsideMargin.right}px ${PageSettings.InsideMargin.bottom}px ${PageSettings.InsideMargin.left}px`,
                      fontWeight: PageSettings.TitleFontStyle.bold ? 'bold' : 'normal',
                      fontSize: PageSettings.TitleFontStyle.size,
                    }}
                  ></p>
                </div>
                <div style={{ display: 'flex', flex: 7, flexDirection: 'row' }}>
                  <p
                    style={{
                      margin: `${PageSettings.InsideMargin.top}px ${PageSettings.InsideMargin.right}px ${PageSettings.InsideMargin.bottom}px ${PageSettings.InsideMargin.left}px`,
                      fontWeight: PageSettings.ValueFontStyle.bold ? 'bold' : 'normal',
                      fontSize: PageSettings.ValueFontStyle.size,
                    }}
                  ></p>
                </div>
              </div>

              <div
                id={'ad-order-header-left-panel-storage-phone-and-fax'}
                style={{ display: 'flex', flex: 1, flexDirection: 'row' }}
              >
                <div style={{ display: 'flex', flex: 3, flexDirection: 'row' }}>
                  <p
                    style={{
                      margin: `${PageSettings.InsideMargin.top}px ${PageSettings.InsideMargin.right}px ${PageSettings.InsideMargin.bottom}px ${PageSettings.InsideMargin.left}px`,
                      fontWeight: PageSettings.TitleFontStyle.bold ? 'bold' : 'normal',
                      fontSize: PageSettings.TitleFontStyle.size,
                    }}
                  >
                    {Storage.Value.Contats.PhoneAndFax.Title}
                  </p>
                </div>
                <div style={{ display: 'flex', flex: 7, flexDirection: 'row' }}>
                  <div style={{ display: 'flex', flex: 1, flexDirection: 'row' }}>
                    <p
                      style={{
                        margin: `${PageSettings.InsideMargin.top}px ${PageSettings.InsideMargin.right}px ${PageSettings.InsideMargin.bottom}px ${PageSettings.InsideMargin.left}px`,
                        fontWeight: PageSettings.ValueFontStyle.bold ? 'bold' : 'normal',
                        fontSize: PageSettings.ValueFontStyle.size,
                      }}
                    >
                      {Storage.Value.Contats.PhoneAndFax.Phone}
                    </p>
                  </div>
                  <div style={{ display: 'flex', flex: 1, flexDirection: 'row' }}>
                    <p
                      style={{
                        margin: `${PageSettings.InsideMargin.top}px ${PageSettings.InsideMargin.right}px ${PageSettings.InsideMargin.bottom}px ${PageSettings.InsideMargin.left}px`,
                        fontWeight: PageSettings.ValueFontStyle.bold ? 'bold' : 'normal',
                        fontSize: PageSettings.ValueFontStyle.size,
                      }}
                    >
                      {Storage.Value.Contats.PhoneAndFax.Fax}
                    </p>
                  </div>
                </div>
              </div>

              <div
                id={'ad-order-header-right-panel-storage-email'}
                style={{ display: 'flex', flex: 1, flexDirection: 'row' }}
              >
                <div style={{ display: 'flex', flex: 3, flexDirection: 'row' }}>
                  <p
                    style={{
                      margin: `${PageSettings.InsideMargin.top}px ${PageSettings.InsideMargin.right}px ${PageSettings.InsideMargin.bottom}px ${PageSettings.InsideMargin.left}px`,
                      fontWeight: PageSettings.TitleFontStyle.bold ? 'bold' : 'normal',
                      fontSize: PageSettings.TitleFontStyle.size,
                    }}
                  >
                    {Storage.Value.Contats.email.Title}
                  </p>
                </div>
                <div style={{ display: 'flex', flex: 7, flexDirection: 'row' }}>
                  <p
                    style={{
                      margin: `${PageSettings.InsideMargin.top}px ${PageSettings.InsideMargin.right}px ${PageSettings.InsideMargin.bottom}px ${PageSettings.InsideMargin.left}px`,
                      fontWeight: PageSettings.ValueFontStyle.bold ? 'bold' : 'normal',
                      fontSize: PageSettings.ValueFontStyle.size,
                    }}
                  >
                    {Storage.Value.Contats.email.Value}
                  </p>
                </div>
              </div>
            </div>

            {/* Header Right Panel */}
            <div id={'ad-order-header-right-panel'} style={{ display: 'flex', flex: 1, flexDirection: 'column' }}>
              {/* Customer */}

              <div
                id={'ad-order-header-right-panel-customer'}
                style={{ display: 'flex', flex: 1, flexDirection: 'row' }}
              >
                <div style={{ display: 'flex', flex: 3, flexDirection: 'row' }}>
                  <p
                    style={{
                      margin: `${PageSettings.InsideMargin.top}px ${PageSettings.InsideMargin.right}px ${PageSettings.InsideMargin.bottom}px ${PageSettings.InsideMargin.left}px`,
                      fontWeight: PageSettings.TitleFontStyle.bold ? 'bold' : 'normal',
                      fontSize: PageSettings.TitleFontStyle.size,
                    }}
                  >{`${Customer.Title}:`}</p>
                </div>
              </div>

              <div
                id={'ad-order-header-right-panel-customer-code-name'}
                style={{ display: 'flex', flex: 1, flexDirection: 'row' }}
              >
                <div style={{ display: 'flex', flex: 3, flexDirection: 'row' }}>
                  <p
                    style={{
                      margin: `${PageSettings.InsideMargin.top}px ${PageSettings.InsideMargin.right}px ${PageSettings.InsideMargin.bottom}px ${PageSettings.InsideMargin.left}px`,
                      fontWeight: PageSettings.TitleFontStyle.bold ? 'bold' : 'normal',
                      fontSize: PageSettings.TitleFontStyle.size,
                    }}
                  >
                    {Customer.Value.Code}
                  </p>
                </div>
                <div style={{ display: 'flex', flex: 7, flexDirection: 'row' }}>
                  <p
                    style={{
                      margin: `${PageSettings.InsideMargin.top}px ${PageSettings.InsideMargin.right}px ${PageSettings.InsideMargin.bottom}px ${PageSettings.InsideMargin.left}px`,
                      fontWeight: PageSettings.ValueFontStyle.bold ? 'bold' : 'normal',
                      fontSize: PageSettings.ValueFontStyle.size,
                    }}
                  >
                    {Customer.Value.Name}
                  </p>
                </div>
              </div>

              <div
                id={'ad-order-header-right-panel-customer-city'}
                style={{ display: 'flex', flex: 1, flexDirection: 'row' }}
              >
                <div style={{ display: 'flex', flex: 3, flexDirection: 'row' }}>
                  <p
                    style={{
                      margin: `${PageSettings.InsideMargin.top}px ${PageSettings.InsideMargin.right}px ${PageSettings.InsideMargin.bottom}px ${PageSettings.InsideMargin.left}px`,
                      fontWeight: PageSettings.TitleFontStyle.bold ? 'bold' : 'normal',
                      fontSize: PageSettings.TitleFontStyle.size,
                    }}
                  ></p>
                </div>
                <div style={{ display: 'flex', flex: 7, flexDirection: 'row' }}>
                  <p
                    style={{
                      margin: `${PageSettings.InsideMargin.top}px ${PageSettings.InsideMargin.right}px ${PageSettings.InsideMargin.bottom}px ${PageSettings.InsideMargin.left}px`,
                      fontWeight: PageSettings.ValueFontStyle.bold ? 'bold' : 'normal',
                      fontSize: PageSettings.ValueFontStyle.size,
                    }}
                  >
                    {Customer.Value.Address.City}
                  </p>
                </div>
              </div>

              <div
                id={'ad-order-header-right-panel-customer-zip'}
                style={{ display: 'flex', flex: 1, flexDirection: 'row' }}
              >
                <div style={{ display: 'flex', flex: 3, flexDirection: 'row' }}>
                  <p
                    style={{
                      margin: `${PageSettings.InsideMargin.top}px ${PageSettings.InsideMargin.right}px ${PageSettings.InsideMargin.bottom}px ${PageSettings.InsideMargin.left}px`,
                      fontWeight: PageSettings.TitleFontStyle.bold ? 'bold' : 'normal',
                      fontSize: PageSettings.TitleFontStyle.size,
                    }}
                  ></p>
                </div>
                <div style={{ display: 'flex', flex: 7, flexDirection: 'row' }}>
                  <p
                    style={{
                      margin: `${PageSettings.InsideMargin.top}px ${PageSettings.InsideMargin.right}px ${PageSettings.InsideMargin.bottom}px ${PageSettings.InsideMargin.left}px`,
                      fontWeight: PageSettings.ValueFontStyle.bold ? 'bold' : 'normal',
                      fontSize: PageSettings.ValueFontStyle.size,
                    }}
                  >
                    {Customer.Value.Address.ZIPOrPostalCode}
                  </p>
                </div>
              </div>

              <div
                id={'ad-order-header-right-panel-customer-country'}
                style={{ display: 'flex', flex: 1, flexDirection: 'row' }}
              >
                <div style={{ display: 'flex', flex: 3, flexDirection: 'row' }}>
                  <p
                    style={{
                      margin: `${PageSettings.InsideMargin.top}px ${PageSettings.InsideMargin.right}px ${PageSettings.InsideMargin.bottom}px ${PageSettings.InsideMargin.left}px`,
                      fontWeight: PageSettings.TitleFontStyle.bold ? 'bold' : 'normal',
                      fontSize: PageSettings.TitleFontStyle.size,
                    }}
                  ></p>
                </div>
                <div style={{ display: 'flex', flex: 7, flexDirection: 'row' }}>
                  <p
                    style={{
                      margin: `${PageSettings.InsideMargin.top}px ${PageSettings.InsideMargin.right}px ${PageSettings.InsideMargin.bottom}px ${PageSettings.InsideMargin.left}px`,
                      fontWeight: PageSettings.ValueFontStyle.bold ? 'bold' : 'normal',
                      fontSize: PageSettings.ValueFontStyle.size,
                    }}
                  >
                    {Customer.Value.Address.CountryOrRegion}
                  </p>
                </div>
              </div>

              <div
                id={'ad-order-header-right-panel-customer-street'}
                style={{ display: 'flex', flex: 1, flexDirection: 'row' }}
              >
                <div style={{ display: 'flex', flex: 3, flexDirection: 'row' }}>
                  <p
                    style={{
                      margin: `${PageSettings.InsideMargin.top}px ${PageSettings.InsideMargin.right}px ${PageSettings.InsideMargin.bottom}px ${PageSettings.InsideMargin.left}px`,
                      fontWeight: PageSettings.TitleFontStyle.bold ? 'bold' : 'normal',
                      fontSize: PageSettings.TitleFontStyle.size,
                    }}
                  ></p>
                </div>
                <div style={{ display: 'flex', flex: 7, flexDirection: 'row' }}>
                  <p
                    style={{
                      margin: `${PageSettings.InsideMargin.top}px ${PageSettings.InsideMargin.right}px ${PageSettings.InsideMargin.bottom}px ${PageSettings.InsideMargin.left}px`,
                      fontWeight: PageSettings.ValueFontStyle.bold ? 'bold' : 'normal',
                      fontSize: PageSettings.ValueFontStyle.size,
                    }}
                  >
                    {Customer.Value.Address.StreetAddress}
                  </p>
                </div>
              </div>

              {/* Site */}

              <div
                id={'ad-order-header-right-panel-site-code'}
                style={{ display: 'flex', flex: 1, flexDirection: 'row' }}
              >
                <div style={{ display: 'flex', flex: 3, flexDirection: 'row' }}>
                  <p
                    style={{
                      margin: `${PageSettings.InsideMargin.top}px ${PageSettings.InsideMargin.right}px ${PageSettings.InsideMargin.bottom}px ${PageSettings.InsideMargin.left}px`,
                      fontWeight: PageSettings.TitleFontStyle.bold ? 'bold' : 'normal',
                      fontSize: PageSettings.TitleFontStyle.size,
                    }}
                  >
                    {Site.Title}
                  </p>
                </div>
                <div style={{ display: 'flex', flex: 7, flexDirection: 'row' }}>
                  <p
                    style={{
                      margin: `${PageSettings.InsideMargin.top}px ${PageSettings.InsideMargin.right}px ${PageSettings.InsideMargin.bottom}px ${PageSettings.InsideMargin.left}px`,
                      fontWeight: PageSettings.ValueFontStyle.bold ? 'bold' : 'normal',
                      fontSize: PageSettings.ValueFontStyle.size,
                    }}
                  >
                    {Site.Value.Code}
                  </p>
                </div>
              </div>

              <div
                id={'ad-order-header-right-panel-site-city'}
                style={{ display: 'flex', flex: 1, flexDirection: 'row' }}
              >
                <div style={{ display: 'flex', flex: 3, flexDirection: 'row' }}>
                  <p
                    style={{
                      margin: `${PageSettings.InsideMargin.top}px ${PageSettings.InsideMargin.right}px ${PageSettings.InsideMargin.bottom}px ${PageSettings.InsideMargin.left}px`,
                      fontWeight: PageSettings.TitleFontStyle.bold ? 'bold' : 'normal',
                      fontSize: PageSettings.TitleFontStyle.size,
                    }}
                  ></p>
                </div>
                <div style={{ display: 'flex', flex: 7, flexDirection: 'row' }}>
                  <p
                    style={{
                      margin: `${PageSettings.InsideMargin.top}px ${PageSettings.InsideMargin.right}px ${PageSettings.InsideMargin.bottom}px ${PageSettings.InsideMargin.left}px`,
                      fontWeight: PageSettings.ValueFontStyle.bold ? 'bold' : 'normal',
                      fontSize: PageSettings.ValueFontStyle.size,
                    }}
                  >
                    {Site.Value.Address.City}
                  </p>
                </div>
              </div>

              <div
                id={'ad-order-header-right-panel-site-street'}
                style={{ display: 'flex', flex: 1, flexDirection: 'row' }}
              >
                <div style={{ display: 'flex', flex: 3, flexDirection: 'row' }}>
                  <p
                    style={{
                      margin: `${PageSettings.InsideMargin.top}px ${PageSettings.InsideMargin.right}px ${PageSettings.InsideMargin.bottom}px ${PageSettings.InsideMargin.left}px`,
                      fontWeight: PageSettings.TitleFontStyle.bold ? 'bold' : 'normal',
                      fontSize: PageSettings.TitleFontStyle.size,
                    }}
                  ></p>
                </div>
                <div style={{ display: 'flex', flex: 7, flexDirection: 'row' }}>
                  <p
                    style={{
                      margin: `${PageSettings.InsideMargin.top}px ${PageSettings.InsideMargin.right}px ${PageSettings.InsideMargin.bottom}px ${PageSettings.InsideMargin.left}px`,
                      fontWeight: PageSettings.ValueFontStyle.bold ? 'bold' : 'normal',
                      fontSize: PageSettings.ValueFontStyle.size,
                    }}
                  >
                    {Site.Value.Address.StreetAddress}
                  </p>
                </div>
              </div>

              <div
                id={'ad-order-header-right-panel-site-zip'}
                style={{ display: 'flex', flex: 1, flexDirection: 'row' }}
              >
                <div style={{ display: 'flex', flex: 3, flexDirection: 'row' }}>
                  <p
                    style={{
                      margin: `${PageSettings.InsideMargin.top}px ${PageSettings.InsideMargin.right}px ${PageSettings.InsideMargin.bottom}px ${PageSettings.InsideMargin.left}px`,
                      fontWeight: PageSettings.TitleFontStyle.bold ? 'bold' : 'normal',
                      fontSize: PageSettings.TitleFontStyle.size,
                    }}
                  ></p>
                </div>
                <div style={{ display: 'flex', flex: 7, flexDirection: 'row' }}>
                  <p
                    style={{
                      margin: `${PageSettings.InsideMargin.top}px ${PageSettings.InsideMargin.right}px ${PageSettings.InsideMargin.bottom}px ${PageSettings.InsideMargin.left}px`,
                      fontWeight: PageSettings.ValueFontStyle.bold ? 'bold' : 'normal',
                      fontSize: PageSettings.ValueFontStyle.size,
                    }}
                  >
                    {Site.Value.Address.ZIPOrPostalCode}
                  </p>
                </div>
              </div>

              <div
                id={'ad-order-header-right-panel-site-phone-and-fax'}
                style={{ display: 'flex', flex: 1, flexDirection: 'row' }}
              >
                <div style={{ display: 'flex', flex: 3, flexDirection: 'row' }}>
                  <p
                    style={{
                      margin: `${PageSettings.InsideMargin.top}px ${PageSettings.InsideMargin.right}px ${PageSettings.InsideMargin.bottom}px ${PageSettings.InsideMargin.left}px`,
                      fontWeight: PageSettings.TitleFontStyle.bold ? 'bold' : 'normal',
                      fontSize: PageSettings.TitleFontStyle.size,
                    }}
                  >
                    {Site.Value.Contats.PhoneAndFax.Title}
                  </p>
                </div>
                <div style={{ display: 'flex', flex: 7, flexDirection: 'row' }}>
                  <div style={{ display: 'flex', flex: 1, flexDirection: 'row' }}>
                    <p
                      style={{
                        margin: `${PageSettings.InsideMargin.top}px ${PageSettings.InsideMargin.right}px ${PageSettings.InsideMargin.bottom}px ${PageSettings.InsideMargin.left}px`,
                        fontWeight: PageSettings.ValueFontStyle.bold ? 'bold' : 'normal',
                        fontSize: PageSettings.ValueFontStyle.size,
                      }}
                    >
                      {Site.Value.Contats.PhoneAndFax.Phone}
                    </p>
                  </div>
                  <div style={{ display: 'flex', flex: 1, flexDirection: 'row' }}>
                    <p
                      style={{
                        margin: `${PageSettings.InsideMargin.top}px ${PageSettings.InsideMargin.right}px ${PageSettings.InsideMargin.bottom}px ${PageSettings.InsideMargin.left}px`,
                        fontWeight: PageSettings.ValueFontStyle.bold ? 'bold' : 'normal',
                        fontSize: PageSettings.ValueFontStyle.size,
                      }}
                    >
                      {Site.Value.Contats.PhoneAndFax.Fax}
                    </p>
                  </div>
                </div>
              </div>

              <div
                id={'ad-order-header-right-panel-site-email'}
                style={{ display: 'flex', flex: 1, flexDirection: 'row' }}
              >
                <div style={{ display: 'flex', flex: 3, flexDirection: 'row' }}>
                  <p
                    style={{
                      margin: `${PageSettings.InsideMargin.top}px ${PageSettings.InsideMargin.right}px ${PageSettings.InsideMargin.bottom}px ${PageSettings.InsideMargin.left}px`,
                      fontWeight: PageSettings.TitleFontStyle.bold ? 'bold' : 'normal',
                      fontSize: PageSettings.TitleFontStyle.size,
                    }}
                  >
                    {Site.Value.Contats.email.Title}
                  </p>
                </div>
                <div style={{ display: 'flex', flex: 7, flexDirection: 'row' }}>
                  <p
                    style={{
                      margin: `${PageSettings.InsideMargin.top}px ${PageSettings.InsideMargin.right}px ${PageSettings.InsideMargin.bottom}px ${PageSettings.InsideMargin.left}px`,
                      fontWeight: PageSettings.ValueFontStyle.bold ? 'bold' : 'normal',
                      fontSize: PageSettings.ValueFontStyle.size,
                    }}
                  >
                    {Site.Value.Contats.email.Value}
                  </p>
                </div>
              </div>
            </div>
          </div>

          {/* Header Info */}
          {/* Date and Certificate Of Origin Title */}

          <div
            id={'ad-order-header-info-date-and-certificate-Of-origin-title'}
            style={{
              borderBottom: '2px solid black',
              backgroundColor: 'lightgray',
              display: 'flex',
              flex: 1,
              flexDirection: 'row',
            }}
          >
            <div style={{ display: 'flex', flex: 1, flexDirection: 'row' }}>
              <div style={{ display: 'flex', flex: 3, flexDirection: 'column' }}>
                <p
                  style={{
                    margin: `${PageSettings.InsideMargin.top}px ${PageSettings.InsideMargin.right}px ${PageSettings.InsideMargin.bottom}px ${PageSettings.InsideMargin.left}px`,
                    fontWeight: PageSettings.TitleFontStyle.bold ? 'bold' : 'normal',
                    fontSize: PageSettings.TitleFontStyle.size,
                  }}
                >
                  {Date.Title}
                </p>
              </div>
              <div style={{ display: 'flex', flex: 17, flexDirection: 'column' }}>
                <p
                  style={{
                    margin: `${PageSettings.InsideMargin.top}px ${PageSettings.InsideMargin.right}px ${PageSettings.InsideMargin.bottom}px ${PageSettings.InsideMargin.left}px`,
                    fontWeight: PageSettings.TitleFontStyle.bold ? 'bold' : 'normal',
                    fontSize: PageSettings.TitleFontStyle.size,
                  }}
                >
                  {CertificateOfOrigin.Title}
                </p>
              </div>
            </div>
          </div>

          {/* Date and Certificate Of Origin Value */}

          <div
            id={'ad-order-header-info-date-and-certificate-Of-origin-value'}
            style={{
              borderBottom: '2px solid black',
              display: 'flex',
              flex: 1,
              flexDirection: 'row',
            }}
          >
            <div style={{ display: 'flex', flex: 1, flexDirection: 'row' }}>
              <div style={{ display: 'flex', flex: 3, flexDirection: 'column' }}>
                <p
                  style={{
                    margin: `${PageSettings.InsideMargin.top}px ${PageSettings.InsideMargin.right}px ${PageSettings.InsideMargin.bottom}px ${PageSettings.InsideMargin.left}px`,
                    fontWeight: PageSettings.ValueFontStyle.bold ? 'bold' : 'normal',
                    fontSize: PageSettings.ValueFontStyle.size,
                  }}
                >
                  {Date.Value}
                </p>
              </div>
              <div style={{ display: 'flex', flex: 17, flexDirection: 'column' }}>
                <p
                  style={{
                    margin: `${PageSettings.InsideMargin.top}px ${PageSettings.InsideMargin.right}px ${PageSettings.InsideMargin.bottom}px ${PageSettings.InsideMargin.left}px`,
                    fontWeight: PageSettings.ValueFontStyle.bold ? 'bold' : 'normal',
                    fontSize: PageSettings.ValueFontStyle.size,
                  }}
                >
                  {CertificateOfOrigin.Value}
                </p>
              </div>
            </div>
          </div>

          {/* Spacer */}

          <div
            style={{
              display: 'flex',
              flex: 1,
              flexDirection: 'column',
            }}
          >
            <p
              style={{
                height: '25px',
              }}
            ></p>
          </div>

          {/* Order / Reference number / Date */}

          <div
            id={'ad-order-header-info-order-reference-number-date'}
            style={{
              display: 'flex',
              flex: 1,
              flexDirection: 'row',
            }}
          >
            <div style={{ display: 'flex', flex: 1, flexDirection: 'column' }}>
              <p
                style={{
                  margin: `${PageSettings.InsideMargin.top}px ${PageSettings.InsideMargin.right}px ${PageSettings.InsideMargin.bottom}px ${PageSettings.InsideMargin.left}px`,
                  fontWeight: PageSettings.TitleFontStyle.bold ? 'bold' : 'normal',
                  fontSize: PageSettings.TitleFontStyle.size,
                }}
              >
                {Order}
              </p>
            </div>
            <div style={{ display: 'flex', flex: 1, flexDirection: 'column' }}>
              <p
                style={{
                  margin: `${PageSettings.InsideMargin.top}px ${PageSettings.InsideMargin.right}px ${PageSettings.InsideMargin.bottom}px ${PageSettings.InsideMargin.left}px`,
                  fontWeight: PageSettings.TitleFontStyle.bold ? 'bold' : 'normal',
                  fontSize: PageSettings.TitleFontStyle.size,
                }}
              >{`${Reference.Title}: ${Reference.Value}`}</p>
            </div>
            <div style={{ display: 'flex', flex: 1, flexDirection: 'column' }}>
              <p
                style={{
                  margin: `${PageSettings.InsideMargin.top}px ${PageSettings.InsideMargin.right}px ${PageSettings.InsideMargin.bottom}px ${PageSettings.InsideMargin.left}px`,
                  fontSize: PageSettings.TitleFontStyle.size,
                }}
              >
                {Date.Value}
              </p>
            </div>
          </div>

          {/* Note */}

          <div
            id={'ad-order-header-info-note'}
            style={{
              display: 'flex',
              flex: 1,
              flexDirection: 'row',
            }}
          >
            <div style={{ display: 'flex', flex: 1, flexDirection: 'row' }}>
              <div style={{ display: 'flex', flex: 3, flexDirection: 'column' }}>
                <p
                  style={{
                    margin: `${PageSettings.InsideMargin.top}px ${PageSettings.InsideMargin.right}px ${PageSettings.InsideMargin.bottom}px ${PageSettings.InsideMargin.left}px`,
                    fontWeight: PageSettings.TitleFontStyle.bold ? 'bold' : 'normal',
                    fontSize: PageSettings.TitleFontStyle.size,
                  }}
                >
                  {Note.Title}
                </p>
              </div>
              <div style={{ display: 'flex', flex: 17, flexDirection: 'column' }}>
                <p
                  style={{
                    margin: `${PageSettings.InsideMargin.top}px ${PageSettings.InsideMargin.right}px ${PageSettings.InsideMargin.bottom}px ${PageSettings.InsideMargin.left}px`,
                    fontWeight: PageSettings.ValueFontStyle.bold ? 'bold' : 'normal',
                    fontSize: PageSettings.ValueFontStyle.size,
                  }}
                >
                  {Note.Value}
                </p>
              </div>
            </div>
          </div>
        </div>

        {/* Order Items header*/}

        <div
          id={'ad-order-items-header'}
          style={{
            backgroundColor: 'lightgray',
            display: 'flex',
            flex: 1,
            flexDirection: 'row',
          }}
        >
          <div style={{ display: 'flex', flex: 1, flexDirection: 'row' }}>
            <div style={{ display: 'flex', flex: 3, flexDirection: 'column' }}>
              <p
                style={{
                  margin: `${PageSettings.InsideMargin.top}px ${PageSettings.InsideMargin.right}px ${PageSettings.InsideMargin.bottom}px ${PageSettings.InsideMargin.left}px`,
                  fontWeight: PageSettings.TitleFontStyle.bold ? 'bold' : 'normal',
                  fontSize: PageSettings.TitleFontStyle.size,
                }}
              >
                {OrderItemHeader.Code}
              </p>
            </div>
            <div style={{ display: 'flex', flex: 7, flexDirection: 'column' }}>
              <p
                style={{
                  margin: `${PageSettings.InsideMargin.top}px ${PageSettings.InsideMargin.right}px ${PageSettings.InsideMargin.bottom}px ${PageSettings.InsideMargin.left}px`,
                  fontWeight: PageSettings.TitleFontStyle.bold ? 'bold' : 'normal',
                  fontSize: PageSettings.TitleFontStyle.size,
                }}
              >
                {OrderItemHeader.Description}
              </p>
            </div>
            <div style={{ display: 'flex', flex: 2, flexDirection: 'column' }}>
              <p
                style={{
                  margin: `${PageSettings.InsideMargin.top}px ${PageSettings.InsideMargin.right}px ${PageSettings.InsideMargin.bottom}px ${PageSettings.InsideMargin.left}px`,
                  fontWeight: PageSettings.TitleFontStyle.bold ? 'bold' : 'normal',
                  fontSize: PageSettings.TitleFontStyle.size,
                }}
              >
                {OrderItemHeader.Quantity}
              </p>
            </div>
            <div style={{ display: 'flex', flex: 2, flexDirection: 'column' }}>
              <p
                style={{
                  margin: `${PageSettings.InsideMargin.top}px ${PageSettings.InsideMargin.right}px ${PageSettings.InsideMargin.bottom}px ${PageSettings.InsideMargin.left}px`,
                  fontWeight: PageSettings.TitleFontStyle.bold ? 'bold' : 'normal',
                  fontSize: PageSettings.TitleFontStyle.size,
                }}
              >
                {OrderItemHeader.Unit}
              </p>
            </div>
            <div style={{ display: 'flex', flex: 2, flexDirection: 'column' }}>
              <p
                style={{
                  margin: `${PageSettings.InsideMargin.top}px ${PageSettings.InsideMargin.right}px ${PageSettings.InsideMargin.bottom}px ${PageSettings.InsideMargin.left}px`,
                  fontWeight: PageSettings.TitleFontStyle.bold ? 'bold' : 'normal',
                  fontSize: PageSettings.TitleFontStyle.size,
                }}
              >
                {OrderItemHeader.UnitPrice}
              </p>
            </div>
            <div style={{ display: 'flex', flex: 2, flexDirection: 'column' }}>
              <p
                style={{
                  margin: `${PageSettings.InsideMargin.top}px ${PageSettings.InsideMargin.right}px ${PageSettings.InsideMargin.bottom}px ${PageSettings.InsideMargin.left}px`,
                  fontWeight: PageSettings.TitleFontStyle.bold ? 'bold' : 'normal',
                  fontSize: PageSettings.TitleFontStyle.size,
                }}
              >
                {OrderItemHeader.Value}
              </p>
            </div>
            <div style={{ display: 'flex', flex: 2, flexDirection: 'column' }}>
              <p
                style={{
                  margin: `${PageSettings.InsideMargin.top}px ${PageSettings.InsideMargin.right}px ${PageSettings.InsideMargin.bottom}px ${PageSettings.InsideMargin.left}px`,
                  fontWeight: PageSettings.TitleFontStyle.bold ? 'bold' : 'normal',
                  fontSize: PageSettings.TitleFontStyle.size,
                }}
              >
                {OrderItemHeader.Currency}
              </p>
            </div>
          </div>
        </div>

        {/* Order Items */}

        {OrderItem.map((item) => {
          return (
            <div
              key={item.id}
              id={'ad-order-item'}
              style={{
                display: 'flex',
                flex: 1,
                flexDirection: 'row',
              }}
            >
              <div style={{ display: 'flex', flex: 1, flexDirection: 'row' }}>
                <div style={{ display: 'flex', flex: 3, flexDirection: 'column' }}>
                  <p
                    style={{
                      margin: `${PageSettings.InsideMargin.top}px ${PageSettings.InsideMargin.right}px ${PageSettings.InsideMargin.bottom}px ${PageSettings.InsideMargin.left}px`,
                      fontWeight: PageSettings.ValueFontStyle.bold ? 'bold' : 'normal',
                      fontSize: PageSettings.ValueFontStyle.size,
                    }}
                  >
                    {item.Code}
                  </p>
                </div>
                <div style={{ display: 'flex', flex: 7, flexDirection: 'column' }}>
                  <p
                    style={{
                      margin: `${PageSettings.InsideMargin.top}px ${PageSettings.InsideMargin.right}px ${PageSettings.InsideMargin.bottom}px ${PageSettings.InsideMargin.left}px`,
                      fontWeight: PageSettings.ValueFontStyle.bold ? 'bold' : 'normal',
                      fontSize: PageSettings.ValueFontStyle.size,
                    }}
                  >
                    {item.Description}
                  </p>
                </div>
                <div style={{ display: 'flex', flex: 2, flexDirection: 'column' }}>
                  <p
                    style={{
                      margin: `${PageSettings.InsideMargin.top}px ${PageSettings.InsideMargin.right}px ${PageSettings.InsideMargin.bottom}px ${PageSettings.InsideMargin.left}px`,
                      fontWeight: PageSettings.ValueFontStyle.bold ? 'bold' : 'normal',
                      fontSize: PageSettings.ValueFontStyle.size,
                    }}
                  >
                    {item.Quantity}
                  </p>
                </div>
                <div style={{ display: 'flex', flex: 2, flexDirection: 'column' }}>
                  <p
                    style={{
                      margin: `${PageSettings.InsideMargin.top}px ${PageSettings.InsideMargin.right}px ${PageSettings.InsideMargin.bottom}px ${PageSettings.InsideMargin.left}px`,
                      fontWeight: PageSettings.ValueFontStyle.bold ? 'bold' : 'normal',
                      fontSize: PageSettings.ValueFontStyle.size,
                    }}
                  >
                    {item.Unit}
                  </p>
                </div>
                <div style={{ display: 'flex', flex: 2, flexDirection: 'column' }}>
                  <p
                    style={{
                      margin: `${PageSettings.InsideMargin.top}px ${PageSettings.InsideMargin.right}px ${PageSettings.InsideMargin.bottom}px ${PageSettings.InsideMargin.left}px`,
                      fontWeight: PageSettings.ValueFontStyle.bold ? 'bold' : 'normal',
                      fontSize: PageSettings.ValueFontStyle.size,
                    }}
                  >
                    {item.UnitPrice}
                  </p>
                </div>
                <div style={{ display: 'flex', flex: 2, flexDirection: 'column' }}>
                  <p
                    style={{
                      margin: `${PageSettings.InsideMargin.top}px ${PageSettings.InsideMargin.right}px ${PageSettings.InsideMargin.bottom}px ${PageSettings.InsideMargin.left}px`,
                      fontWeight: PageSettings.ValueFontStyle.bold ? 'bold' : 'normal',
                      fontSize: PageSettings.ValueFontStyle.size,
                    }}
                  >
                    {item.Value}
                  </p>
                </div>
                <div style={{ display: 'flex', flex: 2, flexDirection: 'column' }}>
                  <p
                    style={{
                      margin: `${PageSettings.InsideMargin.top}px ${PageSettings.InsideMargin.right}px ${PageSettings.InsideMargin.bottom}px ${PageSettings.InsideMargin.left}px`,
                      fontWeight: PageSettings.ValueFontStyle.bold ? 'bold' : 'normal',
                      fontSize: PageSettings.ValueFontStyle.size,
                    }}
                  >
                    {item.Currency}
                  </p>
                </div>
              </div>
            </div>
          );
        })}

        {/* Spacer */}

        <div
          style={{
            display: 'flex',
            flex: 1,
            flexDirection: 'column',
          }}
        >
          <p
            style={{
              height: '25px',
            }}
          ></p>
        </div>

        {/* Order Items Footer */}

        <div
          id={'ad-order-item-footer'}
          style={{
            borderTop: '2px dotted black',
            display: 'flex',
            flex: 1,
            flexDirection: 'row',
          }}
        >
          <div style={{ display: 'flex', flex: 1, flexDirection: 'row' }}>
            <div style={{ display: 'flex', flex: 1, flexDirection: 'column', alignItems: 'flex-end' }}>
              <p
                style={{
                  margin: `${PageSettings.InsideMargin.top}px ${PageSettings.InsideMargin.right}px ${PageSettings.InsideMargin.bottom}px ${PageSettings.InsideMargin.left}px`,
                  fontWeight: PageSettings.TitleFontStyle.bold ? 'bold' : 'normal',
                  fontSize: PageSettings.TitleFontStyle.size,
                }}
              >
                {Total.Title}
              </p>
            </div>
            <div style={{ display: 'flex', flex: 1, flexDirection: 'column' }}>
              <div style={{ display: 'flex', flex: 1, flexDirection: 'row', alignItems: 'center' }}>
                <div style={{ display: 'flex', flex: 2, flexDirection: 'column' }}>
                  <p
                    style={{
                      margin: `${PageSettings.InsideMargin.top}px ${PageSettings.InsideMargin.right}px ${PageSettings.InsideMargin.bottom}px ${PageSettings.InsideMargin.left}px`,
                      fontWeight: PageSettings.ValueFontStyle.bold ? 'bold' : 'normal',
                      fontSize: PageSettings.ValueFontStyle.size,
                    }}
                  >
                    {Total.Quantity}
                  </p>
                </div>
                <div style={{ display: 'flex', flex: 2, flexDirection: 'column' }} />
                <div style={{ display: 'flex', flex: 2, flexDirection: 'column' }} />
                <div style={{ display: 'flex', flex: 2, flexDirection: 'column' }}>
                  <p
                    style={{
                      margin: `${PageSettings.InsideMargin.top}px ${PageSettings.InsideMargin.right}px ${PageSettings.InsideMargin.bottom}px ${PageSettings.InsideMargin.left}px`,
                      fontWeight: PageSettings.ValueFontStyle.bold ? 'bold' : 'normal',
                      fontSize: PageSettings.ValueFontStyle.size,
                    }}
                  >
                    {Total.Value}
                  </p>
                </div>
                <div style={{ display: 'flex', flex: 2, flexDirection: 'column' }}>
                  <p
                    style={{
                      margin: `${PageSettings.InsideMargin.top}px ${PageSettings.InsideMargin.right}px ${PageSettings.InsideMargin.bottom}px ${PageSettings.InsideMargin.left}px`,
                      fontWeight: PageSettings.ValueFontStyle.bold ? 'bold' : 'normal',
                      fontSize: PageSettings.ValueFontStyle.size,
                    }}
                  >
                    {Total.Currency}
                  </p>
                </div>
              </div>
            </div>
          </div>
        </div>

        {/* Spacer */}

        <div
          style={{
            display: 'flex',
            flex: 1,
            flexDirection: 'column',
          }}
        >
          <p
            style={{
              height: '100px',
            }}
          ></p>
        </div>

        {/* Signature */}

        <div
          id={'ad-order-signature'}
          style={{
            display: 'flex',
            flex: 1,
            flexDirection: 'row',
          }}
        >
          <div style={{ display: 'flex', flex: 1, flexDirection: 'row' }}>
            <div
              style={{
                display: 'flex',
                flex: 1,
                flexDirection: 'column',
              }}
            >
              <div
                style={{
                  margin: '0px 150px 0px 150px',
                  borderTop: '2px solid black',
                  display: 'flex',
                  flex: 1,
                  flexDirection: 'column',
                  alignItems: 'center',
                }}
              >
                <div style={{ display: 'flex', flex: 1, flexDirection: 'row' }}>
                  <p
                    style={{
                      margin: `${PageSettings.InsideMargin.top}px ${PageSettings.InsideMargin.right}px ${PageSettings.InsideMargin.bottom}px ${PageSettings.InsideMargin.left}px`,
                      fontWeight: PageSettings.TitleFontStyle.bold ? 'bold' : 'normal',
                      fontSize: PageSettings.TitleFontStyle.size,
                    }}
                  >
                    {Supplier.Title}
                  </p>
                </div>
                <div style={{ display: 'flex', flex: 1, flexDirection: 'row' }}>
                  <p
                    style={{
                      margin: `${PageSettings.InsideMargin.top}px ${PageSettings.InsideMargin.right}px ${PageSettings.InsideMargin.bottom}px ${PageSettings.InsideMargin.left}px`,
                      fontWeight: PageSettings.TitleFontStyle.bold ? 'bold' : 'normal',
                      fontSize: PageSettings.TitleFontStyle.size,
                    }}
                  >
                    {Signature}
                  </p>
                </div>
              </div>
            </div>
            <div
              style={{
                display: 'flex',
                flex: 1,
                flexDirection: 'column',
              }}
            >
              <div
                style={{
                  margin: '0px 150px 0px 150px',
                  borderTop: '2px solid black',
                  display: 'flex',
                  flex: 1,
                  flexDirection: 'column',
                  alignItems: 'center',
                }}
              >
                <div style={{ display: 'flex', flex: 1, flexDirection: 'row' }}>
                  <p
                    style={{
                      margin: `${PageSettings.InsideMargin.top}px ${PageSettings.InsideMargin.right}px ${PageSettings.InsideMargin.bottom}px ${PageSettings.InsideMargin.left}px`,
                      fontWeight: PageSettings.TitleFontStyle.bold ? 'bold' : 'normal',
                      fontSize: PageSettings.TitleFontStyle.size,
                    }}
                  >
                    {Customer.Title}
                  </p>
                </div>
                <div style={{ display: 'flex', flex: 1, flexDirection: 'row' }}>
                  <p
                    style={{
                      margin: `${PageSettings.InsideMargin.top}px ${PageSettings.InsideMargin.right}px ${PageSettings.InsideMargin.bottom}px ${PageSettings.InsideMargin.left}px`,
                      fontWeight: PageSettings.TitleFontStyle.bold ? 'bold' : 'normal',
                      fontSize: PageSettings.TitleFontStyle.size,
                    }}
                  >
                    {Signature}
                  </p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};
