import React from 'react';
import { Meta, Story } from '@storybook/react/types-6-0';
import { usePreviewState } from './usePreviewState';

import { ImagePreview, ImagePreviewProps } from './ImagePreview';

export default {
  title: 'ImagePreview',
  component: ImagePreview,
} as Meta;

export const Interactive: Story<ImagePreviewProps> = (args) => {
  const [photoIndex, setOpen, setClose] = usePreviewState(null);

  return <ImagePreview {...args} photoIndex={photoIndex} setOpen={setOpen} setClose={setClose} />;
};

Interactive.args = {
  images: [
    { src: 'http://placekitten.com/200/300', alt: 'http://placekitten.com/200/300' },
    { src: 'http://placekitten.com/300/200', alt: 'http://placekitten.com/300/200' },
    { src: 'http://placekitten.com/300/300', alt: 'http://placekitten.com/300/300' },
  ],
};
