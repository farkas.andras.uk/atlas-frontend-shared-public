import { Theme } from '@mui/material/styles';

export const sx_config = {
  root: {
    display: 'flex',
    flexWrap: 'wrap',
    justifyContent: 'space-around',
    overflow: 'hidden',
    backgroundColor: (theme: Theme) => theme.palette.background.paper,
    marginTop: (theme: Theme) => theme.spacing(1),
    marginBottom: (theme: Theme) => theme.spacing(1),
  },
  loadingContainer: {
    height: (theme: Theme) => theme.spacing(16),
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
  },
  singleImage: {
    width: '40%',
    marginLeft: (theme: Theme) => theme.spacing(1),
    marginTop: 0,
    marginBottom: 0,
    '& img': {
      maxHeight: '100%',
      width: 'auto !important',
    },
  },
  gridList: {
    flexWrap: 'nowrap',
    transform: 'translateZ(0)',
    width: '100%',
  },
  gridListSingle: {
    margin: '0 !important',
  },
  title: {
    color: (theme: Theme) => theme.palette.primary.light,
  },
  titleBar: {
    background: 'linear-gradient(to top, rgba(0,0,0,0.7) 0%, rgba(0,0,0,0.3) 70%, rgba(0,0,0,0) 100%)',
  },
  gridItem: {
    cursor: 'pointer',
  },
  gridItemSingle: {
    display: 'flex',
    justifyContent: 'center',
    width: '100% !important',
    height: (theme: Theme) => `${theme.spacing(16)} !important`,
    '& .MuiGridListTile-tile': {
      display: 'flex',
      alignItems: 'center',
    },
  },
  gridItemImage: {
    height: 'auto !important',
  },
};
