import { useState } from 'react';

export type UsePreviewState = (initialState: number | null) => [number, (number: number) => void, () => void];

export const usePreviewState: UsePreviewState = () => {
  const [photoIndex, setPhotoIndex] = useState<number | null>(null);

  const setOpen = (index: number) => {
    setPhotoIndex(index);
  };

  const setClose = () => {
    setPhotoIndex(null);
  };

  return [photoIndex, setOpen, setClose];
};
