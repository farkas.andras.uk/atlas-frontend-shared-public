import React, { FC } from 'react';
import ImageList from '@mui/material/ImageList';
import ImageListItem from '@mui/material/ImageListItem';
import CircularProgress from '@mui/material/CircularProgress';
import Box from '@mui/material/Box';
import { ImageViewer, ImageModel } from '../image-viewer/ImageViewer';
import { sx_config } from './ImagePreview.style';

export interface ImagePreviewProps {
  /**
   * images array like [{"src": "http://...", "alt": "Test"}].
   */
  images: ImageModel[];
  /**
   * If `true`, grid is aligned to be able show more image
   */
  multiple: boolean;
  /**
   * If `true`, loading indicator is shown
   */
  loading?: boolean;

  photoIndex?: number;

  setOpen?: (index: number) => void;

  setClose?: () => void;
}

export const ImagePreview: FC<ImagePreviewProps> = ({ images, multiple, loading, photoIndex, setOpen, setClose }) => {
  return (
    <Box
      sx={{
        ...sx_config.root,
        ...(!multiple && { ...sx_config.singleImage }),
      }}
    >
      {loading ? (
        <Box sx={sx_config.loadingContainer}>
          <CircularProgress />
        </Box>
      ) : (
        <>
          {images ? (
            <ImageList
              sx={{
                ...sx_config.gridList,
                ...(!multiple && { ...sx_config.gridListSingle }),
              }}
              cols={multiple ? 2 : 3}
            >
              {images.map((image, index) => (
                <ImageListItem
                  sx={{
                    ...sx_config.gridItem,
                    ...(!multiple && { ...sx_config.gridItemSingle }),
                  }}
                  key={image.src}
                  onClick={() => setOpen(index)}
                >
                  <img
                    style={{
                      ...(!multiple && { ...sx_config.gridItemImage }),
                    }}
                    src={image.src}
                    alt={image.alt}
                  />
                </ImageListItem>
              ))}
            </ImageList>
          ) : null}
          {photoIndex !== null && <ImageViewer images={images} photoIndex={photoIndex} setClose={setClose} />}
        </>
      )}
    </Box>
  );
};
