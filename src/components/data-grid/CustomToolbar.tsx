import React, { useEffect } from 'react';
import {
  GridToolbarContainer,
  GridToolbar,
  GridToolbarQuickFilter,
  useGridApiContext,
  GridColumnsInitialState,
  GridRowModes,
} from '@mui/x-data-grid-premium';
import { randomId } from '@mui/x-data-grid-generator';
import { Button, Box } from '@mui/material';
import { GridToolbarQuickFilterForServerFilterMode } from './GridToolbarQuickFilterForServerFilterMode';
import { Add, Delete, Save, RestartAlt } from '@mui/icons-material';
import { isNil } from 'lodash';

export const CustomToolbar = (props) => {
  const {
    checkboxSelection,
    CRUD,
    getStateForSave,
    gridIdentifier,
    localRowsState,
    paginationAndfilterMode,
    toolbarActionButtons,
    quickFilter,
    quickFilterSearchValue,
    restoreState,
    rowModesModel,
    selectedRows,
    setLocalRowsState,
    setQuickFilterSearchValue,
    setRowModesModel,
  } = props;
  const debounceMs = quickFilter?.debounceMs ? quickFilter.debounceMs : 500;
  const apiRef = useGridApiContext();

  const restoreGridState = () => {
    const gridSpecificRestoreState = restoreState[gridIdentifier];
    if (!isNil(gridSpecificRestoreState)) {
      gridSpecificRestoreState.columns.orderedFields.filter((orderedField) => {
        let allowed = true;
        if (orderedField === '__check__' || orderedField === '__detail_panel_toggle__') {
          allowed = false;
        }
        return allowed;
      });
      const columns: GridColumnsInitialState = {
        ...gridSpecificRestoreState.columns,
        orderedFields: gridSpecificRestoreState.columns.orderedFields,
      };
      apiRef.current.restoreState({
        columns,
      });
    }
  };

  const handleAddClick = () => {
    const id = randomId();
    setLocalRowsState((oldRows) => [{ id, ...CRUD.create.recordPredefinition }, ...oldRows]);
    setRowModesModel((oldModel) => ({
      ...oldModel,
      [id]: {
        mode: GridRowModes.Edit,
        fieldToFocus: CRUD.create.fieldToFocus,
      },
    }));
    setRowModesModel({ ...rowModesModel, [id]: { mode: GridRowModes.View } });
    CRUD.create.createAction(CRUD.create.recordPredefinition);
  };

  const handleDeleteClick = () => {
    const rowDifference = localRowsState.filter((x: any) => !selectedRows.includes(x));
    setLocalRowsState(rowDifference);
    CRUD.delete.deleteAction(selectedRows);
  };

  const saveGridState = () => {
    const gridSpecificSaveState = {
      [gridIdentifier]: apiRef.current.exportState(),
    };
    getStateForSave(gridSpecificSaveState);
  };

  useEffect(() => {
    if (restoreState) {
      restoreGridState();
    }
  }, [restoreState]);

  return (
    <GridToolbarContainer style={{ display: 'flex', alignItems: 'center' }}>
      {toolbarActionButtons}
      <GridToolbar />
      {CRUD.create?.enable && <Button color="primary" startIcon={<Add />} onClick={handleAddClick} />}
      {CRUD.delete?.enable && (
        <Button
          color="primary"
          startIcon={<Delete />}
          onClick={handleDeleteClick}
          disabled={!(checkboxSelection && selectedRows.length > 0)}
        />
      )}
      {gridIdentifier === 'demo' && <Button size="small" startIcon={<RestartAlt />} onClick={restoreGridState} />}
      <Button size="small" startIcon={<Save />} onClick={saveGridState} />
      {quickFilter && (
        <Box
          style={{
            display: 'flex',
            flexDirection: 'column',
            flex: 1,
            alignItems: 'end',
          }}
        >
          {paginationAndfilterMode === 'client' && (
            <GridToolbarQuickFilter
              debounceMs={debounceMs}
              quickFilterParser={(quickFilterSearchInput: string) => {
                const filter = quickFilterSearchInput
                  .split(',')
                  .map((value) => value.trim())
                  .filter((value) => value !== '');
                return filter;
              }}
            />
          )}
          {paginationAndfilterMode === 'server' && (
            <GridToolbarQuickFilterForServerFilterMode
              debounceMs={debounceMs}
              quickFilterSearchValue={quickFilterSearchValue}
              setQuickFilterSearchValue={setQuickFilterSearchValue}
            />
          )}
        </Box>
      )}
    </GridToolbarContainer>
  );
};
