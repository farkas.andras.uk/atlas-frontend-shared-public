import React, { useState, useEffect, useMemo } from 'react';
import { Box, LinearProgress } from '@mui/material';
import { DataGrid as DataGridOriginal, GridFilterModel, GridRowModesModel } from '@mui/x-data-grid';
import {
  DataGridPremium,
  useGridApiRef,
  UncapitalizedGridPremiumSlotsComponent,
  GridRowModel,
  GridSortModel,
  GridLogicOperator,
} from '@mui/x-data-grid-premium';
import { DataGridProps, setQueryOptionsParams } from './DataGridInterfaces';
import { sx_config } from './DataGridGeneral.style';
import { theme } from '../../theme/index';
import { isNil, isEmpty, isNumber } from 'lodash';

import { CustomToolbar } from './CustomToolbar';
import { CustomPagination } from './CustomPagination';
import { CustomNoResultsOverlay } from './CustomNoResultsOverlay';

export const DataGrid = ({
  checkboxSelection,
  consoleLogData,
  CRUD = {
    create: {
      enable: false,
      recordPredefinition: {},
      fieldToFocus: '',
      createAction: async () => Promise.resolve(),
    },
    update: { enable: false, updateAction: async () => Promise.resolve() },
    delete: { enable: false, deleteAction: async () => Promise.resolve() },
  },
  custom_sx_config,
  dataGridHeightCorrection,
  dataLoader,
  detailPanel,
  disableRowSelectionOnClick = false,
  enableCustomPaginator = false,
  enableEmptyState,
  enablePremiumGrid = true,
  enableToolbar = true,
  forcedLoadingState,
  getSelectedRows,
  getStateForSave,
  gridIdentifier,
  initialState,
  initialTableState,
  loadingType,
  onCellClickAction,
  onCellDoubleClickAction,
  pageSizeOptions = [20, 50, 100, 500, 1000],
  paginationAndfilterMode = 'client',
  paginationConfig,
  toolbarActionButtons,
  quickFilter,
  restoreState,
  rowClassByData,
  getRowClassName,
  rows,
  rowUniqueIdentifier,
  ...props
}: DataGridProps) => {
  const apiRef = useGridApiRef();
  const DataGrid = enablePremiumGrid ? DataGridPremium : DataGridOriginal;
  const parentNode: any = document.body.parentNode;

  const [queryOptions, setQueryOptions] = useState<setQueryOptionsParams>({
    ...initialTableState,
    page: 0,
    pageSize: pageSizeOptions[0],
  });
  const [isLoading, setisLoading] = useState(false);
  const [localRowsState, setLocalRowsState] = useState<any[]>([]);
  const [selectedRows, setSelectedRows] = useState<any[]>([]);
  const [rowModesModel, setRowModesModel] = useState<GridRowModesModel>({});
  const [quickFilterSearchValue, setQuickFilterSearchValue] = useState<string>('');

  const loader = async (tableState: setQueryOptionsParams) => {
    setisLoading(true);
    await dataLoader(tableState);
    setisLoading(false);
  };

  useEffect(() => {
    setLocalRowsState(rows);
  }, [rows]);

  useEffect(() => {
    loader(queryOptions);
  }, []);

  useEffect(() => {
    if (!isEmpty(quickFilter) && !isEmpty(quickFilterSearchValue)) {
      const logicOperator = isNil(quickFilter.logicOperator) ? GridLogicOperator.Or : quickFilter.logicOperator;
      const constructFilterItems = !isNil(quickFilter.searchForFields)
        ? quickFilter.searchForFields.map((searchForField) => {
            return {
              field: searchForField,
              id: Math.floor(Math.random() * (99999 - 1) + 1),
              operator: 'contains',
              value: quickFilterSearchValue,
            };
          })
        : [];
      const options = {
        ...queryOptions,
        filterModel: { items: constructFilterItems, logicOperator },
      };
      setQueryOptions(options);
      if (paginationAndfilterMode === 'server') {
        loader(options);
      }
    }
  }, [quickFilterSearchValue]);

  const configuredCustomToolbar = useMemo(
    () => () => {
      return (
        <CustomToolbar
          checkboxSelection={checkboxSelection}
          CRUD={CRUD}
          getStateForSave={getStateForSave}
          gridIdentifier={gridIdentifier}
          localRowsState={localRowsState}
          paginationAndfilterMode={paginationAndfilterMode}
          toolbarActionButtons={toolbarActionButtons}
          quickFilter={quickFilter}
          quickFilterSearchValue={quickFilterSearchValue}
          restoreState={restoreState}
          rowModesModel={rowModesModel}
          selectedRows={selectedRows}
          setLocalRowsState={setLocalRowsState}
          setQuickFilterSearchValue={setQuickFilterSearchValue}
          setRowModesModel={setRowModesModel}
        />
      );
    },
    [restoreState, selectedRows, toolbarActionButtons]
  );

  const slots: Partial<UncapitalizedGridPremiumSlotsComponent> = {
    toolbar: enableToolbar ? configuredCustomToolbar : undefined,
    noResultsOverlay: CustomNoResultsOverlay,
  };
  if (loadingType === 'headerLinear') slots.loadingOverlay = LinearProgress;
  if (enableCustomPaginator) slots.pagination = CustomPagination;
  if (detailPanel?.detailPanelExpandIcon) slots.detailPanelExpandIcon = detailPanel?.detailPanelExpandIcon;
  if (detailPanel?.detailPanelCollapseIcon) slots.detailPanelCollapseIcon = detailPanel?.detailPanelCollapseIcon;

  const clickActionCreator = (action: (rowIdentifier: any) => void) => {
    return action && !CRUD.update?.enable
      ? (params, event) => {
          if (params.field === '__check__' || params.field === '__detail_panel_toggle__') {
            return;
          }
          event.defaultMuiPrevented = true;
          action(params.row);
        }
      : undefined;
  };

  if (consoleLogData) {
    console.log({
      queryOptions,
      quickFilterSearchValue,
      localRowsState,
    });
  }

  return (
    <Box
      sx={{
        height: isNumber(dataGridHeightCorrection)
          ? dataGridHeightCorrection
          : parentNode.clientHeight - dataGridHeightCorrection(theme),
      }}
    >
      <DataGrid
        {...props}
        sx={{ ...sx_config, ...custom_sx_config }}
        apiRef={apiRef}
        checkboxSelection={checkboxSelection}
        disableRowSelectionOnClick={disableRowSelectionOnClick || CRUD.update?.enable}
        filterMode={paginationAndfilterMode}
        getDetailPanelContent={detailPanel?.getDetailPanelContent}
        getDetailPanelHeight={detailPanel?.getDetailPanelHeight}
        getRowClassName={
          getRowClassName
            ? getRowClassName
            : (params) => {
                if (params.row[`${rowClassByData}`]) {
                  return `row-theme--${params.row[`${rowClassByData}`]}`;
                } else return `default-row-color`;
              }
        }
        getRowId={rowUniqueIdentifier ? (row) => row[`${rowUniqueIdentifier}`] : undefined}
        initialState={{
          filter: { filterModel: initialTableState?.filterModel },
          sorting: { sortModel: initialTableState?.sortModel },
          ...initialState,
        }}
        loading={forcedLoadingState ? forcedLoadingState : isLoading}
        onCellClick={clickActionCreator(onCellClickAction)}
        onCellDoubleClick={clickActionCreator(onCellDoubleClickAction)}
        onFilterModelChange={(filterModel: GridFilterModel) => {
          const options = { ...queryOptions, filterModel };
          setQueryOptions(options);
          if (paginationAndfilterMode === 'server') {
            loader(options);
          }
        }}
        onPaginationModelChange={(newPaginationModel) => {
          const { page, pageSize } = newPaginationModel;
          const options = { ...queryOptions, page, pageSize };
          setQueryOptions(options);
          if (paginationAndfilterMode === 'server') {
            loader(options);
          }
        }}
        onProcessRowUpdateError={(err) => {
          console.log(err);
        }}
        onRowSelectionModelChange={(ids) => {
          const selectedIDs = new Set(ids);
          const selectedRows = localRowsState.filter((row) =>
            selectedIDs.has(rowUniqueIdentifier ? row[`${rowUniqueIdentifier}`] : row.id)
          );
          setSelectedRows(selectedRows);
          getSelectedRows ? getSelectedRows(selectedRows) : undefined;
        }}
        onSortModelChange={(sortModel: GridSortModel) => {
          const options = { ...queryOptions, sortModel };
          setQueryOptions(options);
          if (paginationAndfilterMode === 'server') {
            loader(options);
          }
        }}
        pageSizeOptions={pageSizeOptions}
        pagination={paginationConfig?.enabled ? true : undefined}
        paginationMode={paginationAndfilterMode}
        paginationModel={{
          pageSize: queryOptions.pageSize,
          page: queryOptions.page,
        }}
        processRowUpdate={(newRow: GridRowModel, oldRow: GridRowModel) => {
          const updatedRow = { ...newRow };
          if (JSON.stringify(newRow) !== JSON.stringify(oldRow)) {
            setLocalRowsState(localRowsState.map((row) => (row.id === newRow.id ? updatedRow : row)));
            CRUD.update.updateAction(newRow);
          }
          return updatedRow;
        }}
        rowCount={paginationConfig?.rowCount}
        rowModesModel={rowModesModel}
        rows={enableEmptyState ? [] : localRowsState}
        slots={slots}
      />
    </Box>
  );
};
