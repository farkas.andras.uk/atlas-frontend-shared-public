import { ReactNode, JSXElementConstructor } from 'react';
import {
  GridCellParams,
  GridColDef,
  GridFilterModel,
  GridInitialState,
  GridLogicOperator,
  GridRowClassNameParams,
  GridSortDirection,
} from '@mui/x-data-grid';
import { DataGridPremiumProps } from '@mui/x-data-grid-premium';
import { Theme } from '@mui/material/styles';

export interface filterAndSortingParams {
  filterModel?: GridFilterModel;
  sortModel?: { field: string; sort: GridSortDirection }[];
}

export interface setQueryOptionsParams extends filterAndSortingParams {
  page?: number;
  pageSize?: number;
}

export interface DataGridProps extends DataGridPremiumProps {
  checkboxSelection?: boolean;
  columns: GridColDef[];
  consoleLogData?: boolean;
  CRUD?: {
    create?: {
      enable: boolean;
      recordPredefinition: any;
      fieldToFocus: string;
      createAction: (row: any) => Promise<void>;
    };
    update?: { enable: boolean; updateAction: (row: any) => Promise<void> };
    delete?: { enable: boolean; deleteAction: (row: any) => Promise<void> };
  };
  custom_sx_config?: any;
  dataGridHeightCorrection?: number | ((theme: Theme) => number);
  dataLoader: (tableState: setQueryOptionsParams) => Promise<void>;
  detailPanel?: {
    getDetailPanelContent?: (params: any) => ReactNode;
    getDetailPanelHeight?: (params: any) => number | 'auto';
    detailPanelExpandIcon?: JSXElementConstructor<any>;
    detailPanelCollapseIcon?: JSXElementConstructor<any>;
  };
  disableRowSelectionOnClick?: boolean;
  disableSelectionOnClick?: boolean;
  enableCustomPaginator?: boolean;
  enableEmptyState?: boolean;
  enablePremiumGrid?: boolean;
  enableToolbar?: boolean;
  forcedLoadingState?: boolean;
  getRowClassName?: DataGridPremiumProps['getRowClassName'];
  getSelectedRows?: (selectedRows: any[]) => void;
  getStateForSave: (gridState: GridInitialState) => void;
  gridIdentifier: string;
  initialTableState?: filterAndSortingParams;
  loadingType?: 'centerSpinner' | 'headerLinear';
  onCellClickAction?: (rowIdentifier: GridCellParams) => void;
  onCellDoubleClickAction?: (rowIdentifier: GridCellParams) => void;
  pageSizeOptions?: number[];
  paginationAndfilterMode?: 'client' | 'server';
  paginationConfig?: { enabled: boolean; rowCount: number };
  toolbarActionButtons?: ReactNode[];
  quickFilter?: {
    debounceMs?: number;
    searchForFields?: string[];
    logicOperator?: GridLogicOperator;
  };
  restoreState: any;
  rowClassByData?: string;
  rows: any[];
  rowUniqueIdentifier?: string;
  unstable_headerFilters?: boolean;
}
