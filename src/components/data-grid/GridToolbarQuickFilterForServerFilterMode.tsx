import React, { useEffect, useState } from 'react';
import { TextField } from '@mui/material';
import InputAdornment from '@mui/material/InputAdornment';

import SearchIcon from '@mui/icons-material/Search';
import ClearIcon from '@mui/icons-material/Clear';
import { isNil } from 'lodash';

export const GridToolbarQuickFilterForServerFilterMode = (props) => {
  const { setQuickFilterSearchValue, quickFilterSearchValue, debounceMs } = props;
  const [searchValue, setSearchValue] = useState(quickFilterSearchValue);

  useEffect(() => {
    const delayDebounceFn = setTimeout(() => {
      if (!isNil(searchValue)) setQuickFilterSearchValue(searchValue);
    }, debounceMs);

    return () => clearTimeout(delayDebounceFn);
  }, [searchValue]);

  return (
    <TextField
      InputProps={{
        startAdornment: <SearchIcon color="primary" />,
        endAdornment: (
          <InputAdornment position="end" onClick={() => setSearchValue('')}>
            {searchValue ? <ClearIcon color="primary" style={{ cursor: 'pointer' }} /> : <div style={{ width: 24 }} />}
          </InputAdornment>
        ),
      }}
      onChange={(event: React.ChangeEvent<HTMLInputElement>) => {
        setSearchValue(event.target.value);
      }}
      placeholder={'Search...'}
      value={searchValue}
      variant="standard"
    />
  );
};
