import { GridColDef } from '@mui/x-data-grid';

export const generateColumns = (
  columns: GridColDef[],
  options?: {
    enableFiltersForAllColumns?: boolean;
    enableSortingForAllColumns?: boolean;
    enableEditingForAllColumns?: boolean;
  }
) => {
  return columns.map((column) => {
    column = {
      filterable: options.enableFiltersForAllColumns,
      sortable: options.enableSortingForAllColumns,
      editable: options.enableEditingForAllColumns,
      flex: 1,
      ...column,
    };
    if (column.headerClassName) {
      return column;
    } else {
      let headerClassName: string;
      if (column.editable) headerClassName = 'default-editable-header-columns-style';
      return { ...column, headerClassName };
    }
  });
};
