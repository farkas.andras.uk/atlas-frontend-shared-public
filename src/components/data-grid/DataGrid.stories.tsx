import React from 'react';
import type { Meta, StoryObj } from '@storybook/react';
import { useArgs } from '@storybook/client-api';
import Button from '@mui/material/Button';
import { Add } from '@mui/icons-material';
import { Theme } from '@mui/material/styles';
import { DataGrid } from './DataGrid';
import { DataGridProps, setQueryOptionsParams } from './DataGridInterfaces';
import { gridIdentifier, initialTableState, getStateForSave, restoreState } from './Config/State';
import { paginationAndfilterMode, paginationConfig, pageSizeOptions } from './Config/Pagination';
import { CRUD } from './Config/CRUD';
import { columns } from './Config/Columns';
import { custom_sx_config } from './Config/CustomStyle';
import DemoData from './DemoData.json';

const meta: Meta<typeof DataGrid> = {
  title: 'DataGrid',
  component: DataGrid,
  tags: ['autodocs'],
  argTypes: {
    getRowClassName: {
      description: 'If this prop is used then rowClassByData is no effect.',
    },
  },
};

export default meta;
type Story = StoryObj<typeof DataGrid>;

export const Demo: Story = {
  render: (args: DataGridProps) => {
    const [, updateArgs] = useArgs<DataGridProps>();

    const loadList = async (tableState: setQueryOptionsParams) => {
      const randomMillis = Math.floor(Math.random() * 5000);
      await new Promise((resolve) => setTimeout(resolve, randomMillis));
      updateArgs({ rows: DemoData });
    };

    return <DataGrid {...args} dataLoader={loadList} />;
  },
  args: {
    unstable_headerFilters: true,
    checkboxSelection: true,
    columns,
    consoleLogData: true,
    CRUD,
    custom_sx_config,
    dataGridHeightCorrection: (theme: Theme) => theme.mixins.pageHeader.appBar.height,
    detailPanel: {
      getDetailPanelContent: ({ row }) => <div>Row ID: {row.id}</div>,
      getDetailPanelHeight: ({ row }) => 100,
    },
    disableRowSelectionOnClick: true,
    enableCustomPaginator: false,
    enableEmptyState: false,
    enablePremiumGrid: true,
    enableToolbar: true,
    forcedLoadingState: false,
    getSelectedRows: (selectedRows: any[]) => {
      console.log({ selectedRows });
    },
    getStateForSave,
    gridIdentifier,
    initialTableState,
    loadingType: 'centerSpinner',
    onCellClickAction: (onCellClickActionRowIdentifier) => {
      console.log({ onCellClickActionRowIdentifier });
    },
    onCellDoubleClickAction: (onCellDoubleClickActionRowIdentifier) => {
      console.log({ onCellDoubleClickActionRowIdentifier });
    },
    pageSizeOptions,
    paginationAndfilterMode,
    paginationConfig,
    toolbarActionButtons: [
      <Button
        key={'customHeaderActionButton1'}
        color="primary"
        startIcon={<Add />}
        onClick={() => {
          console.log('customHeaderActionButton1 clicked');
        }}
      >
        customHeaderActionButton1
      </Button>,
    ],
    quickFilter: { debounceMs: 500, searchForFields: ['fullName'] },
    restoreState,
    rowClassByData: 'status',
    rows: [],
  },
};
