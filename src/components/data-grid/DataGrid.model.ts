export enum DataGridColumnType {
  ACTIONS = 'actions',
  BOOLEAN = 'boolean',
  DATE = 'date',
  DATETIME = 'dateTime',
  NUMBER = 'number',
  SINGLESELECT = 'singleSelect',
  STRING = 'string',
}
