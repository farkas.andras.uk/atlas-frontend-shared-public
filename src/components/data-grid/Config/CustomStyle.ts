import { Theme } from '@mui/material/styles';

export const custom_sx_config = {
  '& .row-theme--active': {
    backgroundColor: (theme: Theme) => theme.palette.success.light,
    // '&:hover': {
    //   color: (theme: Theme) => theme.palette.primary.contrastText,
    //   backgroundColor: (theme: Theme) => theme.palette.success.dark,
    // },
    // '&.Mui-selected': {
    //   color: (theme: Theme) => theme.palette.primary.contrastText,
    //   backgroundColor: (theme: Theme) => theme.palette.success.main,
    //   '&:hover': {
    //     color: (theme: Theme) => theme.palette.primary.contrastText,
    //     backgroundColor: (theme: Theme) => theme.palette.success.main,
    //   },
    // },
  },
  '& .row-theme--retired': {
    backgroundColor: (theme: Theme) => theme.palette.error.light,
    // '&:hover': {
    //   color: (theme: Theme) => theme.palette.primary.contrastText,
    //   backgroundColor: (theme: Theme) => theme.palette.error.dark,
    // },
  },
  '& .row-theme--noPriceListItemId': {
    backgroundColor: (theme: Theme) => theme.palette.common.grey7d,
    cursor: 'pointer',
  },
  '& .row-theme--1': {
    backgroundColor: (theme: Theme) => theme.palette.action.main,
    '&:hover': {
      backgroundColor: (theme: Theme) => theme.palette.action.hoverDark,
    },
    cursor: 'pointer',
  },
  '& .row-theme--6': {
    backgroundColor: (theme: Theme) => theme.palette.warning.main,
    cursor: 'pointer',
  },
};
