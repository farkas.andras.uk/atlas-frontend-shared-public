import { GridValueGetterParams } from '@mui/x-data-grid';
import { DataGridColumnType } from '../DataGrid.model';
import { generateColumns } from '../DataGridCustomFunctions';

export const columns = generateColumns(
  [
    { field: 'id', headerName: 'ID' },
    {
      field: 'firstName',
      headerName: 'First name',
      type: DataGridColumnType.STRING,
      editable: true,
      /* getApplyQuickFilterFn: (value: string) => {
        console.log({ value });
        return (params: GridCellParams): boolean => {
          return params.value === value;
        };
      }, */
    },
    {
      field: 'lastName',
      headerName: 'Last name',
      type: DataGridColumnType.STRING,
      editable: true,
    },
    {
      field: 'dateOfBirth',
      headerName: 'Date of Birth',
      type: DataGridColumnType.DATE,
      minWidth: 150,
      valueGetter: (params: GridValueGetterParams) => new Date(params.row.dateOfBirth),
      editable: true,
    },
    {
      field: 'age',
      headerName: 'Age',
      type: DataGridColumnType.NUMBER,
      valueGetter: (params: GridValueGetterParams) =>
        new Date().getFullYear() - new Date(params.row.dateOfBirth).getFullYear(),
      editable: true,
    },
    {
      field: 'fullName',
      headerName: 'Full name',
      type: DataGridColumnType.STRING,
      description: 'This column has a value getter and is not sortable.',
      sortable: false,
      valueGetter: (params: GridValueGetterParams) => `${params.row.firstName || ''} ${params.row.lastName || ''}`,
    },
    {
      field: 'status',
      headerName: 'Status',
      type: DataGridColumnType.SINGLESELECT,
      valueOptions: [
        { value: 'active', label: 'active' },
        { value: 'retired', label: 'retired' },
      ],
      editable: true,
    },
    {
      field: 'oscar',
      headerName: 'Has Oscar Award',
      type: DataGridColumnType.BOOLEAN,
      editable: true,
    },
  ],
  { enableFiltersForAllColumns: true, enableSortingForAllColumns: true }
);
