import { GridLogicOperator } from '@mui/x-data-grid';
import { filterAndSortingParams } from '../DataGridInterfaces';

export const gridIdentifier = 'demo';

export const initialTableState: filterAndSortingParams = {
  filterModel: {
    items: [
      { id: 1, field: 'status', value: 'active', operator: 'is' },
      { id: 2, field: 'status', value: 'retired', operator: 'is' },
    ],
    logicOperator: GridLogicOperator.Or,
  },
  sortModel: [{ field: 'lastName', sort: 'desc' }],
};

export const getStateForSave = (gridState) => console.log({ gridState });

export const restoreState = {
  demo: {
    rowGrouping: {
      model: [],
    },
    pinnedColumns: {},
    columns: {
      columnVisibilityModel: {
        __check__: true,
        id: false,
      },
      orderedFields: [
        '__check__',
        '__detail_panel_toggle__',
        'id',
        'lastName',
        'firstName',
        'dateOfBirth',
        'age',
        'fullName',
        'status',
        'oscar',
      ],
      dimensions: {
        id: {
          maxWidth: -1,
          minWidth: 90,
          width: 100,
          flex: 1,
        },
        firstName: {
          maxWidth: -1,
          minWidth: 150,
          width: 100,
          flex: 1,
        },
        lastName: {
          maxWidth: -1,
          minWidth: 150,
          width: 100,
          flex: 1,
        },
        dateOfBirth: {
          maxWidth: -1,
          minWidth: 150,
          width: 100,
          flex: 1,
        },
        age: {
          maxWidth: -1,
          minWidth: 150,
          width: 100,
          flex: 1,
        },
        fullName: {
          maxWidth: -1,
          minWidth: 160,
          width: 100,
          flex: 1,
        },
        status: {
          maxWidth: -1,
          minWidth: 60,
          width: 100,
          flex: 1,
        },
        oscar: {
          maxWidth: -1,
          minWidth: 60,
          width: 100,
          flex: 1,
        },
      },
    },
    preferencePanel: {
      open: false,
    },
    filter: {
      filterModel: {
        items: [],
        logicOperator: GridLogicOperator.And,
        quickFilterValues: [],
        quickFilterLogicOperator: 'and',
      },
    },
    sorting: {
      sortModel: [],
    },
    pagination: {
      pageSize: 20,
      page: 1,
    },
  },
};
