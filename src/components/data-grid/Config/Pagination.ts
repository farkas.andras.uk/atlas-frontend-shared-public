export const paginationAndfilterMode = 'server';

export const paginationConfig = {
  enabled: true,
  rowCount: 10,
};

export const pageSizeOptions = [2, 5, 10, 20, 50, 100, 1000000];
