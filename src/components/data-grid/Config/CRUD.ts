export const CRUD = {
  create: {
    enable: true,
    recordPredefinition: {
      lastName: '',
      firstName: '',
      dateOfBirth: new Date(),
      status: 'active',
      oscar: false,
    },
    fieldToFocus: 'firstName',
    createAction: async (row) => {
      console.log('create', { row });
    },
  },
  update: {
    enable: false,
    updateAction: async (row) => {
      console.log('update', { row });
    },
  },
  delete: {
    enable: true,
    deleteAction: async (row) => {
      console.log('delete', { row });
    },
  },
};
