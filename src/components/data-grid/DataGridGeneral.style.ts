import { Theme } from '@mui/material/styles';

export const sx_config = {
  '& .MuiDataGrid-columnHeaders': {
    '& .MuiDataGrid-columnHeadersInner': {
      color: (theme: Theme) => theme.palette.primary.contrastText,
      backgroundColor: (theme: Theme) => theme.palette.primary.main,
      '& .css-yrdy0g-MuiDataGrid-columnHeaderRow': {
        '& .default-editable-header-columns-style': {
          color: (theme: Theme) => theme.palette.primary.contrastText,
          backgroundColor: (theme: Theme) => theme.palette.success.main,
        },
      },
      '& .MuiDataGrid-headerFilterRow': {
        backgroundColor: (theme: Theme) => theme.palette.primary.contrastText,
        '& .MuiFormLabel-root': {
          color: (theme: Theme) => theme.palette.grey[800],
        },
      },
    },
  },
  '& .default-row-color': {
    backgroundColor: (theme: Theme) => theme.palette.action.main,
    '&:hover': {
      backgroundColor: (theme: Theme) => theme.palette.action.hoverDark,
    },
    cursor: 'pointer',
  },
  // '& .default-editable-header-columns-style': {
  //   color: (theme: Theme) => theme.palette.primary.contrastText,
  //   backgroundColor: (theme: Theme) => theme.palette.success.main,
  // },
  // '& .MuiDataGrid-cell': {
  //   backgroundColor: (theme: Theme) => theme.palette.grey[100],
  //   '&:hover': {
  //     color: (theme: Theme) => theme.palette.primary.main,
  //     backgroundColor: (theme: Theme) => theme.palette.grey[400],
  //   },
  //   cursor: 'pointer',
  // },
  // '& .MuiDataGrid-row': {
  //   backgroundColor: (theme: Theme) => theme.palette.grey[100],
  //   '&:hover': {
  //     color: (theme: Theme) => theme.palette.primary.main,
  //     backgroundColor: (theme: Theme) => theme.palette.grey[400],
  //   },
  //   cursor: 'pointer',
  // },
  '& .MuiCheckbox-root.Mui-checked:not(.MuiCheckbox-indeterminate) svg': {
    backgroundColor: (theme: Theme) => theme.palette.secondary.contrastText,
    borderRadius: 1,
  },
  '& .MuiCheckbox-root.Mui-checked svg': {
    backgroundColor: (theme: Theme) => theme.palette.secondary.contrastText,
    borderRadius: 1,
  },
};
