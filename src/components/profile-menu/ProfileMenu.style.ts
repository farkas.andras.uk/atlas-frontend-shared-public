import { Theme } from '@mui/material/styles';

export const sx_config = {
  avatarContainer: (theme: Theme) => ({
    paddingLeft: theme.mixins.profileMenu.avatarContainer.paddingLeft,
    paddingRight: theme.mixins.profileMenu.avatarContainer.paddingRight,
    paddingBottom: theme.mixins.profileMenu.avatarContainer.paddingBottom,
    paddingTop: theme.mixins.profileMenu.avatarContainer.paddingTop,
    borderRadius: '50%',
  }),
  avatar: (theme: Theme) => ({
    width: theme.mixins.profileMenu.avatar.width,
    height: theme.mixins.profileMenu.avatar.height,
    marginRight: theme.mixins.profileMenu.avatar.marginRight,
    backgroundColor: theme.mixins.profileMenu.avatar.backgroundColor,
    position: 'relative',
  }),
  moreVertIcon: {
    cursor: 'pointer',
  },
};
