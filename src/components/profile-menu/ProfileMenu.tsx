import React, { ReactElement, ReactNode, BaseSyntheticEvent } from 'react';
import Menu from '@mui/material/Menu';
import MenuItem from '@mui/material/MenuItem';
import Avatar from '@mui/material/Avatar';
import Box from '@mui/material/Box';
import { MoreVert } from '@mui/icons-material';
import { sx_config } from './ProfileMenu.style';

export interface MenuItemProps {
  key: number;
  button?: true;
  component?: ReactNode;
  to?: string;
}

export interface ProfileMenuProps {
  profileContent: ReactNode;
  profileIcon: ReactElement;
  overrideProfileIconStyle?: boolean;
  logout: () => void;
  logoutLabel: ReactNode;
  menuItems?: MenuItemProps[];
}

export const ProfileMenu = ({
  profileContent,
  profileIcon,
  logout,
  logoutLabel,
  menuItems,
  overrideProfileIconStyle,
}: ProfileMenuProps) => {
  const [anchorEl, setAnchorEl] = React.useState(null);

  const handleClick = (event: BaseSyntheticEvent) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  const onClickLogout = () => {
    handleClose();
    logout();
  };

  return (
    <>
      <Box display="flex" alignItems="center">
        <Box aria-controls="profile-menu" aria-haspopup="true" sx={sx_config.avatarContainer}>
          <Avatar sx={sx_config.avatar}>
            {React.cloneElement(profileIcon, !overrideProfileIconStyle ? { style: sx_config.avatarContainer } : null)}
          </Avatar>
        </Box>
        <Box mr={2}>{profileContent}</Box>
        <MoreVert onClick={handleClick} sx={sx_config.moreVertIcon} />
      </Box>
      <Menu id="profile-menu" anchorEl={anchorEl} keepMounted open={Boolean(anchorEl)} onClose={handleClose}>
        {menuItems && menuItems.map((menuItem) => <MenuItem key={menuItem.key} {...menuItem} />)}
        <MenuItem onClick={onClickLogout}>{logoutLabel}</MenuItem>
      </Menu>
    </>
  );
};
