import React from 'react';

import { ProfileMenu } from './ProfileMenu';

export default {
  title: 'ProfileMenu',
  component: ProfileMenu,
};

export const Default = (): React.ReactNode => {
  const logout = () => {
    console.log('logout');
  };

  return (
    <div style={{ backgroundColor: 'black', color: 'white', padding: 10 }}>
      <ProfileMenu
        logout={logout}
        logoutLabel="Logout"
        profileContent="Test User"
        overrideProfileIconStyle
        profileIcon={
          <div className="test" style={{ color: 'green' }}>
            P
          </div>
        }
      />
    </div>
  );
};
