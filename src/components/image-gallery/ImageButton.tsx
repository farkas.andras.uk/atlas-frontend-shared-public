import React, { ReactNode, MouseEventHandler, DetailedHTMLProps, ImgHTMLAttributes } from 'react';
import Box from '@mui/material/Box';
import InputLabel from '@mui/material/InputLabel';
import ButtonBase from '@mui/material/ButtonBase';
import { sx_config } from './ImageButton.style';

export interface ImageButtonProps extends DetailedHTMLProps<ImgHTMLAttributes<HTMLImageElement>, HTMLImageElement> {
  label: ReactNode;
  onClick: MouseEventHandler;
  imageClassName?: string;
  src: string;
}

export function ImageButton({ label, onClick, className, imageClassName, ...props }: ImageButtonProps) {
  return (
    <Box className={className}>
      <InputLabel shrink>{label}</InputLabel>
      <Box width="100%" display="flex" justifyContent="center">
        <ButtonBase onClick={onClick}>
          <img
            style={{
              ...sx_config.img,
              ...(imageClassName && { ...(imageClassName as React.CSSProperties) }),
            }}
            {...props}
          />
        </ButtonBase>
      </Box>
    </Box>
  );
}
