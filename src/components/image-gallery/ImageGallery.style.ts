import { Theme, alpha } from '@mui/material/styles';

export const sx_config = {
  navButton: {
    position: 'absolute',
    top: '50%',
    zIndex: 4,
    color: (theme: Theme) => theme.palette.primary.contrastText,
    backgroundColor: (theme: Theme) => alpha(theme.palette.primary.main, 0.75),
    '&:hover': {
      backgroundColor: (theme: Theme) => alpha(theme.palette.primary.main, 1),
    },
  },
  rightNav: {
    right: 0,
  },
};
