import React from 'react';
import ReactImageGallery, { ReactImageGalleryProps } from 'react-image-gallery';
import IconButton from '@mui/material/IconButton';
import KeyboardArrowLeft from '@mui/icons-material/KeyboardArrowLeft';
import KeyboardArrowRight from '@mui/icons-material/KeyboardArrowRight';
import 'react-image-gallery/styles/css/image-gallery.css';
import noImage from '../../assets/images/no-image.png';
import { sx_config } from './ImageGallery.style';
import './ImageGallery.css';

export function ImageGallery({ ...props }: ReactImageGalleryProps) {
  return (
    <ReactImageGallery
      showFullscreenButton={false}
      showPlayButton={false}
      renderLeftNav={(onClick, disabled) => (
        <IconButton sx={sx_config.navButton} onClick={onClick} disabled={disabled}>
          <KeyboardArrowLeft />
        </IconButton>
      )}
      renderRightNav={(onClick, disabled) => (
        <IconButton sx={{ ...sx_config.navButton, ...sx_config.rightNav }} onClick={onClick} disabled={disabled}>
          <KeyboardArrowRight />
        </IconButton>
      )}
      onErrorImageURL={noImage}
      {...props}
    />
  );
}
