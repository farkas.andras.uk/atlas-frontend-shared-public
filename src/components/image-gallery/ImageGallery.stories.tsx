import React from 'react';
import { Meta, Story } from '@storybook/react/types-6-0';

import { ImageGallery, ImageGalleryProps } from './ImageGallery';

export default {
  title: 'ImageGallery',
  component: ImageGallery,
} as Meta;

export const Interactive: Story<ImageGalleryProps> = (args) => <ImageGallery {...args} />;

Interactive.args = {
  items: [
    { original: 'http://placekitten.com/200/300', thumbnail: 'http://placekitten.com/200/300' },
    { original: 'http://placekitten.com/300/200', thumbnail: 'http://placekitten.com/300/200' },
    { original: 'http://placekitten.com/300/300', thumbnail: 'http://placekitten.com/300/300' },
  ],
};
