import React from 'react';
import { Meta, Story } from '@storybook/react/types-6-0';

import { ImageButton, ImageButtonProps } from './ImageButton';

export default {
  title: 'ImageButton',
  component: ImageButton,
} as Meta;

export const Interactive: Story<ImageButtonProps> = (args) => <ImageButton {...args} />;

Interactive.args = {
  src: 'http://placekitten.com/200/300',
};
