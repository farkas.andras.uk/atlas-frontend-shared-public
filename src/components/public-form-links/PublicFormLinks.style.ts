import { Theme } from '@mui/material/styles';

export const sx_config = {
  link: (theme: Theme) => ({
    textDecoration: 'none',
    cursor: 'pointer',
    color: theme.palette.primary.main,
    userSelect: 'none',
    fontStyle: 'italic',
  }),
};
