import React from 'react';
import { Router } from 'react-router-dom';
import { createBrowserHistory } from 'history';

import { ThemeProvider, StyledEngineProvider } from '@mui/material/styles';

import { theme } from '../../theme';
import { PublicFormLinks } from './PublicFormLinks';

const history = createBrowserHistory();

export default {
  title: 'PublicFormLinks',
  component: PublicFormLinks,
};

export const Default = (): React.ReactNode => {
  return (
    <Router history={history}>
      <StyledEngineProvider injectFirst>
        <ThemeProvider theme={theme}>
          <PublicFormLinks links={[{ title: 'Forgot password?' }]} submitLabel="Login" />
        </ThemeProvider>
      </StyledEngineProvider>
    </Router>
  );
};
