import React from 'react';
import { Link } from 'react-router-dom';
import Box from '@mui/material/Box';
import { Button } from '../button/Button';
import { Typography } from '../typography/Typography';
import { ErrorMessage } from '../util/ErrorMessage';
import { sx_config } from './PublicFormLinks.style';
import './PublicFormLinks.css';

interface PublicFormIndividualLinkProps {
  to?: string;
  onClick?: () => void;
  key?: string;
  title: string;
}

export interface PublicFormLinksProps {
  links: PublicFormIndividualLinkProps[];
  submitLabel: string;
  errorMessage?: string;
  loading?: boolean;
}

export const PublicFormLinks: React.FC<PublicFormLinksProps> = ({ errorMessage, links, submitLabel, loading }) => {
  return (
    <>
      <Box>
        <Box mb={1}>
          <ErrorMessage message={errorMessage} notInForm />
        </Box>
        <Button type="submit" fullWidth loading={loading}>
          {submitLabel}
        </Button>
      </Box>
      <Box display="flex" flexDirection="column" alignItems="center" mt={2}>
        {links.map((link) => (
          <Box key={link.to || link.key} mt={2}>
            {link.to ? (
              <Link to={link.to} className={'link'}>
                <Typography variant="body2">{link.title}</Typography>
              </Link>
            ) : (
              <Box onClick={link.onClick} sx={sx_config.link}>
                <Typography variant="body2">{link.title}</Typography>
              </Box>
            )}
          </Box>
        ))}
      </Box>
    </>
  );
};
