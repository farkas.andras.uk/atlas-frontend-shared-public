import React from 'react';
import MuiTooltip, { TooltipProps as MuiTooltipProps } from '@mui/material/Tooltip';
import { Theme } from '@mui/material/styles';

export interface TooltipProps extends MuiTooltipProps {
  ttFontColor?: string;
  ttBGColor?: string;
  ttFontSize?: number;
}

export const Tooltip = (props: TooltipProps) => {
  return (
    <MuiTooltip
      componentsProps={{
        tooltip: {
          sx: {
            color: (theme: Theme) => (props.ttFontColor ? props.ttFontColor : theme.palette.secondary.contrastText),
            backgroundColor: (theme: Theme) => (props.ttBGColor ? props.ttBGColor : theme.palette.primary.main),
            fontSize: props.ttFontSize ? props.ttFontSize : 15,
          },
        },
      }}
      {...props}
    >
      {props.children}
    </MuiTooltip>
  );
};
