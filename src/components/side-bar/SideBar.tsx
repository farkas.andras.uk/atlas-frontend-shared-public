import React, { useState, useEffect, ReactNode } from 'react';
import MuiDrawer from '@mui/material/Drawer';
import { Box } from '@mui/material';
import List from '@mui/material/List';
import Backdrop from '@mui/material/Backdrop';
import IconButton from '@mui/material/IconButton';
import { Typography } from '../../components/typography/Typography';
import { RouteConfigurationModel } from '../../models';
import { NavigationItem, NavigationItemProps } from './NavigationItem';
import { ListItem } from './ListItem';
import { sx_config } from './SideBar.style';

export interface SideBarProps extends Pick<NavigationItemProps, 'accessController'> {
  open: boolean;
  toggleSidebar: () => void;
  closeToggleSidebar: () => void;
  pages: RouteConfigurationModel[];
  bottomIcon: ReactNode;
  closeIcon: ReactNode;
  openSubDrawer?: string | null;
}

export const SideBar = ({
  pages,
  open,
  toggleSidebar,
  closeToggleSidebar,
  accessController,
  bottomIcon,
  closeIcon,
  openSubDrawer,
}: SideBarProps) => {
  const [subDrawerOpen, setSubDrawerOpen] = useState<RouteConfigurationModel | null>(null);

  useEffect(() => {
    if (openSubDrawer && !subDrawerOpen) {
      const page = pages.find(({ path }) => path === openSubDrawer);
      setSubDrawerOpen(page);
    }
  }, [openSubDrawer]);

  const closeSubDrawer = () => setSubDrawerOpen(null);

  const onBackdropClick = () => {
    closeSubDrawer();
    closeToggleSidebar();
  };

  const content = (
    <Box
      sx={{
        display: 'flex',
        flex: 1,
        flexDirection: 'column',
      }}
    >
      <List sx={sx_config.menuContainer}>
        {pages.map((page) =>
          !page.hideAtSideBar ? (
            <NavigationItem
              item={page}
              key={page.key || page.path}
              toggleSubDrawer={(close) => {
                setSubDrawerOpen(subDrawerOpen || close ? null : page);
              }}
              open={open}
              closeToggleSidebar={closeToggleSidebar}
              accessController={accessController}
            />
          ) : null
        )}
      </List>

      <Box mt="auto" mb={3} mx="auto" display="flex" sx={{ flexWrap: 'nowrap' }}>
        {bottomIcon}
      </Box>
    </Box>
  );

  return (
    <>
      <MuiDrawer
        variant="persistent"
        sx={[sx_config.subDrawer, open && sx_config.subDrawerOpened, !open && sx_config.subDrawerClosed]}
        open={!!subDrawerOpen}
        onClose={closeSubDrawer}
        elevation={1}
        ModalProps={{
          keepMounted: true,
        }}
      >
        {subDrawerOpen ? (
          <>
            <Box sx={sx_config.subDrawerTitle}>
              <Box sx={sx_config.subDrawerTitleText}>
                <Typography variant="inherit" uppercase>
                  {subDrawerOpen.title()}
                </Typography>
              </Box>
              <IconButton sx={sx_config.subDrawerCloseIcon} onClick={closeSubDrawer}>
                {closeIcon}
              </IconButton>
            </Box>

            {subDrawerOpen.children
              ?.filter(({ hideAtSideBar }) => !hideAtSideBar)
              .map((child) => (
                <ListItem
                  item={child}
                  key={child.link}
                  textProps={{ color: 'inherit' }}
                  subDrawerItem
                  onClick={closeSubDrawer}
                  closeToggleSidebar={closeToggleSidebar}
                />
              ))}
          </>
        ) : null}
      </MuiDrawer>
      <MuiDrawer
        variant="permanent"
        anchor="left"
        sx={[sx_config.drawer, open && sx_config.drawerOpened, !open && sx_config.drawerClosed]}
        open={open}
        onClose={toggleSidebar}
      >
        {content}
      </MuiDrawer>
      <Backdrop sx={sx_config.backdrop} open={open || !!subDrawerOpen} onClick={onBackdropClick} />
    </>
  );
};

SideBar.defaultProps = {
  open: true,
  pages: [],
};
