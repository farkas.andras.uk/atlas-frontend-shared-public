import { Theme } from '@mui/material/styles';

export const sx_config = {
  drawer: (theme: Theme) => ({
    '& .MuiDrawer-paper': {
      top: theme.mixins.pageHeader.appBar.height,
      height: `calc(100% - ${theme.mixins.pageHeader.appBar.height}px)`,
      backgroundColor: theme.palette.common.white,
      maxWidth: '100%',
      overflowX: 'hidden',
    },
  }),
  drawerOpened: {
    '& .MuiDrawer-paper': (theme: Theme) => ({
      width: theme.mixins.sideBar.fullWidth,
      zIndex: 1211,
      transition: theme.transitions.create('width', {
        easing: theme.transitions.easing.sharp,
        duration: theme.transitions.duration.enteringScreen,
      }),
    }),
  },
  drawerClosed: {
    '& .MuiDrawer-paper': (theme: Theme) => ({
      width: theme.mixins.sideBar.closedWidth,
      transition: theme.transitions.create('width', {
        easing: theme.transitions.easing.sharp,
        duration: theme.transitions.duration.leavingScreen,
      }),
    }),
  },
  subDrawer: (theme: Theme) => ({
    '& .MuiDrawer-paper': {
      top: theme.mixins.pageHeader.appBar.height,
      height: `calc(100% - ${theme.mixins.pageHeader.appBar.height}px)`,
      width: theme.mixins.subSideBar.width,
      backgroundColor: theme.palette.common.white,
      maxWidth: '100%',
      overflowX: 'hidden',
    },
  }),
  subDrawerOpened: {
    '& .MuiDrawer-paper': (theme: Theme) => ({
      left: theme.mixins.sideBar.fullWidth,
    }),
  },
  subDrawerClosed: {
    '& .MuiDrawer-paper': (theme: Theme) => ({
      left: theme.mixins.sideBar.closedWidth,
    }),
  },
  menuContainer: {
    paddingTop: 0,
    paddingBottom: 0,
  },
  subDrawerTitle: {
    display: 'flex',
    justifyContent: 'space-between',
    padding: '10px',
  },
  subDrawerTitleText: {
    display: 'flex',
    alignItems: 'center',
    fontSize: (theme: Theme) => theme.typography.pxToRem(22),
    color: (theme: Theme) => theme.palette.primary.main,
  },
  subDrawerCloseIcon: {
    fontSize: 12,
  },
  backdrop: {
    zIndex: 1199,
  },
};
