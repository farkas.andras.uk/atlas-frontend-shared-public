import React, { useState } from 'react';
import { Router } from 'react-router-dom';
import { createBrowserHistory } from 'history';
import { Home, Settings } from '@mui/icons-material';

import { ThemeProvider, StyledEngineProvider } from '@mui/material/styles';

import { theme } from '../../theme';
import { Button } from '../button/Button';

import { SideBar } from './SideBar';

const history = createBrowserHistory();

export default {
  title: 'SideBar',
  component: SideBar,
};

const AccessController = ({ children }) => {
  return children;
};

export const Default = (): React.ReactNode => {
  const [open, setOpen] = useState(true);

  return (
    <Router history={history}>
      <StyledEngineProvider injectFirst>
        <ThemeProvider theme={theme}>
          <Button onClick={() => setOpen(!open)}>Toggle Sidebar</Button>
          <SideBar
            open={open}
            closeToggleSidebar={() => setOpen(false)}
            pages={[
              { title: () => 'Home', icon: Home },
              { title: () => 'Settings', icon: Settings, children: [{ title: () => 'Settings' }] },
            ]}
            toggleSidebar={() => setOpen(!open)}
            accessController={AccessController}
            bottomIcon={<>B</>}
            closeIcon={<>X</>}
          />
        </ThemeProvider>
      </StyledEngineProvider>
    </Router>
  );
};
