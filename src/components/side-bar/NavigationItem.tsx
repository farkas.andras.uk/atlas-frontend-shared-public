import React, { ComponentType } from 'react';
import Box from '@mui/material/Box';
import Tooltip from '@mui/material/Tooltip';
import { Theme } from '@mui/material/styles';
import useMediaQuery from '@mui/material/useMediaQuery';
import { ConditionalWrapper } from '../../components/util';
import { RouteConfigurationModel } from '../../models';
import { ListItem, ListItemProps } from './ListItem';

export interface NavigationItemProps extends Pick<ListItemProps, 'closeToggleSidebar'> {
  open: boolean;
  item: RouteConfigurationModel;
  toggleSubDrawer: (close?: boolean) => void;
  accessController: ComponentType<any>;
}

export const NavigationItem = ({
  open,
  item,
  toggleSubDrawer,
  closeToggleSidebar,
  accessController: AccessController,
}: NavigationItemProps) => {
  const hasChildren = Boolean(item.children && item.children.length);
  const title = item.title();

  const largeScreen = useMediaQuery((theme: Theme) => theme.breakpoints.up(theme.breakpoints.values.sm));

  return (
    <AccessController allowedFor={item.allowedFor}>
      {hasChildren ? (
        <>
          <ConditionalWrapper
            condition={largeScreen && !open}
            wrapper={(children) => (
              <Tooltip title={<>{title}</>} placement="right">
                <Box width="100%">{children}</Box>
              </Tooltip>
            )}
          >
            <ListItem
              item={item}
              disableGutters
              hasChildren
              onClick={toggleSubDrawer}
              closeToggleSidebar={closeToggleSidebar}
            />
          </ConditionalWrapper>
        </>
      ) : (
        <ConditionalWrapper
          condition={largeScreen && !open}
          wrapper={(children) => (
            <Tooltip title={<>{title}</>} placement="right">
              <div>{children}</div>
            </Tooltip>
          )}
        >
          <ListItem item={item} onClick={() => toggleSubDrawer(true)} closeToggleSidebar={closeToggleSidebar} />
        </ConditionalWrapper>
      )}
    </AccessController>
  );
};
