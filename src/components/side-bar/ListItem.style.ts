import { Theme } from '@mui/material/styles';

export const sx_config = {
  listItem: {
    paddingLeft: (theme: Theme) => theme.mixins.sideBar.icon.paddingLeft,
    paddingRight: (theme: Theme) => theme.mixins.sideBar.icon.paddingRight,
    paddingBottom: (theme: Theme) => theme.mixins.sideBar.icon.paddingBottom,
    paddingTop: (theme: Theme) => theme.mixins.sideBar.icon.paddingTop,
    color: (theme: Theme) => theme.palette.common.black,
    transition: (theme: Theme) => theme.transitions.create('border-left-color'),
    '& .MuiListItemIcon-root': {
      color: (theme: Theme) => theme.palette.dark.contrastText,
      fontSize: 29,
    },
    '& .MuiListItemText-primary': {
      color: (theme: Theme) => theme.palette.common.white,
      fontWeight: (theme: Theme) => theme.typography.fontWeightLight,
    },
    cursor: 'pointer',
  },
  listItemIcon: {
    justifyContent: 'center',
  },
  icon: (theme: Theme) => ({
    backgroundColor: theme.palette.common.white,
    color: theme.palette.common.zltBlue,
    fontSize: theme.mixins.sideBar.icon.size,
    border: `${theme.mixins.sideBar.icon.borderWidth}px solid ${theme.palette.common.white}`,
    borderRadius: theme.mixins.sideBar.icon.borderRadius,
  }),
  root: (theme: Theme) => ({
    '&:hover': {
      '& .MuiSvgIcon-root': {
        backgroundColor: theme.palette.common.zltBlue,
        border: `${theme.mixins.sideBar.icon.borderWidth}px solid ${theme.palette.common.zltBlue}`,
        color: theme.palette.common.white,
      },
    },
    '&.active': {
      // backgroundColor: (theme: Theme) => theme.palette.primary.main,
      '& .MuiSvgIcon-root': {
        backgroundColor: theme.palette.common.zltBlue,
        border: `${theme.mixins.sideBar.icon.borderWidth}px solid ${theme.palette.common.zltBlue}`,
        color: theme.palette.common.white,
      },
      '& .MuiListItemIcon-root': {
        color: (theme: Theme) => theme.palette.common.white,
      },
      '& .MuiListItemText-primary': {
        color: (theme: Theme) => theme.palette.common.white,
        fontWeight: (theme: Theme) => theme.typography.fontWeightMedium,
      },
    },
  }),
  mainSectionText: {
    textAlign: 'left',
  },
  selected: (theme: Theme) => ({
    // backgroundColor: (theme: Theme) => theme.palette.primary.main,
    '& .MuiSvgIcon-root': {
      backgroundColor: theme.palette.common.zltBlue,
      border: `${theme.mixins.sideBar.icon.borderWidth}px solid ${theme.palette.common.zltBlue}`,
      color: theme.palette.common.white,
    },
    '& .MuiListItemIcon-root': {
      color: (theme: Theme) => theme.palette.common.white,
    },
  }),
  subDrawerItem: (theme: Theme) => ({
    paddingLeft: (theme: Theme) => theme.mixins.subSideBar.paddingLeft,
    paddingRight: (theme: Theme) => theme.mixins.subSideBar.paddingRight,
    paddingBottom: (theme: Theme) => theme.mixins.subSideBar.paddingBottom,
    paddingTop: (theme: Theme) => theme.mixins.subSideBar.paddingTop,
    color: theme.palette.common.grey31,
    '&.active, &:hover': {
      backgroundColor: `${theme.palette.common.greyE5} !important`,
    },
  }),
};
