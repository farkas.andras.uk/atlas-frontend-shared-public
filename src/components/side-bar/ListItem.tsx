import React, { useCallback } from 'react';
import { NavLink, useLocation } from 'react-router-dom';
import MuiListItem, { ListItemProps as MuiListItemProps } from '@mui/material/ListItem';
import { ListItemIcon } from '@mui/material';
import { sx_config } from './ListItem.style';

import { RouteConfigurationModel } from '../../models';
import { Typography, TypographyProps } from '../../components/typography/Typography';

export interface ListItemProps extends MuiListItemProps {
  item: RouteConfigurationModel;
  onClick?: () => void;
  hasChildren?: boolean;
  textProps?: TypographyProps;
  subDrawerItem?: boolean;
  closeToggleSidebar: () => void;
}

export const ListItem = ({
  item,
  onClick,
  hasChildren,
  textProps,
  subDrawerItem,
  closeToggleSidebar,
}: ListItemProps) => {
  const Icon = item.icon;
  const title = item.title();
  const location = useLocation();
  const isChildSelected = hasChildren && item.path && location.pathname.includes(item.path);

  const onItemClick = useCallback(() => {
    if (onClick) {
      onClick();
    }
    if (!hasChildren) {
      closeToggleSidebar();
    }
  }, [closeToggleSidebar, onClick, hasChildren]);

  return (
    <MuiListItem
      component={hasChildren ? 'li' : NavLink}
      to={item.link || ''}
      sx={[
        sx_config.root,
        sx_config.listItem,
        isChildSelected && sx_config.selected,
        subDrawerItem && sx_config.subDrawerItem,
      ]}
      onClick={onItemClick}
    >
      {Icon && (
        <ListItemIcon sx={sx_config.listItemIcon}>
          {<Icon color="inherit" fontSize="inherit" sx={sx_config.icon} />}
        </ListItemIcon>
      )}
      <Typography noWrap sx={sx_config.mainSectionText} {...textProps}>
        {title}
      </Typography>
    </MuiListItem>
  );
};
