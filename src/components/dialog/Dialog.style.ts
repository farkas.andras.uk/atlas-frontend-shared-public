import { Theme, alpha } from '@mui/material/styles';

export const sx_config = {
  paperWidthXs: {
    maxWidth: 355,
  },
  hidden: {
    visibility: 'hidden',
  },
  rootComponent: {
    display: 'flex',
    flexDirection: 'column',
    overflowY: 'auto',
  },
  progressContainer: {
    zIndex: 100,
    background: (theme: Theme) => theme.palette.primary.dark,
  },
  title: {
    zIndex: 100,
    color: (theme: Theme) => theme.palette.common.white,
    background: (theme: Theme) => theme.palette.primary.dark,
    paddingTop: (theme: Theme) => theme.spacing(5),
    paddingBottom: (theme: Theme) => theme.spacing(5),
  },
  transparentTitleBg: {
    background: 'transparent',
    color: (theme: Theme) => theme.palette.common.black,
  },
  titleText: {
    width: '100%',
  },
  titleWithClose: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'space-between',
    padding: (theme: Theme) => theme.spacing(1, 2, 1, 3),
  },
  loadingContainer: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    position: 'absolute',
    top: 0,
    left: 0,
    height: '100%',
    width: '100%',
    zIndex: 1,
    background: (theme: Theme) => alpha(theme.palette.common.black, 0.5),
  },
  dialogActions: {
    paddingTop: (theme: Theme) => theme.spacing(1),
    paddingBottom: (theme: Theme) => theme.spacing(3.5),
  },
};
