import React from 'react';

import { ThemeProvider, StyledEngineProvider } from '@mui/material/styles';
import Box from '@mui/material/Box';

import { theme } from '../../theme';

import { Button } from '../button/Button';
import { Dialog } from './Dialog';
import { ConfirmDialog } from './ConfirmDialog';
import { useDialogState } from './useDialogState';
import { Meta, Story } from '@storybook/react/types-6-0';

export default {
  title: 'Dialog',
  component: Dialog,
} as Meta;

export const Interactive: Story = (args) => {
  const [dialogOpen, setOpen, setClose] = useDialogState();
  const [confirmDialogOpen, setOpenConfirm, setCloseConfirm] = useDialogState();

  return (
    <StyledEngineProvider injectFirst>
      <ThemeProvider theme={theme}>
        <Box mb={1}>
          <Button onClick={setOpen}>Open Dialog</Button>
          <Dialog
            {...args}
            title="Dialog Title"
            closeIcon={<>C</>}
            onClose={setClose}
            open={dialogOpen}
            actions={
              <>
                <Button type="submit">Submit</Button>
              </>
            }
          >
            Dialog Content
          </Dialog>
        </Box>

        <Box mb={1}>
          <Button onClick={setOpenConfirm}>Open Confrim Dialog</Button>
          <ConfirmDialog
            {...args}
            title="Confirm Dialog Title"
            closeIcon={<>C</>}
            onClose={setCloseConfirm}
            open={confirmDialogOpen}
            onSubmit={setCloseConfirm}
            confirmTitle="Confirm"
            cancelTitle="Cancel"
          >
            Confirm Dialog Content
          </ConfirmDialog>
        </Box>
      </ThemeProvider>
    </StyledEngineProvider>
  );
};
