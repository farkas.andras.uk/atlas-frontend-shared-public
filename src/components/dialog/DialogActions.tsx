import React, { FC, ReactNode } from 'react';

import Box from '@mui/material/Box';

export interface DialogActionsProps {
  children: ReactNode;
}

export const DialogActions: FC<DialogActionsProps> = ({ children }) => {
  return (
    <Box display="flex" justifyContent="center" width="100%">
      {children}
    </Box>
  );
};
