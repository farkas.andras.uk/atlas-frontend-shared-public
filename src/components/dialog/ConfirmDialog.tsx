import React, { FC, useState } from 'react';

import Box from '@mui/material/Box';

import { Button } from '../button/Button';
import { Dialog, BaseDialogProps, DialogIconProps } from './Dialog';

export interface ConfimrDialogTextsProps extends DialogIconProps {
  confirmTitle?: React.ReactNode;
  cancelTitle?: React.ReactNode;
  loading?: boolean;
}

export interface ConfirmDialogProps extends BaseDialogProps, ConfimrDialogTextsProps {
  onSubmit?: (e: React.SyntheticEvent<any>) => any | null;
}

export const ConfirmDialog: FC<ConfirmDialogProps> = ({
  open,
  title,
  children,
  onSubmit,
  loading: loadingProp,
  confirmTitle,
  cancelTitle,
  onClose,
  ...props
}) => {
  const [loading, setLoading] = useState(false);

  const handleSubmit = async (e: React.SyntheticEvent<any>) => {
    if (onSubmit) {
      setLoading(true);

      try {
        await onSubmit(e);

        if (onClose) {
          onClose(e);
        }
      } finally {
        setLoading(false);
      }
    }
  };

  return (
    <Dialog
      open={open}
      onClose={onClose}
      title={title}
      actions={
        <Box mt={2} display="flex">
          <Box mr={2}>
            <Button variant="outlined" color="secondary" onClick={onClose} disabled={loading || loadingProp}>
              {cancelTitle}
            </Button>
          </Box>
          <Button onClick={handleSubmit} loading={loading || loadingProp}>
            {confirmTitle}
          </Button>
        </Box>
      }
      {...props}
    >
      {children}
    </Dialog>
  );
};
