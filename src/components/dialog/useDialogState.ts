import { useState } from 'react';

export type UseDialogState = (initialOpen?: boolean) => [boolean, () => void, () => void];

export const useDialogState: UseDialogState = (initialOpen = false) => {
  const [dialogOpen, setDialogOpen] = useState(initialOpen);

  const setOpen = () => {
    setDialogOpen(true);
  };

  const setClose = () => {
    setDialogOpen(false);
  };

  return [dialogOpen, setOpen, setClose];
};
