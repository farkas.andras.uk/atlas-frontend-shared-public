import React, { ElementType, ReactNode, SyntheticEvent, ReactElement } from 'react';
import MuiDialog, { DialogProps as MuiDialogProps } from '@mui/material/Dialog';
import DialogTitle from '@mui/material/DialogTitle';
import DialogContent from '@mui/material/DialogContent';
import MuiDialogActions from '@mui/material/DialogActions';
import LinearProgress from '@mui/material/LinearProgress';
import useMediaQuery from '@mui/material/useMediaQuery';
import IconButton from '@mui/material/IconButton';
import Box from '@mui/material/Box';
import { Typography, TypographyProps } from '../typography/Typography';
import { Loader } from '../loader/Loader';
import { DialogActions } from './DialogActions';
import { Theme } from '@mui/material/styles';
import { sx_config } from './Dialog.style';

export interface DialogIconProps {
  loadingLabel?: ReactNode | string;
  closeIcon: ReactElement;
  overrideCloseIconStyle?: boolean;
}

export interface BaseDialogProps {
  title?: ReactNode;
  open: boolean;
  loading?: boolean;
  onClose: (e?: SyntheticEvent<any>, reason?: 'backdropClick' | 'escapeKeyDown') => void;
  titleProps?: TypographyProps;
  transparentTitleBg?: boolean;
  initLoading?: boolean;
  escapeKeyDownDisabled?: boolean;
  backdropClickDisabled?: boolean;
  children?: ReactNode;
}

export interface DialogProps extends Omit<MuiDialogProps, 'title' | 'onClose'>, BaseDialogProps, DialogIconProps {
  actions?: ReactNode;
  contentClass?: string;
  hideCloseButton?: boolean;
  component?: ElementType;
}

export const Dialog = ({
  open,
  component,
  onClose,
  actions,
  title,
  children,
  contentClass,
  hideCloseButton,
  loading,
  maxWidth,
  fullWidth,
  escapeKeyDownDisabled,
  backdropClickDisabled,
  titleProps,
  transparentTitleBg,
  initLoading,
  scroll = 'paper',
  loadingLabel,
  closeIcon,
  overrideCloseIconStyle,
  ...props
}: DialogProps) => {
  const fullScreen = useMediaQuery((theme: Theme) => theme.breakpoints.down('sm'));
  const transparentTitleBgStyle = transparentTitleBg ? sx_config.transparentTitleBg : undefined;

  const RootComponent = component || 'div';
  const showCloseButton = Boolean(!hideCloseButton && onClose);

  return (
    <MuiDialog
      maxWidth={maxWidth || 'sm'}
      fullWidth={fullWidth !== false}
      fullScreen={fullScreen}
      open={open}
      onClose={(e: SyntheticEvent<any>, reason) => {
        switch (reason) {
          case 'backdropClick':
            if (backdropClickDisabled) {
              return;
            } else {
              onClose(e);
            }
            break;
          case 'escapeKeyDown':
            if (escapeKeyDownDisabled) {
              return;
            } else {
              onClose(e);
            }
          default:
            onClose(e);
            break;
        }
      }}
      scroll={scroll}
      /* classes={{ paperWidthXs: classes.paperWidthXs }} */
      {...props}
    >
      <RootComponent sx={sx_config.rootComponent} data-testid="dialog-root-component">
        <Box sx={{ ...sx_config.progressContainer, ...transparentTitleBgStyle }}>
          <LinearProgress sx={!loading && sx_config.hidden} />
        </Box>

        <DialogTitle
          sx={{
            ...sx_config.title,
            ...(showCloseButton && { ...sx_config.titleWithClose }),
            ...(transparentTitleBg && { ...sx_config.transparentTitleBg }),
          }}
        >
          <Typography
            sx={sx_config.titleText}
            variant="body1"
            fontSize={23}
            role="title"
            align="center"
            {...titleProps}
          >
            {title}
          </Typography>

          {showCloseButton ? (
            <IconButton onClick={onClose} edge="end" role="close" size="large">
              {React.cloneElement(closeIcon, !overrideCloseIconStyle ? { fontSize: 'medium' } : null)}
            </IconButton>
          ) : null}
        </DialogTitle>

        <DialogContent className={contentClass}>
          {initLoading ? (
            <>
              <Box sx={sx_config.loadingContainer}>
                <Loader label={loadingLabel} />
              </Box>
              {children}
            </>
          ) : (
            children
          )}
        </DialogContent>
        {actions && (
          <MuiDialogActions sx={sx_config.dialogActions}>
            <DialogActions>{actions}</DialogActions>
          </MuiDialogActions>
        )}
      </RootComponent>
    </MuiDialog>
  );
};
