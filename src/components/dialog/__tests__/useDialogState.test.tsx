import { cleanup, renderHook, act } from '@testing-library/react-hooks';
import { useDialogState } from '../useDialogState';

describe('useWindowResize', () => {
  beforeEach(() => {
    cleanup();
  });

  test('returns an array with three elements, the dialog state, and setOpen, setClose functions', () => {
    const { result } = renderHook(() => useDialogState());

    expect(result.current).toBeInstanceOf(Array);
    expect(result.current.length).toBe(3);

    expect(result.current[0]).toBe(false);

    act(() => {
      result.current[1]();
    });

    expect(result.current[0]).toBe(true);

    act(() => {
      result.current[2]();
    });

    expect(result.current[0]).toBe(false);
  });
});
