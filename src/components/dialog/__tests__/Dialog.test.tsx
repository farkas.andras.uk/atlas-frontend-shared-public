import React from 'react';
import { cleanup, render, queryByText } from '@testing-library/react';

import { ThemeProvider, Theme, StyledEngineProvider } from '@mui/material/styles';

import theme from 'theme';

import { Dialog, DialogProps } from '../Dialog';

/* declare module '@mui/styles/defaultTheme' {
  // eslint-disable-next-line @typescript-eslint/no-empty-interface
  interface DefaultTheme extends Theme {}
} */

const renderComponent = (props: DialogProps) => {
  return render(
    <StyledEngineProvider injectFirst>
      <ThemeProvider theme={theme}>
        <Dialog {...props} />
      </ThemeProvider>
    </StyledEngineProvider>
  );
};

describe('Dialog component', () => {
  beforeEach(() => {
    cleanup();
  });

  test('render a dialog with title and content', () => {
    const element = renderComponent({
      open: true,
      onClose: jest.fn,
      title: 'Title',
      children: <div data-testid="content">Content</div>,
    });

    expect(queryByText(element.getByRole('title'), /Title/i)).toBeInTheDocument();
    expect(queryByText(element.getByTestId('content'), /Content/i)).toBeInTheDocument();
    expect(element.queryByRole('close')).toBeInTheDocument();
  });

  test('hide the close button', () => {
    const element = renderComponent({
      open: true,
      onClose: jest.fn,
      title: 'Title',
      hideCloseButton: true,
      children: <div data-testid="content">Content</div>,
    });

    expect(element.queryByRole('close')).toBeNull();
  });

  test('render a dialog with actions', () => {
    const element = renderComponent({
      open: true,
      onClose: jest.fn,
      title: 'Title',
      children: <div data-testid="content">Content</div>,
      actions: <div data-testid="actions">Actions</div>,
    });

    expect(queryByText(element.getByTestId('actions'), /Actions/i)).toBeInTheDocument();
  });

  test('closed dialog should not render anything', () => {
    const element = renderComponent({
      open: false,
      onClose: jest.fn,
      title: 'Title',
    });

    expect(element.container.firstChild).toBeNull();
  });

  test("Dialog content's root component can be changed", () => {
    const element = renderComponent({
      open: true,
      component: 'form',
      onClose: jest.fn,
      title: 'Title',
    });

    expect(element.getByTestId('dialog-root-component').nodeName).toEqual('FORM');
  });
});
