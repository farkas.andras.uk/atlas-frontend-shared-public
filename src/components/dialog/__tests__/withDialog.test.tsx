import React, { FC } from 'react';
import { render, fireEvent, cleanup, queryByText } from '@testing-library/react';

import { withDialog, WithDialogProps } from '../withDialog';

const TestComponent: FC<WithDialogProps> = ({ openDialog, closeDialog, isOpen, openDialogFactory, dialog }) => {
  const DIALOGS = {
    DIALOG1: 'DIALOG1',
    DIALOG2: 'DIALOG2',
  };

  return (
    <div>
      <div data-testid="current-dialog">{dialog}</div>
      <div data-testid="is-open-dialog-1">{JSON.stringify(isOpen(DIALOGS.DIALOG1))}</div>
      <div data-testid="is-open-dialog-2">{JSON.stringify(isOpen(DIALOGS.DIALOG2))}</div>

      <button onClick={() => openDialog(DIALOGS.DIALOG1)} data-testid="open-dialog">
        Open dialog
      </button>

      <button onClick={openDialogFactory(DIALOGS.DIALOG2)} data-testid="open-dialog-2">
        Open dialog
      </button>

      <button onClick={() => closeDialog()} data-testid="close-dialog">
        Close dialog
      </button>
    </div>
  );
};

const TestComponentWithHoc = withDialog(TestComponent);

describe('withDialog', () => {
  beforeEach(() => {
    cleanup();
  });

  test('opens the given dialog and sets the state accordingly', () => {
    const element = render(<TestComponentWithHoc />);

    expect(element.getByTestId('current-dialog').firstChild).toBe(null);
    expect(queryByText(element.getByTestId('is-open-dialog-1'), /false/i)).toBeInTheDocument();

    fireEvent.click(element.getByTestId('open-dialog'));

    expect(queryByText(element.getByTestId('current-dialog'), /DIALOG1/i)).toBeInTheDocument();
    expect(queryByText(element.getByTestId('is-open-dialog-1'), /true/i)).toBeInTheDocument();
  });

  test('opens the given with the factory function', () => {
    const element = render(<TestComponentWithHoc />);

    expect(element.getByTestId('current-dialog').firstChild).toBe(null);

    expect(queryByText(element.getByTestId('is-open-dialog-2'), /false/i)).toBeInTheDocument();

    fireEvent.click(element.getByTestId('open-dialog-2'));

    expect(queryByText(element.getByTestId('current-dialog'), /DIALOG2/i)).toBeInTheDocument();
    expect(queryByText(element.getByTestId('is-open-dialog-2'), /true/i)).toBeInTheDocument();
  });

  test('close the open dialog', () => {
    const element = render(<TestComponentWithHoc />);

    fireEvent.click(element.getByTestId('open-dialog'));

    expect(queryByText(element.getByTestId('current-dialog'), /DIALOG1/i)).toBeInTheDocument();

    fireEvent.click(element.getByTestId('close-dialog'));

    expect(element.getByTestId('current-dialog').firstChild).toBe(null);
  });
});
