import React from 'react';
import { Subtract } from 'utility-types';

const CLOSED_DIALOG_NAME = '';

export type DIALOG_NAME = string;

export interface WithDialogState<T> {
  dialog: DIALOG_NAME;
  selectedEntity: T | null;
}

export interface WithDialogProps<T = any> {
  openDialog: (dialog: DIALOG_NAME, entity?: T | null) => void;
  openDialogFactory: (dialog: DIALOG_NAME) => (entity?: T | null) => void;
  closeDialog: () => void;
  isOpen: (dialog: DIALOG_NAME) => boolean;
  selectedEntity: T;
  dialog: DIALOG_NAME;
}

export const withDialog = <T extends object = any, P extends WithDialogProps<T> = any>(
  Component: React.ComponentType<P>
) =>
  class WithDialog extends React.Component<Subtract<P, WithDialogProps<T>>, WithDialogState<T>> {
    readonly state: WithDialogState<T> = {
      selectedEntity: null,
      dialog: CLOSED_DIALOG_NAME,
    };

    openDialog = (dialog: DIALOG_NAME, entity?: T | null) => {
      this.setState({ dialog, selectedEntity: entity || null });
    };

    openDialogFactory = (dialog: DIALOG_NAME) => (entity?: T | null) => {
      this.openDialog(dialog, entity);
    };

    closeDialog = () => {
      this.setState({ dialog: CLOSED_DIALOG_NAME, selectedEntity: null });
    };

    isOpen = (dialog: DIALOG_NAME) => {
      return this.state.dialog === dialog;
    };

    render() {
      return (
        <Component
          {...(this.props as P)}
          openDialogFactory={this.openDialogFactory}
          closeDialog={this.closeDialog}
          isOpen={this.isOpen}
          dialog={this.state.dialog}
          openDialog={this.openDialog}
          selectedEntity={this.state.selectedEntity}
        />
      );
    }
  };
