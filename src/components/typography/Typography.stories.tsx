import React from 'react';
import { Meta, Story } from '@storybook/react/types-6-0';

import { Typography, TypographyProps } from './Typography';

export default {
  title: 'Typography',
  component: Typography,
};

export const Interactive: Story<TypographyProps> = (args) => (
  <>
    <Typography {...args}>
      LoremIpsumissimplydummytextoftheprintingandtypesettingindustry.LoremIpsumhasbeentheindustry'sstandarddummytexteversincethe1500s,whenanunknownprintertookagalleyoftypeandscrambledittomakeatype
      specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining
      essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum
      passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem
      Ipsum.
    </Typography>
  </>
);
