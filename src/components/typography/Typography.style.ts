export const sx_config = {
  Wrap: {
    wordBreak: 'break-word',
  },
  Uppercase: {
    textTransform: 'uppercase',
  },
  LowLineHeight: {
    lineHeight: '30px',
  },
  Italic: {
    fontStyle: 'italic',
  },
};
