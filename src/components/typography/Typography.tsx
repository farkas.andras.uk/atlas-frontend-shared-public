import React from 'react';
import MuiTypography, { TypographyProps as MuiTypographyProps } from '@mui/material/Typography';
import { sx_config } from './Typography.style';

export interface TypographyProps extends MuiTypographyProps {
  wrap?: boolean;
  uppercase?: boolean;
  italic?: boolean;
  textDecoration?: 'none' | 'overline' | 'line-through' | 'underline' | 'underline overline';
  fontWeight?: string;
  color?: string;
  lowLineHeight?: boolean;
}

export const Typography = ({
  fontWeight,
  wrap,
  uppercase,
  italic,
  color,
  textDecoration,
  lowLineHeight,
  className,
  ...props
}: TypographyProps) => {
  return (
    <MuiTypography
      sx={[
        wrap && sx_config.Wrap,
        uppercase && sx_config.Uppercase,
        italic && sx_config.Italic,
        lowLineHeight && sx_config.LowLineHeight,
        className && { ...(className as React.CSSProperties) },
      ]}
      style={{
        color,
        textDecoration,
        fontWeight,
      }}
      {...props}
    />
  );
};
