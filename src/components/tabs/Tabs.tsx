import React, { ReactNode, useState, ChangeEvent, useMemo } from 'react';
import MuiTabs, { TabsProps as MuiTabsProps } from '@mui/material/Tabs';
import Tab from '@mui/material/Tab';
import Box from '@mui/material/Box';
import { sx_config } from './Tabs.style';

export interface TabPanelProps {
  children?: ReactNode;
  index?: number;
  value?: number;
  disableContentPadding?: boolean;
}

export const TabPanel = ({ children, value, index, disableContentPadding, ...props }: TabPanelProps) => {
  return (
    <Box
      role="tabpanel"
      hidden={value !== index}
      id={`scrollable-auto-tabpanel-${index}`}
      aria-labelledby={`scrollable-auto-tab-${index}`}
      p={!disableContentPadding ? 3 : 0}
      {...props}
    >
      {value === index ? children : null}
    </Box>
  );
};

export interface TabsHeaderProps extends Omit<MuiTabsProps, 'onChange'> {
  onChange: (newTabIndex: number | boolean) => void;
  className?: string;
}

export const TabsHeader = ({ onChange, ...props }: TabsHeaderProps) => {
  return (
    <MuiTabs
      onChange={(event: ChangeEvent<{}>, newTabIndex: number) => onChange(newTabIndex)}
      indicatorColor="primary"
      textColor="inherit"
      {...props}
    />
  );
};

function getId(index: number, staticItem?: boolean) {
  return {
    id: `${staticItem ? 'tab-' : 'scrollable-auto-tab-'}${index}`,
    'aria-controls': `${staticItem ? 'tabpanel-' : 'scrollable-auto-tabpanel-'}${index}`,
  };
}

export interface TabModel {
  staticItem?: boolean;
  key: string;
  label: ReactNode | string;
  content: ReactNode;
  hidden?: boolean;
  disableContentPadding?: boolean;
}

export interface TabsProps extends MuiTabsProps {
  items: TabModel[];
  createItem?: TabModel | null;
  onChanged?: (tabIndex: number | boolean) => void;
  initTabIndex?: number;
  plusIcon?: ReactNode;
}

const getTabItems = (items: TabModel[], createItem: TabModel, staticItem: boolean) =>
  createItem
    ? [
        ...items,
        {
          ...createItem,
          staticItem,
        },
      ]
    : items;

export const Tabs = ({ items, createItem, onChanged, initTabIndex = 0, plusIcon, ...props }: TabsProps) => {
  const [tabIndex, setTabIndex] = useState<number | boolean>(initTabIndex);
  const [showCreateItem, setShowCreateItem] = useState<boolean>(false);

  const onChange = (newTabIndex: number | boolean) => {
    setTabIndex(newTabIndex);

    if (onChanged) {
      onChanged(newTabIndex);
    }
  };

  const onCreateItem = () => {
    setShowCreateItem(true);
    onChange(createItemIndex);
  };

  const createItemIndex = useMemo(() => (items && items.length) || 1, [items]);
  const staticItems = useMemo(
    () => getTabItems(items, createItem, false).filter((item) => !item.hidden),
    [items, createItem]
  );
  const scrollableItems = useMemo(
    () => getTabItems(items, createItem, !showCreateItem).filter((item) => !item.hidden),
    [items, createItem, showCreateItem]
  );

  return (
    <Box sx={sx_config.root}>
      <Box display="flex">
        <Box sx={sx_config.tabContainer}>
          <TabsHeader {...props} value={tabIndex} sx={sx_config.staticTab} onChange={onChange}>
            {staticItems?.map((item, index) => (
              <Tab
                sx={{
                  ...(!item.staticItem && { ...sx_config.hiddenTab }),
                }}
                key={item.key}
                label={item.label}
                {...getId(index, true)}
              />
            ))}
          </TabsHeader>
        </Box>

        {createItem ? (
          <Box sx={sx_config.createItem} ml={2} mr={2} display="flex" alignItems="center" onClick={onCreateItem}>
            {plusIcon}
          </Box>
        ) : null}

        <TabsHeader
          {...props}
          value={tabIndex}
          variant="scrollable"
          scrollButtons="auto"
          sx={sx_config.scrollableTab}
          onChange={onChange}
        >
          {scrollableItems?.map((item, index) => (
            <Tab
              sx={{
                ...(item.staticItem && { ...sx_config.hiddenTab }),
              }}
              key={item.key}
              label={item.label}
              {...getId(index)}
            />
          ))}
        </TabsHeader>
      </Box>

      {scrollableItems?.map((item, index) => (
        <TabPanel
          key={item.key}
          value={tabIndex as number}
          index={index}
          disableContentPadding={item.disableContentPadding}
        >
          {item.content}
        </TabPanel>
      ))}
    </Box>
  );
};
