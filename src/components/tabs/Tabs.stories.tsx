import React from 'react';
import { Meta, Story } from '@storybook/react/types-6-0';

import PlusOneIcon from '@mui/icons-material/PlusOne';

import { Tabs, TabsProps } from './Tabs';

export default {
  title: 'Tabs',
  component: Tabs,
} as Meta;

export const Interactive: Story<TabsProps> = (args) => (
  <Tabs
    {...args}
    plusIcon={<PlusOneIcon />}
    createItem={{
      key: 'new',
      content: 'New Item',
      label: 'New Item',
    }}
    items={[{ key: 'one', label: 'One', content: 'One content' }]}
  />
);
