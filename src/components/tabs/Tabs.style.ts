import { Theme } from '@mui/material/styles';

export const sx_config = {
  root: (theme: Theme) => ({
    width: '100%',
    backgroundColor: theme.palette.background.paper,
    '& .MuiTabs-root': {
      paddingBottom: `${theme.mixins.tabs.paddingBottom}px`,
      paddingTop: `${theme.mixins.tabs.paddingTop}px`,
      minHeight: theme.mixins.tabs.tabHeight,
    },
    '& .MuiTab-root': {
      color: theme.mixins.tabs.InactiveTabFontColor,
      fontWeight: 'bold',
      opacity: 1,
    },
    '& .Mui-selected': {
      fontWeight: 'bold',
      color: theme.palette.primary.main,
    },
  }),
  tabContainer: {
    color: (theme: Theme) => theme.palette.primary.main,
    '& .MuiTabs-flexContainer': {
      height: '100%',
    },
  },
  scrollableTab: (theme: Theme) => ({
    width: '100%',
    '& .MuiButtonBase-root': {
      height: theme.mixins.tabs.tabHeight,
      minHeight: theme.mixins.tabs.tabHeight,
      backgroundColor: (theme: Theme) => theme.palette.common.white,
    },
  }),
  hiddenTab: {
    display: 'none',
  },
  staticTab: {
    backgroundColor: (theme: Theme) => theme.palette.primary.light,
    height: '100%',
  },
  createItem: {
    cursor: 'pointer',
  },
};
