import React, { FC, ReactNode } from 'react';
import Box from '@mui/material/Box';
import { Typography } from '../typography/Typography';
import { sx_config } from './ExpandToggle.style';

export interface ExpandCollapseModel {
  onClick: () => void;
  label: ReactNode;
}

export interface ExpandToggleProps {
  expand: ExpandCollapseModel;
  collapse: ExpandCollapseModel;
}

export const ExpandToggle: FC<ExpandToggleProps> = ({ expand, collapse }) => {
  return (
    <Box display="flex">
      <Box onClick={expand.onClick} mr={1} sx={sx_config.expandToogle}>
        <Typography variant="body2" color="inherit">
          {expand.label}
        </Typography>
      </Box>
      /
      <Box onClick={collapse.onClick} ml={1} sx={sx_config.expandToogle}>
        <Typography variant="body2" color="inherit">
          {collapse.label}
        </Typography>
      </Box>
    </Box>
  );
};
