import React, { useState } from 'react';

import { ExpandToggle } from './ExpandToggle';

export default {
  title: 'ExpandToggle',
  component: ExpandToggle,
};

export const Default = (): React.ReactNode => {
  const [expand, setExpand] = useState(false);

  return (
    <>
      <ExpandToggle
        expand={{ label: 'Expand All', onClick: () => setExpand(true) }}
        collapse={{ label: 'Collapse All', onClick: () => setExpand(false) }}
      />
      (Just for test) isExpandAll: {`${expand}`}
    </>
  );
};
