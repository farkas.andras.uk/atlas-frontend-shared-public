import { Theme } from '@mui/material/styles';

export const sx_config = {
  expandToogle: {
    cursor: 'pointer',
    userSelect: 'none',
    '&:hover': {
      color: (theme: Theme) => theme.palette.primary.main,
    },
  },
};
