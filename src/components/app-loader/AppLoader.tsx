import React from 'react';
import { Box } from '@mui/material';
import { Loader, LoaderProps } from '../loader/Loader';
import { sx_config } from './AppLoader.style';

export interface AppLoaderProps extends LoaderProps {
  show?: boolean;
}

export const AppLoader = ({ show = false, ...props }: AppLoaderProps) => {
  return (
    show && (
      <Box sx={sx_config.box}>
        <Loader {...props} />
      </Box>
    )
  );
};
