import React from 'react';
import { Meta, Story } from '@storybook/react/types-6-0';

import { AppLoader, AppLoaderProps } from './AppLoader';

export default {
  title: 'AppLoader',
  component: AppLoader,
} as Meta;

export const Interactive: Story<AppLoaderProps> = (args) => <AppLoader {...args} />;

Interactive.args = {
  label: 'Loading...',
  labelProps: {
    variant: 'body1',
    fontWeight: '800',
    color: 'white',
  },
};
