import { Theme, alpha } from '@mui/material/styles';

export const sx_config = {
  box: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    height: '100%',
    position: 'fixed',
    left: 0,
    right: 0,
    top: 0,
    bottom: 0,
    zIndex: 1211,
    backgroundColor: (theme: Theme) => alpha(theme.palette.common.black, 0.9),
  },
};
