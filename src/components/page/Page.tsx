import React, { FC, ReactNode, ReactElement } from 'react';
import { Box } from '@mui/material';
import { PageHeaderProps } from '../page-header/PageHeader';
import { sx_config } from './Page.style';

export interface PageProps {
  title?: ReactNode;
  disableFade?: boolean;
  inScroll?: boolean;
  pageHeaderProps?: PageHeaderProps;
  pageHeader?: ReactElement;
  children?: ReactNode;
}

export const Page: FC<PageProps> = ({ title, children, inScroll, pageHeader, pageHeaderProps }) => {
  return (
    <Box sx={sx_config.container}>
      {React.cloneElement(pageHeader, { title, ...pageHeaderProps })}
      <Box sx={[sx_config.content, inScroll && sx_config.inScroll]}>{children}</Box>
    </Box>
  );
};
