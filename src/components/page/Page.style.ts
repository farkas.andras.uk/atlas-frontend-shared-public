import { Theme } from '@mui/material/styles';

export const sx_config = {
  container: (theme: Theme) => ({
    top: theme.mixins.pageHeader.appBar.height,
    position: 'relative',
  }),
  content: (theme: Theme) => ({
    paddingLeft: theme.mixins.page.paddingLeft,
    paddingRight: theme.mixins.page.paddingRight,
    paddingBottom: `${theme.mixins.page.paddingBottom}px`,
    paddingTop: `${theme.mixins.page.paddingTop}px`,
    overflowY: 'auto',
  }),
  inScroll: (theme: Theme) => ({
    height: `calc(100vh - ${theme.mixins.pageHeader.appBar.height}px - ${theme.mixins.page.paddingBottom}px - ${theme.mixins.page.paddingTop}px)`,
  }),
};
