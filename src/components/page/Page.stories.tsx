import React from 'react';
import { Link } from 'react-router-dom';
import { Box } from '@mui/material';
import { PageHeader } from '../page-header/PageHeader';
import { Page } from './Page';

export default {
  title: 'Page',
  component: Page,
};

export const Default = (): React.ReactNode => {
  const logout = () => {
    console.log('logout');
  };

  return (
    <Page
      inScroll
      title="PageTitle"
      pageHeader={
        <PageHeader
          logoIcon={<>L</>}
          sidebarIcon={<>S</>}
          toggleSidebar={() => {}}
          isSideBarOpen={false}
          profileMenuProps={{
            logout,
            logoutLabel: 'Logout',
            profileContent: 'Test User',
            overrideProfileIconStyle: true,
            profileIcon: (
              <div className="test" style={{ color: 'red' }}>
                P
              </div>
            ),
            menuItems: [
              { key: 1, children: 'Menu 1', component: Link, to: '/test' },
              { key: 2, children: 'Menu 2' },
            ],
          }}
        />
      }
    >
      <Box sx={{ backgroundColor: 'red', width: '100%', height: 5000 }} />
    </Page>
  );
};
