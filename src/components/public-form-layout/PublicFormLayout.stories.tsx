import React from 'react';
import { Router } from 'react-router-dom';
import { createBrowserHistory } from 'history';

import { ThemeProvider, StyledEngineProvider } from '@mui/material/styles';
import Pageview from '@mui/icons-material/Pageview';
import HelpOutline from '@mui/icons-material/HelpOutline';

import { theme } from '../../theme';
import { PublicFormLayout } from './PublicFormLayout';
import { PageGridLayout } from '../page-grid-layout/PageGridLayout';
import { PageHeader } from '../page-header/PageHeader';

const history = createBrowserHistory();

export default {
  title: 'PublicFormLayout',
  component: PublicFormLayout,
};

export const Default = (): React.ReactNode => {
  return (
    <Router history={history}>
      <StyledEngineProvider injectFirst>
        <ThemeProvider theme={theme}>
          <PublicFormLayout
            title="PublicFormLayout"
            pageGridLayout={
              <PageGridLayout
                logoIcon={<Pageview />}
                helpdeskIcon={<HelpOutline />}
                phone={'06301234567'}
                email={'test@irm.com'}
              />
            }
            pageHeader={<PageHeader />}
          />
        </ThemeProvider>
      </StyledEngineProvider>
    </Router>
  );
};
