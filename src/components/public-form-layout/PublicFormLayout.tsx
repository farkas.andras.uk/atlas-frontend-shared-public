import React, { ReactElement } from 'react';
import { PageGridLayoutProps } from '../page-grid-layout/PageGridLayout';
import { PageHeaderProps } from '../page-header/PageHeader';
import Box from '@mui/material/Box';
import { sx_config } from './PublicFormLayout.style';

export interface PublicFormLayoutProps
  extends Omit<PageGridLayoutProps, 'logoIcon' | 'helpdeskIcon' | 'phone' | 'email'> {
  pageHeaderProps?: PageHeaderProps;
  pageHeader: ReactElement;
  pageGridLayout: ReactElement;
}

export const PublicFormLayout = ({ pageHeaderProps, pageHeader, pageGridLayout, ...props }: PublicFormLayoutProps) => {
  return (
    <Box sx={sx_config.root}>
      {React.cloneElement(pageHeader, { unAuthorized: true, ...pageHeaderProps })}
      {React.cloneElement(pageGridLayout, { ...props })}
    </Box>
  );
};
