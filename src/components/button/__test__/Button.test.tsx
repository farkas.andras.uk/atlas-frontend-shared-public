import React from 'react';
import { cleanup, render } from '@testing-library/react';

import { Button } from '../Button';

describe('Button component', () => {
  beforeEach(() => {
    cleanup();
  });

  test('render a base button', () => {
    const element = render(<Button>Button label</Button>);

    expect(element.queryByText(/Button label/i)).toBeInTheDocument();
  });

  test('render a button with loading state', () => {
    const element = render(<Button loading>Button label</Button>);

    expect(element.getByRole('progressbar')).toBeInTheDocument();
  });
});
