import { Theme } from '@mui/material/styles';

export const sx_config = {
  root: {
    position: 'relative',
    display: 'inline-block',
  },
  buttonProgress: {
    color: (theme: Theme) => theme.palette.primary.main,
    position: 'absolute',
    top: 'calc(50% - 12px)',
    right: 'calc(50% - 12px)',
  },
  fullWidth: {
    width: '100%',
  },
  buttonText: {
    height: 22,
    letterSpacing: 0.4,
  },
};
