import React from 'react';
import type { Meta, StoryObj } from '@storybook/react';
import { Button } from './Button';

const meta: Meta<typeof Button> = {
  title: 'Button',
  component: Button,
  tags: ['autodocs'],
};

export default meta;
type Story = StoryObj<typeof Button>;

export const Demo: Story = {
  args: {
    // buttonClassName: { background: 'blue', paddingLeft: 48.5, paddingRight: 48.5 },
    fullWidth: false,
    children: 'Submit',
  },
};
