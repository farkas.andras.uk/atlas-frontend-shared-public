import React, { FC, forwardRef, Ref } from 'react';
import { Box, CircularProgress } from '@mui/material';
import MuiButton, { ButtonProps as MuiButtonProps } from '@mui/material/Button';
import { Typography, TypographyProps } from '../typography/Typography';
import { sx_config } from './Button.style';

export interface ButtonProps extends MuiButtonProps {
  buttonClassName?: React.CSSProperties;
  loading?: boolean;
  textProps?: TypographyProps;
  fullWidth?: boolean;
  className?: string;
  [key: string]: any;
}

export const Button: FC<ButtonProps> = forwardRef(
  (
    { loading, disabled, className, fullWidth, buttonClassName, variant, color, children, textProps, ...props },
    ref: Ref<any>
  ) => {
    return (
      <Box ref={ref} sx={{ ...sx_config.root, ...(fullWidth && { ...sx_config.fullWidth }) }}>
        <MuiButton
          disabled={loading || disabled}
          sx={{ ...(buttonClassName && { ...buttonClassName }) }}
          fullWidth={fullWidth}
          variant={variant}
          color={color}
          {...props}
        >
          <Typography variant="button" sx={sx_config.buttonText} {...textProps}>
            {children}
          </Typography>
        </MuiButton>
        {loading && <CircularProgress size={24} sx={sx_config.buttonProgress} />}
      </Box>
    );
  }
);

Button.defaultProps = {
  variant: 'contained',
  color: 'primary',
};
