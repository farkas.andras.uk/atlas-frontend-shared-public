import { Theme } from '@mui/material/styles';

export const sx_config = {
  formContainer: (theme: Theme) => ({
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'center',
    padding: theme.spacing(10, 4, 5, 4),
    [theme.breakpoints.down(theme.breakpoints.values.lg)]: {
      paddingTop: theme.spacing(5),
    },
  }),
  title: (theme: Theme) => ({
    marginTop: theme.spacing(5),
    [theme.breakpoints.down(theme.breakpoints.values.lg)]: {
      marginTop: theme.spacing(1),
    },
  }),
  content: (theme: Theme) => ({
    marginTop: theme.spacing(10),
    marginBottom: theme.spacing(8),
    [theme.breakpoints.down(theme.breakpoints.values.lg)]: {
      marginTop: 0,
      marginBottom: theme.spacing(2),
    },
  }),
  footer: (theme: Theme) => ({
    marginTop: (theme: Theme) => theme.spacing(10),
    [theme.breakpoints.down(theme.breakpoints.values.lg)]: {
      marginTop: theme.spacing(7),
    },
  }),
};
