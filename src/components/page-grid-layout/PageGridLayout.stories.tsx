import React from 'react';
import { Router } from 'react-router-dom';
import { createBrowserHistory } from 'history';

import { ThemeProvider, StyledEngineProvider } from '@mui/material/styles';
import Pageview from '@mui/icons-material/Pageview';
import HelpOutline from '@mui/icons-material/HelpOutline';

import { theme } from '../../theme';
import { PageGridLayout } from './PageGridLayout';

const history = createBrowserHistory();

export default {
  title: 'PageGridLayout',
  component: PageGridLayout,
};

export const Default = (): React.ReactNode => {
  return (
    <Router history={history}>
      <StyledEngineProvider injectFirst>
        <ThemeProvider theme={theme}>
          <PageGridLayout
            title="PageTitle"
            logoIcon={<Pageview />}
            helpdeskIcon={<HelpOutline />}
            phone={'06301234567'}
            email={'test@irm.com'}
          />
        </ThemeProvider>
      </StyledEngineProvider>
    </Router>
  );
};
