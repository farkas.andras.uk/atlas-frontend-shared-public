import React, { ReactNode, ReactElement } from 'react';
import { Theme } from '@mui/material/styles';
import { Box, Grid, GridSize } from '@mui/material';
import { Typography } from '../typography/Typography';
import { sx_config } from './PageGridLayout.style';

export interface PageGridLayoutProps {
  title?: string | ReactNode;
  hideLogo?: boolean;
  wide?: boolean;
  overrideLogoIconStyle?: boolean;
  logoIcon: ReactElement;
  helpdeskIcon?: ReactNode;
  phone?: string;
  email?: string;
  children?: ReactNode;
}

export const PageGridLayout: React.FC<PageGridLayoutProps> = ({
  children,
  title,
  hideLogo,
  wide,
  logoIcon,
  overrideLogoIconStyle,
  helpdeskIcon,
  email,
  phone,
}) => {
  const getWideValue = (notWideValue: GridSize, wideValue?: GridSize) => (wide ? wideValue || 12 : notWideValue);
  const parentNode: any = document.body.parentNode;

  return (
    <Grid
      container
      justifyContent="center"
      sx={{
        height: (theme: Theme) =>
          parentNode.clientHeight -
          theme.mixins.pageHeader.appBar.height -
          theme.mixins.pageHeader.paddingTop -
          theme.mixins.pageHeader.paddingBottom,
      }}
    >
      <Grid item xs={12} sm={getWideValue(9)} md={getWideValue(5)} lg={getWideValue(3, 8)} sx={sx_config.formContainer}>
        <Box sx={sx_config.content} display="flex" justifyContent="center" flexDirection="column" alignItems="center">
          {!hideLogo
            ? React.cloneElement(logoIcon, !overrideLogoIconStyle ? { fontSize: wide ? 'xxLarge' : 'xLarge' } : null)
            : null}
          {title ? (
            <Box sx={sx_config.title}>
              <Typography variant="h5" align="center">
                {title}
              </Typography>
            </Box>
          ) : null}
        </Box>
        {children}
        {helpdeskIcon && (
          <Box sx={sx_config.footer} display="flex" alignItems="center" justifyContent="center">
            {helpdeskIcon}
            <Box ml={1}>
              <Box>
                <Typography color="#839093" variant="body2">
                  {phone}
                </Typography>
              </Box>
              <Box>
                <Typography color="#1C5B95" variant="body2">
                  {email}
                </Typography>
              </Box>
            </Box>
          </Box>
        )}
      </Grid>
    </Grid>
  );
};
