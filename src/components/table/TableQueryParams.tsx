import React from 'react';
import qs, { StringifyOptions } from 'query-string';
import { withRouter } from 'react-router-dom';
import { RouteComponentProps } from 'react-router';
import isEmpty from 'lodash/isEmpty';
import omit from 'lodash/omit';

import { QueryParamModel, SortDirection } from '../../models';
import { removeProp } from '../../utils/objects';
import { compose } from '../../utils/functions';
import sharedConfig from '../../config';

export interface SearchConditions {
  name: string;
  value: string | string[];
}

export type OnSortFunction = (
  event: React.MouseEvent<unknown>,
  orderBy: string,
  direction: SortDirection
) => QueryParamModel;

export type OnSearchFunction = (searchConditions: SearchConditions) => QueryParamModel;

export interface WithTableQueryParamsProps extends TableParams {
  setCurrentParams: (params: Partial<QueryParamModel>) => QueryParamModel;
  onSearch: OnSearchFunction;
}

export interface TableParams {
  params: QueryParamModel;
  onSort: OnSortFunction;
  onChangePage: (event: unknown, page: number) => QueryParamModel;
  onChangeRowsPerPage: (event: React.ChangeEvent<HTMLInputElement>) => QueryParamModel;
}

export interface TableQueryParamsDefaltProps {
  queryFormat: StringifyOptions;
  defaultRowsPerPageOption: number;
}

export interface TableQueryParamsProps extends RouteComponentProps {
  filterParams: QueryParamModel;
  setFilterParams: (params: QueryParamModel) => void;
  location: {
    search: string;
    pathname: string;
  };
  history: any;
  match: {
    url: string;
  };
}

export interface TableQueryParamsState {
  params: QueryParamModel;
}

export interface QueryModel {
  query?: string;
}

export const initialParams: Partial<QueryParamModel> = { direction: SortDirection.ASC, orderBy: 'name' };

export type OverrideQueryParamsModel = Partial<QueryParamModel>;

export type TableQueryParamsComponentProps = WithTableQueryParamsProps & TableQueryParamsProps;

const FILTER_KEY_VALUE_GIVING = '=';

export const withTableQueryParams =
  (
    overrideParams: OverrideQueryParamsModel = initialParams,
    defaultProps: TableQueryParamsDefaltProps = {
      queryFormat: sharedConfig.QUERY_FORMAT,
      defaultRowsPerPageOption: sharedConfig.ROWS_PER_PAGE_OPTIONS[0],
    }
  ) =>
  <P extends WithTableQueryParamsProps>(WrappedComponent: React.ComponentType<P>) => {
    const defaultParams = { ...initialParams, ...overrideParams };
    type Props = WithTableQueryParamsProps & TableQueryParamsProps;

    class TableQueryParams extends React.Component<Props, TableQueryParamsState> {
      getCurrentParams = () => {
        const { filterParams: savedQueryParams, location } = this.props;

        const queryParams = { ...qs.parse(location.search, defaultProps.queryFormat) } as QueryModel;

        if (!isEmpty(queryParams) && queryParams.query) {
          return this.getParams(JSON.parse(atob(queryParams.query)));
        }

        if (!isEmpty(savedQueryParams) && savedQueryParams.from === location.pathname) {
          const { from, ...pureQueryParams } = savedQueryParams;
          this.addQueryToHistory(pureQueryParams);

          return this.getParams(pureQueryParams);
        }

        return this.getParams(defaultParams);
      };

      getParams = ({ page, orderBy, direction, pageSize, filter, ...rest }: any = {}) => {
        const hasFilter = filter && filter.filter((item: string) => item.includes(FILTER_KEY_VALUE_GIVING)).length > 0;

        return {
          page: typeof page === 'string' ? parseInt(page, 10) : page || 1,
          orderBy: typeof orderBy === 'string' ? orderBy : defaultParams.orderBy,
          direction: typeof direction === 'undefined' ? defaultParams.direction : (direction as SortDirection),
          pageSize:
            typeof pageSize === 'string'
              ? parseInt(pageSize, 10)
              : pageSize || defaultParams.pageSize || defaultProps.defaultRowsPerPageOption,
          filter: hasFilter ? filter : undefined,
          ...rest,
        } as QueryParamModel;
      };

      addQueryToHistory = (params: Partial<QueryParamModel> = this.state.params) => {
        const { history, match, location } = this.props;

        history.replace(
          `${match.url}?${qs.stringify(
            { ...omit(qs.parse(location.search), 'query'), query: btoa(JSON.stringify(params)) },
            defaultProps.queryFormat
          )}`
        );
      };

      setCurrentParams = (queryParams?: Partial<QueryParamModel>) => {
        const params = this.getParams(queryParams);

        this.setState({ params });
        this.props.setFilterParams({ ...params, from: this.props.location.pathname });
        this.addQueryToHistory(params);

        return params;
      };

      onSort: OnSortFunction = (event, orderBy, direction) => {
        return this.setCurrentParams({ ...this.state.params, direction, orderBy });
      };

      onSearch: OnSearchFunction = ({ name, value }) => {
        const filterValue = defaultParams.filter
          ? defaultParams.filter.map((key: string) => `${key}${FILTER_KEY_VALUE_GIVING}${value}`)
          : [];

        const params = value
          ? { ...this.state.params, page: 1, [name]: filterValue || null }
          : removeProp(this.state.params, name);

        return this.setCurrentParams({ ...params });
      };

      onChangePage = (event: unknown, page: number) => {
        return this.setCurrentParams({ ...this.state.params, page: page + 1 });
      };

      onChangeRowsPerPage = (event: React.ChangeEvent<HTMLInputElement>) => {
        return this.setCurrentParams({ ...this.state.params, page: 1, pageSize: parseInt(event.target.value, 10) });
      };

      readonly state: TableQueryParamsState = {
        params: this.getCurrentParams(),
      };

      render() {
        return (
          <WrappedComponent
            {...(this.props as P & TableQueryParamsProps)}
            params={this.state.params}
            setCurrentParams={this.setCurrentParams}
            onSort={this.onSort}
            onSearch={this.onSearch}
            onChangePage={this.onChangePage}
            onChangeRowsPerPage={this.onChangeRowsPerPage}
          />
        );
      }
    }

    return compose(withRouter)(TableQueryParams);
  };
