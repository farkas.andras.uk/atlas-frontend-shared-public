import React, { useState } from 'react';
import { Router } from 'react-router-dom';
import { createBrowserHistory } from 'history';

import { ThemeProvider, StyledEngineProvider } from '@mui/material/styles';
import FilterIcon from '@mui/icons-material/Filter';
import CancelIcon from '@mui/icons-material/Close';

import { theme } from '../../theme';
import { Table } from './Table';

const history = createBrowserHistory();

export default {
  title: 'Table',
  component: Table,
};

const AccessController = ({ children }) => {
  return children;
};

export const Default = (): React.ReactNode => {
  const [selectedItems, setSelectedItems] = useState([]);

  const onSort = () => {
    return {};
  };

  const onChangePage = () => {};
  const onChangeRowsPerPage = () => {};

  return (
    <Router history={history}>
      <StyledEngineProvider injectFirst>
        <ThemeProvider theme={theme}>
          <Table
            list={[
              {
                id: 1,
                title: 'Test Title 1',
                description: 'Test Description 1',
                isActive: true,
                isDeleted: false,
              },
              {
                id: 2,
                title: 'Test Title 2',
                description: 'Test Description 2',
                isActive: true,
                isDeleted: false,
              },
              {
                id: 3,
                title: 'Test Title 3',
                description: 'Test Description 3',
                isActive: false,
                isDeleted: false,
              },

              {
                id: 4,
                title: 'Test Title 4',
                description: 'Test Description 4x',
                isActive: false,
                isDeleted: true,
              },
            ]}
            total={1}
            loading={false}
            params={{}}
            onSort={onSort}
            onChangePage={onChangePage}
            onChangeRowsPerPage={onChangeRowsPerPage}
            actions={[
              {
                key: 'asd',
                header: () => 'Title',
                icon: (row) => (row.id === 1 ? <FilterIcon /> : <CancelIcon />),
              },
              {
                key: 'qwe',
                header: () => 'Description',
                icon: () => <FilterIcon />,
              },
            ]}
            columns={[
              {
                key: 'title',
                header: () => 'Title',
                accessor: 'title',
                sortable: true,
              },
              {
                key: 'description',
                header: () => 'Description',
                accessor: 'description',
                sortable: true,
              },
            ]}
            expandMoreIcon={<>E</>}
            accessController={AccessController}
            defaultRowsPerPage={10}
            labelRowsPerPage="Oldalméret"
            deleteAction={{ selectedItems, setSelectedItems }}
            isTableRowInactive={(item) => !item.isActive}
            isCheckboxDisabled={(item) => item.isDeleted}
            isTableRowActive={{
              key: 'id',
              value: 2,
            }}
          />
        </ThemeProvider>
      </StyledEngineProvider>
    </Router>
  );
};
