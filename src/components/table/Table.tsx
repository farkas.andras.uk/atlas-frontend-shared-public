import React, { ReactNode, ComponentType, useCallback, useState, useEffect, useMemo, MouseEvent } from 'react';
import get from 'lodash/get';
import isFunction from 'lodash/isFunction';
import TableHead from '@mui/material/TableHead';
import TableBody from '@mui/material/TableBody';
import IconButton from '@mui/material/IconButton';
import Tooltip, { TooltipProps } from '@mui/material/Tooltip';
import Collapse from '@mui/material/Collapse';
import { ConditionalWrapper, usePreviousProps } from '../util';
import { BaseModel, AccessRestriction, QueryParamModel } from '../../models';
import { arrayify, removeEmpty, getFilterValue } from '../../utils';
import { TableRoot, BaseTableRootProps } from './components/TableRoot';
import { TableCell } from './components/TableCell';
import { TableRow } from './components/TableRow';
import { TableHeaderCell } from './components/TableHeaderCell';
import { Checkbox } from '../form/checkbox/Checkbox';
import { sx_config } from './Table.style';

export type AccessorFunction<T> = (item: T, index: number) => ReactNode;

export type Accessor<T> = string | AccessorFunction<T>;

export type KeyExtractor<T> = string | ((row: T, index: number) => string);

export interface Column<T, EmbedType> extends AccessRestriction {
  key: string;
  header?: () => ReactNode;
  tooltip?: Accessor<T | EmbedType> | ReactNode;
  tooltipProps?: Omit<TooltipProps, 'children' | 'title'>;
  hidden?: boolean;
  noTypography?: boolean;
}

export interface ColumnProps<T, EmbedType> extends Column<T, EmbedType> {
  accessor?: Accessor<T | EmbedType>;
  field?: string;
  padding?: 'checkbox' | 'none' | 'normal';
  align?: 'inherit' | 'left' | 'center' | 'right' | 'justify';
  sortable?: boolean;
  noWrap?: boolean;
}

export type ActionFunctionType<T, EmbedType> = (item: T | EmbedType, index: number, parentItem?: T) => any;

export interface ActionColumnProps<T, EmbedType> extends Column<T, EmbedType> {
  icon: ActionFunctionType<T, EmbedType>;
  action?: ActionFunctionType<T, EmbedType>;
  ctrlKeyAction?: ActionFunctionType<T, EmbedType>;
  disabled?: (item: T | EmbedType, index: number, parentItem?: T) => boolean;
  link?: string;
}

export type ActionProps<T, EmbedType> = Omit<ActionColumnProps<T, EmbedType>, 'icon' | 'key'>;

export interface DeleteActionProps<T, EmbedType>
  extends Omit<ActionColumnProps<T, EmbedType>, 'icon' | 'key' | 'action'> {
  selectedItems: number[];
  setSelectedItems: (items: number[]) => any;
}

export interface BaseTableProps<T> {
  hideHeader?: boolean;
}

export interface EmbedTableProps<T, EmbedType> extends BaseTableProps<T> {
  embedColumns?: (ColumnProps<T, EmbedType> | null)[];
  embedListKey?: string;
  editAction?: ActionProps<T, EmbedType>;
  keyField?: KeyExtractor<EmbedType>;
}

export interface TableRowActiveModel {
  key: string;
  value: any;
}

export interface TableRowProps<T, EmbedType> {
  expandMoreIcon?: ReactNode;
  checkboxIcon?: ReactNode;
  rowTooltipTitle?: ReactNode;
  isTableRowActive?: TableRowActiveModel;
  isTableRowInactive?: (item: T) => boolean;
  isCheckboxDisabled?: (item: T) => boolean;
  accessController: ComponentType<any>;
  expandAll?: boolean;
  embed?: boolean;
  embedTableProps?: EmbedTableProps<T, EmbedType>;
  editAction?: ActionProps<T, EmbedType>;
  deleteAction?: DeleteActionProps<T, EmbedType>;
}

export interface TableProps<T, EmbedType> extends BaseTableRootProps, BaseTableProps<T>, TableRowProps<T, EmbedType> {
  list?: T[] | T;
  showEmpty?: boolean;
  columns: (ColumnProps<T, EmbedType> | null)[];
  actions?: (ActionColumnProps<T, EmbedType> | null)[];
  keyField?: KeyExtractor<T>;
}

const hasEmbedColumns = (embedColumns?: any[]) => embedColumns && !!embedColumns.length;

export const Table = <T extends BaseModel, EmbedType extends BaseModel>({
  columns,
  actions,
  list,
  keyField,
  editAction,
  deleteAction,
  embedTableProps,
  hideHeader,
  embed,
  expandAll,
  accessController: AccessController,
  expandMoreIcon,
  checkboxIcon,
  rowTooltipTitle,
  isTableRowActive,
  isTableRowInactive,
  isCheckboxDisabled,
  ...props
}: TableProps<T, EmbedType>) => {
  const arrayList = arrayify(list);
  const validColumns = removeEmpty(columns);
  const validActions = removeEmpty(actions);
  const validEmbedColumns = embedTableProps ? removeEmpty(embedTableProps.embedColumns) : [];
  const availableItems = arrayList.filter((item) => (isCheckboxDisabled ? !isCheckboxDisabled(item) : true));

  const isAllSelected = deleteAction && deleteAction.selectedItems.length >= availableItems.length;

  const getFieldName = (column: ColumnProps<T, EmbedType>) => {
    if (!column.sortable) {
      return null;
    }

    return typeof column.accessor === 'string' ? column.accessor : column.field;
  };

  const selectAllRows = () => {
    if (deleteAction.selectedItems) {
      const next = isAllSelected ? [] : availableItems.map(({ id }) => id || -1);

      deleteAction.setSelectedItems(next);
    }
  };

  useEffect(() => {
    const disabledItems =
      deleteAction?.selectedItems.filter((id) => !availableItems.find((item) => item.id === id)) || [];

    if (disabledItems.length) {
      deleteAction?.setSelectedItems(deleteAction.selectedItems.filter((id) => !disabledItems.includes(id)));
    }
  }, [availableItems, deleteAction?.selectedItems]);

  return (
    <TableRoot {...props} embed={embed} count={arrayList.length}>
      {!hideHeader ? (
        <TableHead>
          <TableRow>
            {deleteAction ? (
              <AccessController allowedFor={deleteAction.allowedFor}>
                <TableHeaderCell padding="checkbox">
                  <Checkbox icon={checkboxIcon} center theme="light" checked={isAllSelected} onChange={selectAllRows} />
                </TableHeaderCell>
              </AccessController>
            ) : null}
            {validColumns.map((column, columnIndex) =>
              !column.hidden ? (
                <AccessController allowedFor={column.allowedFor} key={column.key}>
                  <TableHeaderCell
                    align={column.align}
                    padding={column.padding}
                    field={getFieldName(column)}
                    noPaddingLeft={!!deleteAction && !columnIndex}
                  >
                    {column.header ? column.header() : null}
                  </TableHeaderCell>
                </AccessController>
              ) : null
            )}
            {validActions.map((action) =>
              !action.hidden && action.icon ? (
                <AccessController allowedFor={action.allowedFor} key={action.key}>
                  <TableHeaderCell align="right" padding="checkbox">
                    {action.header ? action.header() : null}
                  </TableHeaderCell>
                </AccessController>
              ) : null
            )}
            {hasEmbedColumns(validEmbedColumns) ? (
              <TableHeaderCell align="right" padding="checkbox" withExpandIcon />
            ) : null}
          </TableRow>
        </TableHead>
      ) : null}
      <TableBody>
        {arrayList.map((item, index) => (
          <Row
            key={String(isFunction(keyField) ? keyField(item, index) : get(item, keyField || 'id'))}
            item={item}
            index={index}
            columns={removeEmpty(columns)}
            actions={removeEmpty(actions)}
            editAction={editAction}
            deleteAction={deleteAction}
            embedTableProps={{ ...embedTableProps, embedColumns: validEmbedColumns }}
            embed={embed}
            expandAll={expandAll}
            accessController={AccessController}
            expandMoreIcon={expandMoreIcon}
            checkboxIcon={checkboxIcon}
            params={props.params}
            rowTooltipTitle={rowTooltipTitle}
            isTableRowActive={isTableRowActive}
            isTableRowInactive={isTableRowInactive}
            isCheckboxDisabled={isCheckboxDisabled}
          />
        ))}
      </TableBody>
    </TableRoot>
  );
};

interface RowProps<T, EmbedType> extends TableRowProps<T, EmbedType> {
  columns: ColumnProps<T, EmbedType>[];
  actions: ActionColumnProps<T, EmbedType>[];
  item: T;
  index: number;
  params?: QueryParamModel;
}

const Row = <T extends BaseModel, EmbedType extends BaseModel>({
  editAction,
  embedTableProps,
  embed,
  expandAll,
  accessController: AccessController,
  params,
  rowTooltipTitle,
  isTableRowActive,
  isTableRowInactive,
  isCheckboxDisabled,
  ...props
}: RowProps<T, EmbedType>) => {
  const { embedColumns, embedListKey, hideHeader } = embedTableProps || {};
  const isItemSelected = props.deleteAction && !!props.deleteAction.selectedItems.find((id) => id === props.item.id);
  const hasEmbedColumnsAction = embedTableProps && hasEmbedColumns(embedTableProps.embedColumns);
  const prevProps = usePreviousProps({ item: props.item });

  const [expanded, setExpanded] = useState(false);
  const handleExpandClick = useCallback(() => {
    setExpanded(!expanded);
  }, [expanded, setExpanded]);

  useEffect(() => {
    if (params?.filter && !expanded && prevProps.item !== props.item) {
      const filter = getFilterValue(params.filter);
      const embedList = embedListKey && (props.item as any)[embedListKey];
      const embedAccessors = embedTableProps?.embedColumns?.map((col) => col?.accessor);

      const isContainFilter = !!arrayify(embedList).find(
        (item, i) =>
          embedAccessors?.find((key) => {
            if (!key) {
              return false;
            }

            const value = isFunction(key) ? key(item, i) : get(item, key);

            return value.toString().toLowerCase().includes(filter.toLowerCase());
          })
      );

      if (isContainFilter) {
        setExpanded(true);
      }
    }
  }, [params, props.item, props.columns, expanded, embedListKey, prevProps.item]);

  useEffect(() => {
    if (typeof expandAll !== 'undefined' && expandAll !== expanded) {
      handleExpandClick();
    }
  }, [expandAll, expanded, handleExpandClick]);

  const isRowDisabled = useMemo(() => {
    return editAction && editAction.disabled ? editAction.disabled(props.item, props.index) : false;
  }, [editAction]);

  const isItemActive = useMemo(() => {
    if (isTableRowActive) {
      return get(props.item, isTableRowActive.key) === isTableRowActive.value;
    }
  }, [isTableRowActive, props.item, props.index]);

  const isRowInactive = useMemo(() => {
    return isTableRowInactive ? isTableRowInactive(props.item) : false;
  }, [isTableRowInactive, props.item]);

  const onRowClick = (event: MouseEvent) => {
    if (isRowDisabled) {
      return;
    }

    if (event.ctrlKey && editAction && editAction.ctrlKeyAction) {
      editAction.ctrlKeyAction(props.item, props.index);
    } else if (editAction && editAction.action) {
      editAction.action(props.item, props.index);
    } else if (hasEmbedColumnsAction) {
      handleExpandClick();
    }
  };

  return (
    <AccessController
      allowedFor={editAction && editAction.allowedFor}
      noAccessChildren={
        <TableRow>
          <RowContent embedTableProps={embedTableProps} embed={embed} accessController={AccessController} {...props} />
        </TableRow>
      }
    >
      <TableRow
        sx={[
          sx_config.tableRow,
          !((editAction && editAction.action) || hasEmbedColumnsAction) && { ...sx_config.noAction },
          isItemSelected && { ...sx_config.selectedTableRow },
          isItemActive && { ...sx_config.activeTableRow },
          embed && { ...sx_config.embedRow },
          isRowDisabled && { ...sx_config.disabled },
          isRowInactive && { ...sx_config.isInactive },
        ]}
        tooltipTitle={isRowDisabled && rowTooltipTitle}
      >
        <RowContent
          onClick={onRowClick}
          isItemSelected={isItemSelected}
          handleExpandClick={handleExpandClick}
          expanded={expanded}
          embedTableProps={embedTableProps}
          embed={embed}
          accessController={AccessController}
          isCheckboxDisabled={isCheckboxDisabled ? isCheckboxDisabled(props.item) : false}
          {...props}
        />
      </TableRow>
      {hasEmbedColumns(embedColumns) && embedListKey && embedColumns ? (
        <TableRow sx={sx_config.overflowHidden}>
          <TableCell padding="checkbox" colSpan={props.columns.length + 1} noBorderBottom>
            <Collapse in={expanded} timeout="auto" unmountOnExit>
              <Table
                list={(props.item as any)[embedListKey]}
                columns={embedColumns}
                hideHeader={hideHeader}
                editAction={
                  embedTableProps && {
                    ...embedTableProps.editAction,
                    action: (item: T | any, index: number) =>
                      embedTableProps.editAction &&
                      embedTableProps.editAction.action &&
                      embedTableProps.editAction.action(item, index, props.item),
                  }
                }
                keyField={embedTableProps.keyField}
                accessController={AccessController}
                expandMoreIcon={props.expandMoreIcon}
                embed
              />
            </Collapse>
          </TableCell>
        </TableRow>
      ) : null}
    </AccessController>
  );
};

interface RowContentProps<T, EmbedType> extends Omit<RowProps<T, EmbedType>, 'editAction' | 'isCheckboxDisabled'> {
  onClick?: (event: MouseEvent) => void;
  isItemSelected?: boolean;
  isCheckboxDisabled?: boolean;
  handleExpandClick?: () => void;
  expanded?: boolean;
}

const RowContent = <T extends BaseModel, EmbedType extends BaseModel>({
  columns,
  actions,
  item,
  index,
  deleteAction,
  embedTableProps,
  onClick,
  isItemSelected,
  isCheckboxDisabled,
  handleExpandClick,
  expanded,
  embed,
  accessController: AccessController,
  expandMoreIcon,
  checkboxIcon,
}: RowContentProps<T, EmbedType>) => {
  const getValue = useCallback(
    (accessor?: Accessor<T> | ReactNode) => {
      if (!accessor) {
        return null;
      }

      if (typeof accessor === 'string') {
        return get(item, accessor);
      }

      if (isFunction(accessor)) {
        return accessor(item, index);
      }

      return accessor;
    },
    [item, index]
  );

  const onCheckboxChange = () => {
    if (deleteAction && deleteAction.setSelectedItems) {
      deleteAction.setSelectedItems(
        isItemSelected
          ? deleteAction.selectedItems.filter((id) => id !== item.id)
          : [...deleteAction.selectedItems, item.id || -1]
      );
    }
  };

  return (
    <>
      {deleteAction ? (
        <AccessController allowedFor={deleteAction.allowedFor}>
          <TableCell padding="checkbox" embed={embed}>
            <Checkbox
              icon={checkboxIcon}
              color="primary"
              center
              theme="dark"
              checked={isItemSelected}
              disabled={isCheckboxDisabled}
              onChange={onCheckboxChange}
            />
          </TableCell>
        </AccessController>
      ) : null}
      {columns.map((column, columnIndex) =>
        !column.hidden ? (
          <AccessController allowedFor={column.allowedFor} key={column.key}>
            <TableCell
              embed={embed}
              align={column.align}
              padding={column.padding}
              noWrap={column.noWrap}
              noPaddingLeft={!!deleteAction && !columnIndex}
              onClick={onClick}
              noTypography={column.noTypography}
            >
              <ConditionalWrapper
                condition={Boolean(getValue(column.tooltip))}
                wrapper={(children) => (
                  <Tooltip title={getValue(column.tooltip)} {...column.tooltipProps}>
                    <span>{children}</span>
                  </Tooltip>
                )}
              >
                {getValue(column.accessor)}
              </ConditionalWrapper>
            </TableCell>
          </AccessController>
        ) : null
      )}

      {actions.map((action) =>
        !action.hidden && action.icon ? (
          <AccessController allowedFor={action.allowedFor} key={action.key}>
            <TableCell align="right" padding="checkbox">
              <ConditionalWrapper
                condition={Boolean(action.tooltip)}
                wrapper={(children) => (
                  <Tooltip title={getValue(action.tooltip)}>
                    <span>{children}</span>
                  </Tooltip>
                )}
              >
                {getValue(action.icon)}
              </ConditionalWrapper>
            </TableCell>
          </AccessController>
        ) : null
      )}
      {embedTableProps && hasEmbedColumns(embedTableProps.embedColumns) && expandMoreIcon ? (
        <TableCell align="right" padding="checkbox" withExpandIcon>
          <IconButton
            sx={[sx_config.expand, expanded && { ...sx_config.expandOpen }]}
            onClick={handleExpandClick}
            aria-expanded={expanded}
          >
            {expandMoreIcon}
          </IconButton>
        </TableCell>
      ) : null}
    </>
  );
};
