import React, { createContext, ReactNode } from 'react';
import { Paper, Box } from '@mui/material';
import Typography from '@mui/material/Typography';
import MuiTable, { TableProps as MuiTableProps } from '@mui/material/Table';
import { QueryParamModel, SortDirection } from '../../../models';
import { OnSortFunction } from '../TableQueryParams';
import { TablePagination, OnChangePage, OnChangeRowsPerPage, TablePaginationProps } from './TablePagination';
import { sx_config } from './TableRoot.style';

export interface TableContextValues {
  onSort?: OnSortFunction | null;
  orderBy?: string | null;
  direction?: SortDirection | string | null;
}

export const TableContext = createContext<TableContextValues>({
  orderBy: null,
  direction: null,
  onSort: null,
});

export interface BaseTableRootProps extends MuiTableProps, TableContextValues {
  params?: QueryParamModel;
  emptyState?: ReactNode;
  loading?: boolean;
  total?: number;
  onChangePage?: OnChangePage;
  onChangeRowsPerPage?: OnChangeRowsPerPage;
  pagination?: TablePaginationProps;
  embed?: boolean;
  defaultRowsPerPage?: number;
  labelRowsPerPage?: string;
  showEmpty?: boolean;
  className?: string;
}

export interface TableRootProps extends BaseTableRootProps {
  count?: number;
  disableStickyHeader?: boolean;
}

export const TableRoot = ({
  children,
  onSort,
  orderBy,
  direction,
  emptyState,
  loading,
  count,
  onChangePage,
  onChangeRowsPerPage,
  total,
  params,
  pagination,
  disableStickyHeader,
  embed,
  defaultRowsPerPage,
  labelRowsPerPage,
  className,
  showEmpty,
}: TableRootProps) => {
  return (
    <TableContext.Provider
      value={{ onSort, orderBy: orderBy || params?.orderBy, direction: direction || params?.direction }}
    >
      {!loading && (showEmpty || count) ? (
        <Paper
          square
          sx={[
            sx_config.paper,
            embed && { ...sx_config.embedPaper },
            className && { ...(className as React.CSSProperties) },
          ]}
        >
          <Box sx={[sx_config.root, !onChangePage && !embed && { ...sx_config.noPaginationRoot }]}>
            <MuiTable stickyHeader={!disableStickyHeader}>{children}</MuiTable>
          </Box>
          {onChangePage ? (
            <TablePagination
              {...(pagination || {})}
              labelRowsPerPage={labelRowsPerPage}
              count={total}
              page={params?.page}
              rowsPerPage={params?.pageSize || defaultRowsPerPage}
              onPageChange={onChangePage}
              onChangeRowsPerPage={onChangeRowsPerPage}
              component="div"
            />
          ) : null}
        </Paper>
      ) : null}

      {!loading && !count ? <Typography variant="h4">{emptyState}</Typography> : null}
    </TableContext.Provider>
  );
};
