import React, { forwardRef, ReactNode, ElementType } from 'react';
import { TablePagination as TablePaginationOriginal } from '@mui/material';
import { SelectProps } from '@mui/material/Select';
import { TablePaginationActionsProps } from '@mui/material/TablePagination/TablePaginationActions';
import { IconButtonProps } from '@mui/material/IconButton';
import { formatNumber } from '../../../utils';
import { sx_config } from './TablePagination.style';

export type OnChangePage = (event: React.MouseEvent<HTMLButtonElement> | null, page: number) => void;
export type OnChangeRowsPerPage = React.ChangeEventHandler<HTMLTextAreaElement | HTMLInputElement>;
export type onRowsPerPageChange = React.ChangeEvent<HTMLTextAreaElement | HTMLInputElement>;

export interface TablePaginationProps {
  innerRef?: React.Ref<any>;
  ActionsComponent?: React.ElementType<TablePaginationActionsProps>;
  backIconButtonProps?: Partial<IconButtonProps>;
  count?: number;
  labelDisplayedRows?: (paginationInfo) => ReactNode;
  labelRowsPerPage?: string;
  nextIconButtonProps?: Partial<IconButtonProps>;
  onPageChange: OnChangePage;
  onChangeRowsPerPage?: OnChangeRowsPerPage;
  page?: number;
  rowsPerPage?: number;
  rowsPerPageOptions?: Array<number | { value: number; label: string }>;
  SelectProps?: Partial<SelectProps>;
  component?: ElementType<any>;
  className?: string;
}

function getDisplayedRowsLabel({ from, to, count }) {
  return `${formatNumber(from) || 0} - ${formatNumber(to) || 0} / ${formatNumber(count) || 0}`;
}

export const TablePagination = forwardRef(
  (
    {
      labelRowsPerPage,
      labelDisplayedRows,
      rowsPerPageOptions,
      page,
      rowsPerPage,
      count,
      onChangeRowsPerPage,
      ...props
    }: TablePaginationProps,
    ref
  ) => {
    const adjustedPage = (page || 1) - 1;

    return (
      <TablePaginationOriginal
        ref={ref}
        sx={sx_config.root}
        rowsPerPageOptions={rowsPerPageOptions}
        count={count || 0}
        page={!count && adjustedPage > 0 ? 0 : adjustedPage}
        rowsPerPage={rowsPerPage}
        labelRowsPerPage={labelRowsPerPage}
        labelDisplayedRows={labelDisplayedRows || getDisplayedRowsLabel}
        onRowsPerPageChange={onChangeRowsPerPage}
        {...props}
      />
    );
  }
);
