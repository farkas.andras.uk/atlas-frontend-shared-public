export const sx_config = {
  root: {
    fontSize: 12,
    display: 'flex',
    flexDirection: 'column',
    '& .MuiTypography-root': {
      fontSize: 12,
    },
  },
};
