import React, { useContext } from 'react';
import TableSortLabel from '@mui/material/TableSortLabel';
import { SortDirection as MuiSortDirection } from '@mui/material/TableCell';
import { SortDirection } from '../../../models';
import { TableCell, TableCellProps } from './TableCell';
import { TableContext } from './TableRoot';
import { sx_config } from './TableHeaderCell.style';

interface TableHeaderCellProps extends TableCellProps {
  field?: string | null;
}

export const TableHeaderCell = ({ field, children, noWrap, ...props }: TableHeaderCellProps) => {
  const { onSort, orderBy, direction } = useContext(TableContext);

  const active = orderBy === field;

  const getOrder = () => {
    if (!active) {
      return SortDirection.ASC;
    }

    if (direction === SortDirection.ASC) {
      return SortDirection.DESC;
    }

    return SortDirection.ASC;
  };

  const createSortHandler = (property: string) => (event: React.MouseEvent<unknown>) => {
    if (onSort) {
      const nextOrder = getOrder();

      onSort(event, property, nextOrder);
    }
  };

  const getOrderBy = (): MuiSortDirection => {
    switch (direction) {
      case SortDirection.ASC: {
        return 'asc';
      }
      case SortDirection.DESC: {
        return 'desc';
      }
      default: {
        return 'asc';
      }
    }
  };

  const muiOrderBy = getOrderBy();

  return (
    <TableCell
      sx={sx_config.tableCell}
      sortDirection={direction && orderBy === field ? muiOrderBy : false}
      noWrap={noWrap !== false}
      header
      {...props}
    >
      {field ? (
        <TableSortLabel
          active={active}
          sx={[sx_config.sortLabelRoot, sx_config.active]}
          direction={active ? muiOrderBy || undefined : 'asc'}
          onClick={createSortHandler(field)}
        >
          {children}
        </TableSortLabel>
      ) : (
        children
      )}
    </TableCell>
  );
};

export default TableHeaderCell;
