import { Theme } from '@mui/material/styles';

const PAGINATION_HEIGHT = 52;

export const sx_config = {
  root: {
    overflowX: 'auto',
    maxHeight: `calc(100% - ${PAGINATION_HEIGHT}px)`,
  },
  noPaginationRoot: {
    maxHeight: '100%',
  },
  paper: (theme: Theme) => ({
    display: 'flex',
    flexDirection: 'column',
    overflowY: 'auto',
    marginBottom: theme.spacing(2),
    backgroundColor: theme.palette.common.white,
    boxShadow: '0px 3px 1px -2px rgba(0, 0, 0, 0.2), 0px 2px 2px rgba(0, 0, 0, 0.14), 0px 1px 5px rgba(0, 0, 0, 0.12)',
    borderRadius: '4px',
  }),
  embedPaper: {
    borderRadius: '0px',
    boxShadow: 'none',
    marginBottom: '0px',
  },
};
