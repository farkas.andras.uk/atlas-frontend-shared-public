import React, { FC, ReactElement, ReactNode, memo } from 'react';

import MuiTableRow, { TableRowProps as MuiTableRowProps } from '@mui/material/TableRow';
import Tooltip from '@mui/material/Tooltip';

import { TableCell, TableLinkProps } from './TableCell';

export interface TableRowProps extends MuiTableRowProps, TableLinkProps {
  tooltipTitle?: ReactNode;
}

export const TableRow: FC<TableRowProps> = memo(({ children, link, to, replace, tooltipTitle, ...props }) => {
  const row = (
    <MuiTableRow {...props}>
      {children
        ? React.Children.map(children, (child) => {
            if (React.isValidElement(child) && child.type === TableCell) {
              return React.cloneElement(child as ReactElement, {
                link: child.props.link === false ? child.props.link : link,
                to,
                replace,
              });
            }

            return child;
          })
        : null}
    </MuiTableRow>
  );

  return tooltipTitle ? <Tooltip title={tooltipTitle}>{row}</Tooltip> : row;
});

export default TableRow;
