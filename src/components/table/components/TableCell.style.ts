import { Theme, alpha } from '@mui/material/styles';

export const sx_config = {
  noWrap: {
    whiteSpace: 'nowrap',
  },
  link: {
    textDecoration: 'none',
    display: 'flex',
    color: 'inherit',
    padding: (theme: Theme) => theme.spacing(2),
    textAlign: 'inherit',
    flexDirection: 'inherit',
  },
  linkCell: {
    padding: '0px',
    '&.MuiTableCell-sizeSmall': {
      '& $link': {
        padding: '6px 24px 6px 16px',
      },
    },
    '&.MuiTableCell-paddingCheckbox': {
      '& $link': {
        padding: '0 0 0 4px',
      },
    },
  },
  embedCell: {
    borderBottom: (theme: Theme) => `1px solid ${alpha(theme.palette.common.greyE0, 0.3)}`,
    '&:first-child': {
      paddingLeft: '50px',
    },
  },
  tableCell: {
    '&.MuiTableCell-paddingCheckbox': {
      width: '51px',
    },
  },
  withExpandIcon: {
    overflow: 'hidden',
  },
  noBorderBottom: {
    borderBottom: '0px',
  },
  noPaddingLeft: {
    paddingLeft: '0px',
  },
  mergedCell: {
    padding: '0px !important',
  },
};
