import React, { memo } from 'react';
import { Link } from 'react-router-dom';
import MuiTableCell, { TableCellProps as MuiTableCellProps } from '@mui/material/TableCell';
import { Typography } from '../../typography/Typography';
import { sx_config } from './TableCell.style';
import './TableCell.css';

export interface TableLinkProps {
  link?: boolean;
  to?: string;
  replace?: boolean;
  header?: boolean;
}

export interface TableCellProps extends MuiTableCellProps, TableLinkProps {
  noWrap?: boolean;
  noTypography?: boolean;
  noPaddingLeft?: boolean;
  withExpandIcon?: boolean;
  colSpan?: number;
  noBorderBottom?: boolean;
  embed?: boolean;
}

export const TableCell = memo(
  ({
    noWrap,
    className,
    children,
    link,
    to,
    replace,
    noPaddingLeft,
    header,
    withExpandIcon,
    noBorderBottom,
    embed,
    noTypography,
    ...props
  }: TableCellProps) => {
    const variant = header ? 'body1' : 'body2';

    const noCheckBoxContent =
      link && to ? (
        <Link to={to} className={'link'} replace={replace}>
          <Typography variant={variant}>{children}</Typography>
        </Link>
      ) : (
        <Typography variant={variant}>{children}</Typography>
      );

    return (
      <MuiTableCell
        sx={[
          sx_config.tableCell,
          noWrap && { ...sx_config.noWrap },
          link && to && { ...sx_config.linkCell },
          noPaddingLeft && { ...sx_config.noPaddingLeft },
          withExpandIcon && { ...sx_config.withExpandIcon },
          noBorderBottom && { ...sx_config.noBorderBottom },
          embed && { ...sx_config.embedCell },
          props.colSpan && { ...sx_config.mergedCell },
          className && { ...(className as React.CSSProperties) },
        ]}
        {...props}
      >
        {props.padding === 'checkbox' || noTypography ? children : noCheckBoxContent}
      </MuiTableCell>
    );
  }
);

export default TableCell;
