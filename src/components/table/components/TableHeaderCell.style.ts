import { Theme } from '@mui/material/styles';

export const sx_config = {
  active: {
    color: (theme: Theme) => `${theme.palette.common.white} !important`,
  },
  sortLabelRoot: {
    '& .MuiSvgIcon-root': {
      color: `rgba(255, 255, 255, 0.54) !important`,
    },
  },
  tableCell: (theme: Theme) => ({
    backgroundColor: theme.palette.primary.main,
    color: theme.palette.common.white,
  }),
};
