import { Theme, alpha } from '@mui/material/styles';

export const sx_config = {
  tableRow: {
    cursor: 'pointer',
    '&:hover': {
      background: 'rgba(0, 0, 0, 0.04)',
    },
  },
  noAction: {
    cursor: 'default',
  },
  embedRow: {
    background: (theme: Theme) => theme.palette.common.greyF7,
  },
  selectedTableRow: {
    background: (theme: Theme) => theme.palette.primary.light,
  },
  activeTableRow: {
    background: (theme: Theme) => theme.palette.common.blueE5,
  },
  expand: (theme: Theme) => ({
    transform: 'rotate(0deg)',
    marginLeft: 'auto',
    transition: theme.transitions.create('transform', {
      duration: theme.transitions.duration.shortest,
    }),
  }),
  expandOpen: {
    transform: 'rotate(180deg)',
  },
  overflowHidden: {
    overflow: 'hidden',
  },
  disabled: (theme: Theme) => ({
    cursor: 'not-allowed',
    background: theme.palette.common.redFF,
    '&:hover': {
      background: theme.palette.common.redFF,
    },
  }),
  isInactive: {
    '& td': {
      color: (theme: Theme) => alpha(theme.palette.text.primary, 0.5),
    },
  },
};
