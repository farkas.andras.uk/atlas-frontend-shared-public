import React from 'react';
import { ProgressAnimation } from './ProgressAnimation';

export default {
  title: 'ProgressAnimation',
  component: ProgressAnimation,
};

export const Default = (): React.ReactNode => (
  <div style={{ backgroundColor: 'black' }}>
    <ProgressAnimation />
  </div>
);
