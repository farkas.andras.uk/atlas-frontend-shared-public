import React, { useRef, useEffect } from 'react';
import lottie from 'lottie-web';

import animationData from '../../assets/animations/progress-animation.json';

export const ProgressAnimation = () => {
  const lottieAnimation = useRef(null);

  useEffect(() => {
    lottie.loadAnimation({
      container: lottieAnimation.current,
      renderer: 'svg',
      loop: true,
      autoplay: true,
      animationData,
    });
  }, []);

  return <div ref={lottieAnimation}></div>;
};
