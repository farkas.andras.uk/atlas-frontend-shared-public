import React from 'react';

import { Image } from './Image';

export default {
  title: 'Image',
  component: Image,
};

export const Default = (): React.ReactNode => <Image />;
