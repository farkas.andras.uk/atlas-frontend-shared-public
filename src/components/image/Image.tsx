import React, { FC, ImgHTMLAttributes, useState } from 'react';
import noImage from '../../assets/images/no-image.png';
import { sx_config } from './Image.style';

type ImageProps = ImgHTMLAttributes<HTMLImageElement>;

export const Image: FC<ImageProps> = ({ src, alt, ...props }) => {
  const [isError, setError] = useState(false);

  return (
    <img
      src={isError || !src ? noImage : src}
      onError={() => setError(true)}
      style={{
        ...sx_config.root,
      }}
      alt={alt || src || 'No image'}
      data-testid="image"
      {...props}
    />
  );
};
