import React from 'react';
import { cleanup, render, fireEvent } from '@testing-library/react';

import { Image } from '../Image';

describe('Image component', () => {
  const src = 'https://path-to-image.com/image.png';

  beforeEach(() => {
    cleanup();
  });

  test('render an image with an src', () => {
    const element = render(<Image src={src} />);

    expect(element.getByTestId('image').getAttribute('src')).toEqual(src);
    expect(element.getByTestId('image').getAttribute('alt')).toEqual(src);
  });

  test('render an image with an src and an alt text', () => {
    const element = render(<Image src={src} alt="Test alt" />);

    expect(element.getByTestId('image').getAttribute('src')).toEqual(src);
    expect(element.getByTestId('image').getAttribute('alt')).toEqual('Test alt');
  });

  test('render an image without an src', () => {
    const element = render(<Image />);

    expect(element.getByTestId('image').getAttribute('src')).toEqual('no-image.png');
    expect(element.getByTestId('image').getAttribute('alt')).toEqual('No image');
  });

  test('render an image after an error', () => {
    const element = render(<Image src={src} alt="Test alt" />);

    fireEvent.error(element.getByRole('img'));

    expect(element.getByTestId('image').getAttribute('src')).toEqual('no-image.png');
    expect(element.getByTestId('image').getAttribute('alt')).toEqual('Test alt');
  });
});
