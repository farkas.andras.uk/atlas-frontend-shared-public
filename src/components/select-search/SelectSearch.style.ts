import { Theme } from '@mui/material/styles';

export const sx_config = {
  root: {
    background: (theme: Theme) => theme.palette.background.paper,
    maxWidth: 600,
    '& .MuiOutlinedInput-root': {
      '& fieldset': {
        borderRadius: '0',
        borderWidth: '0px',
        borderBottom: '1px solid rgba(0, 0, 0, 0.42)',
      },
      '&.Mui-focused fieldset': {
        borderWidth: '0px',
        borderBottom: (theme: Theme) => `2px solid ${theme.palette.primary.main}`,
      },
    },
  },
};
