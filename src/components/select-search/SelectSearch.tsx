import React, { ReactNode } from 'react';
import TextField, { TextFieldProps } from '@mui/material/TextField';
import MenuItem from '@mui/material/MenuItem';
import { SearchConditions } from '../table/TableQueryParams';
import { sx_config } from './SelectSearch.style';

export interface SelectSearchProps {
  onSearch: (searchConditions: SearchConditions) => void;
  showAllOption?: boolean;
  name: string;
  allOptionLabel: ReactNode | string;
}

export const ALL_OPTION = '__all__';

export const SelectSearch = ({
  children,
  value,
  onSearch,
  className,
  variant,
  showAllOption = true,
  allOptionLabel,
  ...props
}: SelectSearchProps & TextFieldProps) => {
  const handleChange: React.ChangeEventHandler<HTMLTextAreaElement | HTMLInputElement> = (e) => {
    onSearch({ name: e.target.name, value: e.target.value === ALL_OPTION ? '' : e.target.value });
  };

  const getValue = () => {
    if (value === null || value === undefined || value === '') {
      return showAllOption ? ALL_OPTION : null;
    }

    return value;
  };

  return (
    <TextField
      select
      value={getValue()}
      onChange={handleChange}
      sx={{
        ...sx_config.root,
        ...(className && { ...(className as React.CSSProperties) }),
      }}
      variant={variant}
      {...props}
    >
      {showAllOption ? <MenuItem value={ALL_OPTION}>{allOptionLabel}</MenuItem> : null}
      {children}
    </TextField>
  );
};
