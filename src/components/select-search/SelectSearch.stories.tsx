import React, { useState } from 'react';
import { Router } from 'react-router-dom';
import { createBrowserHistory } from 'history';

import { ThemeProvider, StyledEngineProvider } from '@mui/material/styles';
import MenuItem from '@mui/material/MenuItem';

import { theme } from '../../theme';
import { SearchConditions } from '../table/TableQueryParams';

import { SelectSearch } from './SelectSearch';

const history = createBrowserHistory();

export default {
  title: 'SelectSearch',
  component: SelectSearch,
};

export const Default = (): React.ReactNode => {
  const [value, setValue] = useState<SearchConditions>({ name: 'car', value: 'volkswagen' });

  return (
    <Router history={history}>
      <StyledEngineProvider injectFirst>
        <ThemeProvider theme={theme}>
          <SelectSearch name="car" allOptionLabel="All car" onSearch={setValue} value={value.value}>
            <MenuItem value="volkswagen">Volkswagen</MenuItem>
            <MenuItem value="audi">Audi</MenuItem>
            <MenuItem value="renault">Renault</MenuItem>
          </SelectSearch>
        </ThemeProvider>
      </StyledEngineProvider>
    </Router>
  );
};
