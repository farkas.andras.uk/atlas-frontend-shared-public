import { Theme } from '@mui/material/styles';

export const sx_config = {
  root: {
    '& .MuiTab-root': {
      fontSize: 23,
      lineHeight: '40px',
      whiteSpace: 'nowrap',
      maxWidth: '100%',
      padding: (theme: Theme) => theme.spacing(0.75, 0),

      '&:not(:last-child)': {
        marginRight: (theme: Theme) => theme.spacing(6),
      },
    },
    '& .MuiTabs-flexContainer': {
      borderBottom: (theme: Theme) => `1px solid ${theme.palette.common.grey7d}`,
      maxWidth: 'fit-content',
    },
    '& .MuiTabs-indicator': {
      height: 5,
      borderBottom: (theme: Theme) => `1px solid ${theme.palette.common.grey7d}`,
    },
    '& .MuiTab-textColorPrimary': {
      color: (theme: Theme) => theme.palette.primary.main,
    },
    '& .Mui-selected': {
      fontWeight: 'bold',
    },
  },
};
