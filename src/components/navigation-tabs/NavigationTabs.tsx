import React, { ReactNode, useRef, useLayoutEffect } from 'react';
import qs from 'query-string';
import { useDispatch } from 'react-redux';
import { Link, useLocation } from 'react-router-dom';
import Tabs, { TabsActions, TabsProps as MuiTabsProps } from '@mui/material/Tabs';
import Tab from '@mui/material/Tab';
import { setFilterParams } from '../../store';
import { sx_config } from './NavigationTabs.style';

export interface TabNavigationItem {
  value: string;
  label: ReactNode;
}

export interface NavigationTabsProps extends MuiTabsProps {
  items: TabNavigationItem[];
}

export function NavigationTabs({
  items,
  className,
  indicatorColor = 'primary',
  textColor = 'primary',
  ...props
}: NavigationTabsProps) {
  const location = useLocation();
  const dispatch = useDispatch();
  const action = useRef<TabsActions>(null);

  const params = qs.parse(location.search);

  const value = items?.find((item) => item.value === params.tab)?.value || items?.[0].value;

  const onTabClick = () => {
    dispatch(setFilterParams({}));
  };

  useLayoutEffect(() => {
    setTimeout(() => {
      if (action.current) {
        action.current.updateIndicator();
      }
    });
  }, []);

  return (
    <Tabs
      {...props}
      value={value}
      sx={{
        ...sx_config.root,
        ...(className && { ...(className as React.CSSProperties) }),
      }}
      indicatorColor={indicatorColor}
      textColor={textColor}
      action={action}
    >
      {items?.map((item) => (
        <Tab
          key={item.value}
          label={item.label}
          value={item.value}
          component={Link}
          onClick={onTabClick}
          to={`${location.pathname}?${qs.stringify({ tab: item.value })}`}
        />
      ))}
    </Tabs>
  );
}
