import React from 'react';
import { Meta, Story } from '@storybook/react/types-6-0';

import { NavigationTabs, NavigationTabsProps } from './NavigationTabs';

export default {
  title: 'NavigationTabs',
  component: NavigationTabs,
} as Meta;

export const Interactive: Story<NavigationTabsProps> = (args) => <NavigationTabs {...args} />;

Interactive.args = {
  items: [
    {
      value: 'test1',
      label: 'Test1',
    },
    {
      value: 'test2',
      label: 'Test2',
    },
  ],
};
