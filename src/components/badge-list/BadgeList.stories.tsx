import React from 'react';
import { Meta, Story } from '@storybook/react/types-6-0';

import { BadgeList, BadgeListProps } from './BadgeList';

export default {
  title: 'BadgeList',
  component: BadgeList,
} as Meta;

export const Interactive: Story<BadgeListProps> = (args) => <BadgeList {...args} />;

Interactive.args = {
  items: Array.from({ length: 15 }).map((_, index) => ({ key: index + 1, label: `Test ${index + 1}` })),
  visibleItemCount: 10,
};
