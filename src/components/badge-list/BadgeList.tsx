import React, { ReactNode, useRef, useState, MouseEvent } from 'react';
import Box from '@mui/material/Box';
import Popover from '@mui/material/Popover';
import IconButton from '@mui/material/IconButton';
import Typography from '@mui/material/Typography';
import Chip, { ChipProps } from '@mui/material/Chip';
import { sx_config } from './BadgeList.style';

export interface BadgeListItem {
  key: string | number;
  color?: ChipProps['color'];
  size?: ChipProps['size'];
  label: ReactNode;
}

export interface BadgeListProps {
  items: BadgeListItem[];
  visibleItemCount?: number;
}

export function BadgeList({ items, visibleItemCount }: BadgeListProps) {
  const [isPopoverOpen, setIsPopoverOpen] = useState(false);
  const visibleItems = visibleItemCount ? items.slice(0, visibleItemCount) : items;
  const popoverAnchorRef = useRef();

  const handlePopoverOpen = (e: MouseEvent) => {
    e.stopPropagation();

    setIsPopoverOpen(true);
  };

  const handlePopoverClose = () => {
    setIsPopoverOpen(false);
  };

  return (
    <>
      <div ref={popoverAnchorRef}>
        {visibleItems.map((item, index) => (
          <Box key={item.key} display="flex">
            <Box m={0.25}>
              <Chip color={item.color || 'primary'} size={item.size || 'small'} label={item.label} />
            </Box>
            {visibleItemCount && index === visibleItems.length - 1 && items.length > visibleItemCount ? (
              <IconButton size="small" onClick={handlePopoverOpen} sx={sx_config.button}>
                <Typography variant="caption" color="textPrimary">
                  +{items.length - visibleItems.length}
                </Typography>
              </IconButton>
            ) : null}
          </Box>
        ))}
      </div>
      {popoverAnchorRef.current ? (
        <Popover open={isPopoverOpen} anchorEl={popoverAnchorRef.current} onClose={handlePopoverClose}>
          {items.map((item) => (
            <Box key={item.key} m={0.25}>
              <Chip color={item.color || 'primary'} size={item.size || 'small'} label={item.label} />
            </Box>
          ))}
        </Popover>
      ) : null}
    </>
  );
}
