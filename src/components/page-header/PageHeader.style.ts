import { Theme } from '@mui/material/styles';

export const sx_config = {
  appBar: (theme: Theme) => ({
    boxShadow: 10,
    paddingLeft: theme.mixins.pageHeader.paddingLeft,
    paddingRight: theme.mixins.pageHeader.paddingRight,
    paddingBottom: `${theme.mixins.pageHeader.paddingBottom}px`,
    paddingTop: `${theme.mixins.pageHeader.paddingTop}px`,
    justifyContent: 'center',
    height: theme.mixins.pageHeader.appBar.height,
    // backgroundImage: `url('${background}')`,
    background: theme.mixins.pageHeader.background,
    color: theme.palette.text.secondary,
    zIndex: theme.zIndex.drawer + 10,
    [theme.breakpoints.up(theme.breakpoints.values.sm)]: {
      transition: theme.transitions.create(['margin', 'width'], {
        easing: theme.transitions.easing.sharp,
        duration: theme.transitions.duration.leavingScreen,
      }),
    },
  }),
  hamburgerIcon: (theme: Theme) => ({
    color: theme.palette.common.white,
    fontSize: theme.typography.pxToRem(30),
    width: 50,
    height: 50,
    [theme.breakpoints.up(theme.breakpoints.values.sm)]: {
      marginRight: (theme: Theme) => theme.spacing(2),
    },
  }),
};
