import React, { FC, ReactNode } from 'react';
import { Link } from 'react-router-dom';
import { AppBar, IconButton, Typography } from '@mui/material';
import { Box } from '@mui/material';
import ClearIcon from '@mui/icons-material/Clear';
import { ProfileMenu, ProfileMenuProps } from '../profile-menu/ProfileMenu';
import { sx_config } from './PageHeader.style';

export interface PageHeaderProps {
  title?: ReactNode;
  unAuthorized?: boolean;
  hideSidebarIcon?: boolean;
  isSideBarOpen?: boolean;
  toggleSidebar?: () => void;
  logoIcon?: ReactNode;
  sidebarIcon?: ReactNode;
  profileMenuProps?: ProfileMenuProps;
}

export const PageHeader: FC<PageHeaderProps> = ({
  title,
  isSideBarOpen,
  toggleSidebar,
  unAuthorized,
  hideSidebarIcon,
  logoIcon,
  sidebarIcon,
  profileMenuProps,
}) => {
  return (
    <AppBar position="fixed" sx={sx_config.appBar} elevation={0}>
      {unAuthorized ? (
        <Box display="flex" alignItems="center" justifyContent="space-between" width="100%" height="100%">
          <Box mr={2}>{logoIcon}</Box>
          <Typography variant="body1" color="textSecondary">
            {title}
          </Typography>
        </Box>
      ) : (
        <Box display="flex" alignItems="center" width="100%" height="100%">
          {!hideSidebarIcon ? (
            <>
              {!isSideBarOpen ? (
                <IconButton sx={sx_config.hamburgerIcon} edge="start" onClick={() => toggleSidebar()} color="inherit">
                  {sidebarIcon}
                </IconButton>
              ) : null}
              {isSideBarOpen ? (
                <IconButton sx={sx_config.hamburgerIcon} edge="start" onClick={() => toggleSidebar()}>
                  <ClearIcon color="inherit" fontSize="inherit" />
                </IconButton>
              ) : null}
            </>
          ) : null}
          <Box mr={2}>
            <Link to="/">{logoIcon}</Link>
          </Box>
          <Typography variant="body1" color="textSecondary">
            {title}
          </Typography>
          <Box ml="auto">{profileMenuProps ? <ProfileMenu {...profileMenuProps} /> : null}</Box>
        </Box>
      )}
    </AppBar>
  );
};
