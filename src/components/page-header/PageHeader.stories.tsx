import React from 'react';
import { Router, Link } from 'react-router-dom';
import { createBrowserHistory } from 'history';

import { ThemeProvider, StyledEngineProvider } from '@mui/material/styles';

import { theme } from '../../theme';
import { PageHeader } from './PageHeader';

const history = createBrowserHistory();

export default {
  title: 'PageHeader',
  component: PageHeader,
};

export const Default = (): React.ReactNode => {
  const logout = () => {
    console.log('logout');
  };

  return (
    <Router history={history}>
      <StyledEngineProvider injectFirst>
        <ThemeProvider theme={theme}>
          <PageHeader
            logoIcon={<>L</>}
            sidebarIcon={<>S</>}
            toggleSidebar={() => {}}
            isSideBarOpen={false}
            profileMenuProps={{
              logout,
              logoutLabel: 'Logout',
              profileContent: 'Test User',
              overrideProfileIconStyle: true,
              profileIcon: (
                <div className="test" style={{ color: 'green' }}>
                  P
                </div>
              ),
              menuItems: [
                { key: 1, children: 'Menu 1', component: Link, to: '/test' },
                { key: 2, children: 'Menu 2' },
              ],
            }}
          />
        </ThemeProvider>
      </StyledEngineProvider>
    </Router>
  );
};
