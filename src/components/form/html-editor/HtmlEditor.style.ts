import { Theme } from '@mui/material/styles';

export const sx_config = {
  label: {
    fontSize: 14,
  },
  labelFocus: {
    color: (theme: Theme) => theme.palette.primary.main,
  },
};
