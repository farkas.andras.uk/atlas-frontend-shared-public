import React, { FC, ReactNode, useState, FocusEvent, useEffect } from 'react';
import { Tooltip, TooltipProps } from '../../tooltip/Tooltip';
import ReactQuill from 'react-quill';
import { FieldProps } from 'formik';
import 'react-quill/dist/quill.snow.css';
import Box from '@mui/material/Box';
import FormLabel from '@mui/material/FormLabel';
import { ErrorMessage } from '../../util/ErrorMessage';
import { inErrorState } from '../../../utils/form';
import { sx_config } from './HtmlEditor.style';
import './HtmlEditor.css';

export interface HtmlEditorProps extends FieldProps {
  label?: ReactNode;
  showError?: boolean;
  disabled?: boolean;
  tooltipProps?: TooltipProps;
}

export const HtmlEditor: FC<HtmlEditorProps> = ({ field, form, showError, label, disabled, tooltipProps }) => {
  const hasError = inErrorState(form, field);
  const [editorInFocus, setEditorInFocus] = useState(false);
  const useFocusStyle = editorInFocus && !hasError;
  const linkElements = document.getElementsByClassName('ql-link');

  const onFocus = () => {
    if (!hasError) {
      setEditorInFocus(true);
    }
  };

  const onBlur = (e: FocusEvent<HTMLElement>) => {
    setEditorInFocus(false);
    field.onBlur(e);
    form.setFieldTouched(field.name, true, form.validateOnBlur);
  };

  const onChange = (content: string) => {
    if (form && field) {
      form.setFieldValue(field.name, content, form.validateOnChange);
      if (form.validateOnChange) {
        form.setFieldTouched(field.name, true, form.validateOnChange);
      }
    }
  };

  const alignLinkTooltipPosition = () => {
    const linkElement = linkElements && linkElements[0];
    const asNumber = (value: string) => +value.replace('px', '');

    if (linkElement) {
      linkElement.addEventListener('click', () => {
        const tooltipElements = document.getElementsByClassName('ql-tooltip');
        const editorElements = document.getElementsByClassName('ql-editor');
        const tooltipElement = tooltipElements && tooltipElements[0];
        const editorElement = editorElements && editorElements[0];

        if (tooltipElement && editorElement) {
          const { left, top, width } = window.getComputedStyle(tooltipElement);
          const editorWidth = window.getComputedStyle(editorElement).width;

          const leftAsNumber = asNumber(left);
          const maxLeft = asNumber(editorWidth) - asNumber(width);

          if (leftAsNumber < 0) {
            tooltipElement.setAttribute('style', `left:0; top:${top}`);
          } else if (leftAsNumber > maxLeft) {
            tooltipElement.setAttribute('style', `left:${maxLeft}px; top:${top}`);
          }
        }
      });
    }
  };

  useEffect(alignLinkTooltipPosition, [linkElements]);

  const component = (
    <Box mt={1} onBlur={onBlur} onFocus={onFocus}>
      {label ? (
        <FormLabel
          sx={[sx_config.label, useFocusStyle && sx_config.labelFocus]}
          component="label"
          disabled={disabled}
          error={hasError}
        >
          {label}
        </FormLabel>
      ) : null}
      <ReactQuill
        theme="snow"
        className={`htmlEditor ${hasError && 'error'} ${useFocusStyle && 'focus'}`}
        readOnly={disabled}
        onChange={onChange}
        value={field.value || ''}
      />

      {showError ? <ErrorMessage form={form} field={field} /> : null}
    </Box>
  );

  return tooltipProps ? <Tooltip {...tooltipProps}>{component}</Tooltip> : <>{component}</>;
};

HtmlEditor.defaultProps = {
  showError: true,
};
