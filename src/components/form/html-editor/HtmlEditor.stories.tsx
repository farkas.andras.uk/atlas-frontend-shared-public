import React from 'react';
import { Formik, Form, Field } from 'formik';
import Box from '@mui/material/Box';
import { HtmlEditor } from './HtmlEditor';

export default {
  title: 'Form/HtmlEditor',
  component: HtmlEditor,
};

interface FormValue {
  htmlEditor: string | null;
  htmlEditorWithLabel: string | null;
}

export const Default = (): React.ReactNode => {
  const onSubmit = () => {
    // Handle submit
  };

  return (
    <Formik
      initialValues={{ htmlEditor: null, htmlEditorWithLabel: 'With label content' } as FormValue}
      onSubmit={onSubmit}
    >
      <Form>
        <Box>
          <Field
            name="htmlEditor"
            component={(p) => {
              return <HtmlEditor {...p} tooltipProps={{ title: 'htmlEditor tooltip' }} />;
            }}
          />
        </Box>

        <Box mt={2}>
          <Field
            name="htmlEditorWithLabel"
            component={(p) => {
              return <HtmlEditor {...p} tooltipProps={{ title: 'htmlEditorWithLabel tooltip' }} />;
            }}
            label="With label"
          />
        </Box>
      </Form>
    </Formik>
  );
};
