import React, { useEffect, useMemo, useRef, useState } from 'react';
import { FieldProps } from 'formik';
import Box from '@mui/material/Box';
import IconButton from '@mui/material/IconButton';
import EditIcon from '@mui/icons-material/Edit';
import { ErrorMessage } from '../../util';
import './TitleEditor.css';

export interface TitleEditorProps extends FieldProps {
  className?: string;
  showError?: boolean;
  disabled?: boolean;
}

export function TitleEditor({ form, field, className, showError = true, disabled }: TitleEditorProps) {
  const input = useRef<HTMLInputElement>(null);
  const measure = useRef<HTMLSpanElement>(null);
  const [width, setWidth] = useState<number>(0);

  useEffect(() => {
    setInputWidth(field.value);
  }, [field.value]);

  const onEditClick = () => {
    if (input.current) {
      input.current.focus();
    }
  };

  const setInputWidth = (value: string) => {
    if (measure.current) {
      measure.current.innerText = value;

      setWidth(measure.current.getBoundingClientRect().width);
    }
  };

  const style = useMemo(() => ({ width }), [width]);

  return (
    <Box width="100%" overflow="hidden">
      <Box display="flex" alignItems="center">
        <input
          {...field}
          ref={input}
          autoComplete="off"
          className={`${className && className} input typography`}
          style={style}
          disabled={disabled}
        />
        {!disabled ? (
          <IconButton onClick={onEditClick}>
            <EditIcon fontSize="small" color="primary" />
          </IconButton>
        ) : null}
      </Box>
      {showError ? <ErrorMessage className={'errorMessage'} form={form} field={field} /> : null}
      <span ref={measure} className={'measure typography'} />
    </Box>
  );
}
