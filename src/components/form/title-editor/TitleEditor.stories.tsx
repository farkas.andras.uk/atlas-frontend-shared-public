import React from 'react';
import { Meta, Story } from '@storybook/react/types-6-0';
import { Field, Form, Formik } from 'formik';

import { TitleEditor, TitleEditorProps } from './TitleEditor';

export default {
  title: 'Form/TitleEditor',
  component: TitleEditor,
} as Meta;

export const Interactive: Story<TitleEditorProps> = (args) => (
  <Formik initialValues={{ text: 'Test title' }} onSubmit={undefined}>
    <Form>
      <Field name="text" component={TitleEditor} {...args} />
    </Form>
  </Formik>
);
