import { Theme } from '@mui/material/styles';

export const sx_config = {
  typography: {
    fontWeight: (theme: Theme) => theme.typography.fontWeightBold as any,
    fontFamily: (theme: Theme) => theme.typography.fontFamily,
    fontSize: (theme: Theme) => theme.typography.pxToRem(20),
  },
  input: {
    background: 'transparent',
    border: 'none',
    width: '100%',
    color: (theme: Theme) => theme.palette.primary.main,
    padding: (theme: Theme) => theme.spacing(1.5, 0),
    maxWidth: '100%',
    minWidth: 50,

    '&:focus-visible': {
      outline: 'none',
    },
  },
  errorMessage: {
    marginTop: (theme: Theme) => theme.spacing(-1.5),
  },
  measure: {
    position: 'absolute',
    whiteSpace: 'pre',
    top: -9999,
    left: -9999,
  },
};
