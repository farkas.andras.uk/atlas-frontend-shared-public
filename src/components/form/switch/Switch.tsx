import React, { FC, ChangeEvent } from 'react';
import { FieldProps } from 'formik';
import { Tooltip, TooltipProps } from '../../tooltip/Tooltip';

import FormControlLabel, { FormControlLabelProps } from '@mui/material/FormControlLabel';
import MuiSwitch, { SwitchProps as MuiSwitchProps } from '@mui/material/Switch';

import { ErrorMessage } from '../../util/ErrorMessage';

export interface SwitchProps extends Omit<FormControlLabelProps, 'form' | 'control'>, Partial<FieldProps> {
  color?: 'primary' | 'secondary' | 'default';
  switchProps?: MuiSwitchProps;
  showError?: boolean;
  onChanged?: (checked: boolean) => void;
  tooltipProps?: TooltipProps;
}

export const Switch: FC<SwitchProps> = ({
  label,
  color,
  showError,
  switchProps,
  form,
  field,
  name,
  onChange,
  onChanged,
  onBlur,
  checked,
  disabled,
  labelPlacement,
  className,
  tooltipProps,
}) => {
  const handleChange = (event: ChangeEvent<HTMLInputElement>, checked: boolean) => {
    if (form && field) {
      form.setFieldValue(field.name, checked);
    }

    if (onChange) {
      onChange(event, checked);
    }

    if (onChanged) {
      onChanged(checked);
    }
  };

  const component = <MuiSwitch disabled={disabled} color={color} {...(switchProps || {})} />;

  return (
    <>
      <FormControlLabel
        label={label}
        name={field?.name || name}
        className={className}
        onChange={handleChange}
        onBlur={field?.onBlur || onBlur}
        disabled={disabled}
        control={tooltipProps ? <Tooltip {...tooltipProps}>{component}</Tooltip> : component}
        checked={Boolean(field?.value || checked)}
        labelPlacement={labelPlacement}
      />
      {showError ? <ErrorMessage form={form} field={field} /> : null}
    </>
  );
};

Switch.defaultProps = {
  color: 'primary',
  showError: false,
};
