import React from 'react';
import { Formik, Form, Field } from 'formik';
import { ThemeProvider, StyledEngineProvider } from '@mui/material/styles';
import { LocalizationProvider } from '@mui/x-date-pickers';

import { theme } from '../../../theme';
import { Switch } from './Switch';

export default {
  title: 'Form/Switch',
  component: Switch,
};

interface FormValue {
  switch: boolean;
  switchWithLabel: boolean;
  switchDisabled: boolean;
}

export const Default = (): React.ReactNode => {
  const onSubmit = () => {
    // Handle submit
  };

  return (
    <StyledEngineProvider injectFirst>
      <ThemeProvider theme={theme}>
        <Formik
          initialValues={{ switch: false, switchWithLabel: true, switchDisabled: false } as FormValue}
          onSubmit={onSubmit}
        >
          <Form>
            <Field
              name="switch"
              component={(p) => {
                return <Switch {...p} tooltipProps={{ title: 'switch tooltip' }} />;
              }}
            />
            <Field
              name="switchWithLabel"
              component={(p) => {
                return <Switch {...p} tooltipProps={{ title: 'switchWithLabel tooltip' }} />;
              }}
              label="With label"
            />
            <Field name="switchDisabled" component={Switch} label="Disabled" disabled />
          </Form>
        </Formik>
      </ThemeProvider>
    </StyledEngineProvider>
  );
};
