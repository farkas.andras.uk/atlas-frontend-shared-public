import React, { FC, ReactNode, useState } from 'react';
import { FieldProps } from 'formik';
import Dropzone, { DropEvent, DropzoneProps, FileRejection } from 'react-dropzone';
import i18n from 'i18next';
import Box from '@mui/material/Box';
import Typography from '@mui/material/Typography';
import FormHelperText from '@mui/material/FormHelperText';
import CircularProgress from '@mui/material/CircularProgress';
import InputLabel from '@mui/material/InputLabel';
import UploadIcon from '@mui/icons-material/CloudUpload';
import { ErrorMessage } from '../../util/ErrorMessage';
import { ImagePreview } from '../../image-preview/ImagePreview';
import { sx_config } from './FileUpload.style';

enum FileUploadError {
  UNSUPPORTED_FILE_TYPE = 'UNSUPPORTED_FILE_TYPE',
}

export enum FileType {
  IMAGE = 'image/*',
  FILE = 'application/*',
  DOCUMENT = 'image/png, image/jpeg, application/pdf',
  EXCEL = 'text/csv, application/vnd.ms-excel, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
}

export const FileErrorLabelMap: Record<FileUploadError, () => string> = {
  [FileUploadError.UNSUPPORTED_FILE_TYPE]: () => i18n.t('ERRORS.UNSUPPORTED'),
};

export interface FileUploadProps extends DropzoneProps, Partial<FieldProps> {
  loading?: boolean;
  label?: ReactNode;
  showError?: boolean;
  dropInformation?: ReactNode | string;
  accept?: any;
  baseImageUrl?: string;
}

export const FileUpload: FC<FileUploadProps> = ({
  multiple = false,
  onDropRejected,
  onDropAccepted,
  loading,
  form,
  field,
  label,
  showError = true,
  dropInformation,
  accept = FileType.FILE,
  baseImageUrl = '',
  ...props
}) => {
  const [error, setError] = useState<FileUploadError | null>(null);
  const [imagePreviews, setImagePreviews] = useState<string[]>([]);
  const [imagePreviewsLoading, setImagePreviewsLoading] = useState<boolean>(false);

  const getPreview = (file: File): Promise<string> => {
    return new Promise((resolve) => {
      const reader = new FileReader();
      reader.readAsDataURL(file);
      reader.onloadend = () => {
        resolve(reader.result as string);
      };
    });
  };

  const getAllPreviews = (files: File[]): Promise<string[]> => {
    const allPreviews: string[] = [];

    return new Promise((resolve) => {
      files.forEach(async (file) => {
        const preview = await getPreview(file);
        allPreviews.push(preview);

        if (files.length === allPreviews.length) {
          resolve(allPreviews);
        }
      });
    });
  };

  const setFilesPreview = async (files: File[]) => {
    setImagePreviewsLoading(true);
    const allPreviews = await getAllPreviews(files);

    setImagePreviewsLoading(false);
    setImagePreviews(allPreviews);
  };

  const onRejected = (files: FileRejection[], event: DropEvent) => {
    setError(FileUploadError.UNSUPPORTED_FILE_TYPE);

    if (onDropRejected) {
      onDropRejected(files, event);
    }
  };

  const onAccepted = (files: File[], event: DropEvent) => {
    setError(null);
    setFilesPreview(files);

    if (onDropAccepted) {
      onDropAccepted(files, event);
    }

    if (form && field) {
      form.setFieldValue(field.name, files);
    }
  };

  const getValue = () => {
    if (!field?.value) {
      return null;
    }

    const values = Array.isArray(field.value) ? field.value : [field.value];

    if (accept === FileType.IMAGE && !!values.length && !imagePreviews.length) {
      setImagePreviews(values.map((value) => `${baseImageUrl}/${(value && value.name) || ''}`));
    }

    return values;
  };

  const value = getValue();

  const images = imagePreviews.map((src, index) => {
    const alt = (value && value[index] && value[index].name) || '';
    return { src, alt };
  });

  return (
    <div>
      {label ? (
        <InputLabel sx={sx_config.label} shrink>
          {label}
        </InputLabel>
      ) : null}
      <Dropzone multiple={multiple} onDropRejected={onRejected} onDropAccepted={onAccepted} accept={accept} {...props}>
        {({ getRootProps, getInputProps }) => {
          return (
            <Box display={multiple ? 'block' : 'flex'}>
              <Box {...getRootProps()} sx={[sx_config.root, props.disabled ? sx_config.disabled : sx_config.enabled]}>
                {loading ? (
                  <CircularProgress />
                ) : (
                  <div>
                    <input {...getInputProps()} />
                    {dropInformation}
                    <UploadIcon fontSize="large" color="primary" />
                  </div>
                )}
              </Box>
              {accept === FileType.IMAGE ? (
                <ImagePreview images={images} multiple={multiple} loading={imagePreviewsLoading} />
              ) : null}
            </Box>
          );
        }}
      </Dropzone>
      {showError ? <ErrorMessage form={form} field={field} /> : null}
      <FormHelperText error>{error && FileErrorLabelMap[error] ? FileErrorLabelMap[error]() : null}</FormHelperText>
      {value && accept === FileType.FILE
        ? value.map((val: File) =>
            val.type ? (
              <Typography key={val.name}>{val.name}</Typography>
            ) : (
              <a
                key={val.name}
                target="_blank"
                rel="noopener noreferrer"
                href={`${baseImageUrl}/${(val && val.name) || ''}`}
              >
                <Typography>{val.name}</Typography>
              </a>
            )
          )
        : null}
    </div>
  );
};
