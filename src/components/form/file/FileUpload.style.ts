import { Theme, alpha } from '@mui/material/styles';

export const sx_config = {
  root: (theme: Theme) => ({
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    border: `1px dashed ${alpha(theme.palette.primary.main, 0.2)}`,
    transition: theme.transitions.create(['border', 'background-color'], {
      duration: theme.transitions.duration.complex,
    }),
    height: theme.spacing(16),
    width: '100%',
    cursor: 'pointer',
    textAlign: 'center',
    '&:focus': {
      outline: 'none',
    },
    '&.active.accept': {
      backgroundColor: 'red',
      '& .drop': {
        display: 'block',
      },
    },
    '&.active.reject': {
      borderColor: theme.palette.error.main,
      '& .reject': {
        display: 'block',
      },
    },
    '@keyframes float': {
      '0%': {
        transform: 'translateY(0px)',
      },
      '50%': {
        transform: 'translateY(-4px)',
      },
      '100%': {
        transform: 'translateY(0px)',
      },
    },
  }),
  disabled: {
    cursor: 'default',
  },
  enabled: {
    '&:hover': {
      borderColor: (theme: Theme) => theme.palette.primary.main,

      '& .MuiSvgIcon-root': {
        animationName: 'float',
        animationDuration: '1s',
        animationTimingFunction: 'ease-out',
        animationFillMode: 'forwards',
        animationIterationCount: 'infinite',
      },
    },
  },
  label: {
    marginBottom: (theme: Theme) => theme.spacing(1),
  },
};
