import React from 'react';
import { Meta, Story } from '@storybook/react/types-6-0';

import { FileUpload, FileUploadProps } from './FileUpload';

export default {
  title: 'Form/FileUpload',
  component: FileUpload,
} as Meta;

export const Interactive: Story<FileUploadProps> = (args) => <FileUpload {...args} />;
