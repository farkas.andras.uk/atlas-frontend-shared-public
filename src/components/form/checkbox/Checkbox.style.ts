import { Theme } from '@mui/material/styles';

export const sx_config = {
  root: {
    '& .MuiIconButton-root': {
      height: 42,
      width: 42,
      display: 'flex',
      alignItems: 'baseline',
      boxSizing: 'border-box',
    },
  },
  center: {
    marginLeft: 0,
    marginRight: 0,
    width: '100%',
    justifyContent: 'center',
  },
  dark: {
    '& .MuiCheckbox-root:not(.Mui-disabled)': {
      color: (theme: Theme) => theme.palette.common.black,
    },
    '& .MuiCheckbox-root.Mui-checked:not(.Mui-disabled)': {
      color: (theme: Theme) => theme.palette.primary.main,
    },
  },
  light: {
    '& .MuiCheckbox-root:not(.Mui-disabled)': {
      color: (theme: Theme) => theme.palette.common.white,
    },
    '& .MuiCheckbox-root.Mui-checked:not(.Mui-disabled)': {
      color: (theme: Theme) => theme.palette.common.white,
    },
  },
  defaultLabel: {
    color: (theme: Theme) => theme.palette.common.white,
  },
  primaryLabel: {
    color: (theme: Theme) => theme.palette.primary.main,
    fontWeight: 'bold',
  },
};
