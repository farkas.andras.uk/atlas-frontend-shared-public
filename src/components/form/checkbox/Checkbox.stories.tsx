import React from 'react';
import { Formik, Form, Field } from 'formik';

import { ThemeProvider, StyledEngineProvider } from '@mui/material/styles';

import { theme } from '../../../theme';
import { Checkbox } from './Checkbox';

export default {
  title: 'Form/Checkbox',
  component: Checkbox,
};

interface FormValue {
  checkbox: boolean;
  checkboxWithLabel: boolean;
}

export const Default = (): React.ReactNode => {
  const onSubmit = (values: FormValue) => {};

  return (
    <StyledEngineProvider injectFirst>
      <ThemeProvider theme={theme}>
        <Formik initialValues={{ checkbox: false, checkboxWithLabel: true } as FormValue} onSubmit={onSubmit}>
          <Form>
            <Field name="checkbox" component={Checkbox} theme="dark" />
            <Field name="checkboxWithLabel" component={Checkbox} theme="dark" label="With Label" />
            <Field
              name="checkboxWithLabel"
              component={Checkbox}
              theme="dark"
              label="With Primary Label"
              labelColor="primary"
            />
          </Form>
        </Formik>
      </ThemeProvider>
    </StyledEngineProvider>
  );
};
