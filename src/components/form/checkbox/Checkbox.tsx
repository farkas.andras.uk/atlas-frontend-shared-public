import React, { FC, ReactNode, ChangeEvent } from 'react';
import { FieldProps } from 'formik';
import FormControlLabel, { FormControlLabelProps } from '@mui/material/FormControlLabel';
import MuiCheckbox, { CheckboxProps as MuiCheckboxProps } from '@mui/material/Checkbox';
import { ErrorMessage } from '../../util';
import { sx_config } from './Checkbox.style';

export interface CheckboxProps extends Omit<Partial<FormControlLabelProps>, 'form' | 'control'>, Partial<FieldProps> {
  color?: 'primary' | 'secondary' | 'default';
  checkBoxProps?: MuiCheckboxProps;
  showError?: boolean;
  center?: boolean;
  theme?: 'dark' | 'light';
  icon?: ReactNode;
  labelColor?: 'primary' | 'default';
  onChanged?: (value: boolean) => void;
}

export const Checkbox: FC<CheckboxProps> = ({
  label,
  color,
  showError,
  checkBoxProps,
  form,
  field,
  name,
  onChange,
  onBlur,
  checked,
  disabled,
  center,
  theme,
  icon,
  labelColor,
  onChanged,
}) => {
  const handleChange = (event: ChangeEvent<HTMLInputElement>, checked: boolean) => {
    if (form && field) {
      form.setFieldValue(field.name, checked);
    }

    if (onChange) {
      onChange(event, checked);
    }

    if (onChanged) {
      onChanged(checked);
    }
  };

  return (
    <div>
      <FormControlLabel
        sx={[sx_config.root, sx_config[theme], sx_config[`${labelColor}Label`], center && { ...sx_config.center }]}
        label={label}
        name={field?.name || name}
        onChange={handleChange}
        onBlur={field?.onBlur || onBlur}
        disabled={disabled}
        control={<MuiCheckbox disabled={disabled} color={color} icon={icon} {...(checkBoxProps || {})} />}
        checked={Boolean(field?.value || checked)}
      />
      {showError ? <ErrorMessage form={form} field={field} /> : null}
    </div>
  );
};

Checkbox.defaultProps = {
  color: 'primary',
  showError: false,
};
