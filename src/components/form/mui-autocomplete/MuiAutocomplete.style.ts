import { Theme } from '@mui/material/styles';

export const sx_config = {
  validationMessage: (theme: Theme) => ({
    color: theme.palette.error.main,
  }),
};
