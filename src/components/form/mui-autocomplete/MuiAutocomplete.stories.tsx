import React from 'react';
import type { Meta, StoryObj } from '@storybook/react';
import { Box } from '@mui/material';
import { MuiAutocomplete } from './MuiAutocomplete';
import { MuiAutocompleteBaseProps, MuiAutocompleteOptionProps } from './MuiAutocompleteInterfaces';
import { CountryType, countries } from './CountryDemoData';
import { MovieType, top100Movie } from './MovieDemoData';

const meta: Meta<typeof MuiAutocomplete> = {
  title: 'Form/MuiAutocomplete',
  component: MuiAutocomplete,
  tags: ['autodocs'],
};

interface MuiAutocompleteCountriesProps extends MuiAutocompleteBaseProps, MuiAutocompleteOptionProps<CountryType> {}

export default meta;
type CountriesStory = StoryObj<MuiAutocompleteCountriesProps>;

const paginate = async (array, page: number, pageSize: number) => {
  const randomMillis = Math.floor(Math.random() * 1500);
  await new Promise((resolve) => setTimeout(resolve, randomMillis));
  return array.slice(page * pageSize, (page + 1) * pageSize);
};

const staticOptionsData = [
  { code: 'AD', label: 'Andorra', phone: '376', disabled: true },
  { code: 'BV', label: 'Bouvet Island', phone: '47' },
  { code: 'CY', label: 'Cyprus', phone: '357' },
];

export const FixedListDemo: CountriesStory = {
  args: {
    id: 'demo-autocomplete',
    consoleLogData: true,
    textFieldProps: {
      label: 'Choose a value',
      required: true,
    },
    onChange: (event, value) => console.log({ value }),
    disabled: false,
    multiple: true,
    limitTags: 2,
    disableClearable: true,
    validationMessage: 'Test Error',
    staticOptionsData: staticOptionsData,
    defaultValue: [staticOptionsData[0], staticOptionsData[1]],
    renderOption: (props, option) => {
      return (
        <Box component="li" sx={{ '& > img': { mr: 2, flexShrink: 0 } }} {...props}>
          <img
            loading="lazy"
            width="20"
            src={`https://flagcdn.com/w20/${option.code.toLowerCase()}.png`}
            srcSet={`https://flagcdn.com/w40/${option.code.toLowerCase()}.png 2x`}
            alt=""
          />
          <span
            style={
              {
                /* backgroundColor: 'blue', color: 'red' */
              }
            }
          >
            {option.label} ({option.code}) +{option.phone}
          </span>
        </Box>
      );
    },
    sx: {
      width: 500,
      backgroundColor: 'primary',
      color: 'red',
      '&:hover': {
        backgroundColor: 'primary',
        opacity: [0.9, 0.8, 0.7],
      },
    },
  },
};

export const CountriesDemo: CountriesStory = {
  args: {
    id: 'demo-autocomplete',
    consoleLogData: false,
    textFieldProps: {
      label: 'Choose a country',
    },
    onChange: (event, value) => console.log({ value }),
    disabled: false,
    multiple: true,
    limitTags: 2,
    // groupBy: (option) => option.code,
    asyncDataLoader: {
      pageSize: 20,
      debounceMs: 1000,
      loaderFunction: async (page: number, pageSize: number, inputValue: string) => {
        const items = await paginate(countries, page, pageSize);
        console.log({ page, items, inputValue });
        return items;
      },
    },
    renderOption: (props, option) => {
      return (
        <Box component="li" sx={{ '& > img': { mr: 2, flexShrink: 0 } }} {...props}>
          <img
            loading="lazy"
            width="20"
            src={`https://flagcdn.com/w20/${option.code.toLowerCase()}.png`}
            srcSet={`https://flagcdn.com/w40/${option.code.toLowerCase()}.png 2x`}
            alt=""
          />
          <span
            style={
              {
                /* backgroundColor: 'blue', color: 'red' */
              }
            }
          >
            {option.label} ({option.code}) +{option.phone}
          </span>
        </Box>
      );
    },
    sx: {
      width: 500,
      backgroundColor: 'primary',
      color: 'red',
      '&:hover': {
        backgroundColor: 'primary',
        opacity: [0.9, 0.8, 0.7],
      },
    },
  },
};

interface MuiAutocompleteMoviesProps extends MuiAutocompleteBaseProps, MuiAutocompleteOptionProps<MovieType> {}

type MoviesStory = StoryObj<MuiAutocompleteMoviesProps>;
export const MoviesDemo: MoviesStory = {
  args: {
    id: 'demo-autocomplete',
    consoleLogData: true,
    textFieldProps: {
      label: 'Movie',
    },
    onChange: (event, value) => console.log({ value }),
    disabled: false,
    multiple: false,
    // limitTags: 2,
    defaultValue: { label: 'The Godfather', year: 1972 },
    groupBy: (option) => String(option.year),
    asyncDataLoader: {
      pageSize: 20,
      debounceMs: 1000,
      loaderFunction: async (page: number, pageSize: number, inputValue: string) => {
        const items = await paginate(top100Movie, page, pageSize);
        console.log({ page, items, inputValue });
        return items;
      },
    },
    renderOption: (props, option) => {
      return (
        <Box component="li" sx={{ '& > img': { mr: 2, flexShrink: 0 } }} {...props}>
          {option.label} ({option.year})
        </Box>
      );
    },
    sx: {
      width: 500,
      backgroundColor: 'primary',
      '&:hover': {
        backgroundColor: 'primary',
        opacity: [0.9, 0.8, 0.7],
      },
    },
  },
};
