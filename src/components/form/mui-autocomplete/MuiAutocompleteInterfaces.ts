import { AutocompleteProps, AutocompleteRenderOptionState, TextFieldProps } from '@mui/material';
import { TooltipProps } from '../../tooltip/Tooltip';

export interface MuiAutocompleteOptionProps<T> {
  staticOptionsData?: AutocompleteProps<T, boolean | undefined, boolean | undefined, boolean | undefined>['options'];
  asyncDataLoader?: {
    pageSize: number;
    debounceMs: number;
    loaderFunction: (
      page: number,
      pageSize: number,
      inputValue?: string
    ) => Promise<AutocompleteProps<T, boolean | undefined, boolean | undefined, boolean | undefined>['options']>;
  };
  defaultValue?: T | T[];
  groupBy?: AutocompleteProps<T, undefined, undefined, undefined>['groupBy'];
  renderOption?: (
    props: React.HTMLAttributes<HTMLLIElement>,
    option: T,
    state: AutocompleteRenderOptionState
  ) => React.ReactNode;
}

export interface MuiAutocompleteBaseProps {
  id: string;
  textFieldProps: TextFieldProps;
  consoleLogData?: boolean;
  disabled?: boolean;
  multiple?: boolean;
  disableClearable?: boolean;
  limitTags?: AutocompleteProps<undefined, undefined, undefined, undefined>['limitTags'];
  onChange?: (event: any, value: any) => void;
  sx?: AutocompleteProps<undefined, undefined, undefined, undefined>['sx'];
  tooltipProps?: TooltipProps;
  validationMessage?: string;
}
