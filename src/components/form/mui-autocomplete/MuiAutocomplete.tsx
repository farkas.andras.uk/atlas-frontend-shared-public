import React, { Fragment, useState, useEffect } from 'react';
import { Autocomplete, TextField, Box } from '@mui/material';
import CircularProgress from '@mui/material/CircularProgress';
import { MuiAutocompleteBaseProps, MuiAutocompleteOptionProps } from './MuiAutocompleteInterfaces';
import { isEmpty } from 'lodash';
import { Tooltip } from '../../tooltip/Tooltip';
import { sx_config } from './MuiAutocomplete.style';

export interface OptionType {
  label: string;
}

interface MuiAutocompleteProps extends MuiAutocompleteBaseProps, MuiAutocompleteOptionProps<OptionType> {}

export const MuiAutocomplete = ({
  consoleLogData,
  textFieldProps,
  staticOptionsData,
  asyncDataLoader,
  multiple,
  onChange,
  tooltipProps,
  validationMessage,
  ...props
}: MuiAutocompleteProps) => {
  const [open, setOpen] = useState(false);
  const [loading, setLoading] = useState<boolean>(false);
  const [page, setPage] = useState(0);
  const [options, setOptions] = useState(staticOptionsData ? staticOptionsData : []);
  const [inputValue, setInputValue] = useState('');
  const [position, setPosition] = useState(0);
  const [listboxNode, setListboxNode] = useState<{ scrollTop: number; scrollHeight: number; clientHeight: number }>();
  const [readOnly, setReadOnly] = useState<boolean>(false);

  useEffect(() => {
    if (!open && !staticOptionsData) {
      setPage(0);
      setOptions([]);
    }

    if (!open && staticOptionsData) {
      setOptions(staticOptionsData);
    }
  }, [open, staticOptionsData]);

  useEffect(() => {
    if (page === 0) {
      setListboxNode(undefined);
      setPosition(0);
    }
  }, [page]);

  useEffect(() => {
    if (listboxNode) {
      listboxNode.scrollTop = position;
    }
  }, [position, listboxNode]);

  useEffect(() => {
    let delayDebounceFn;
    if (open && asyncDataLoader) {
      setPage(0);
      if (!isEmpty(inputValue)) {
        delayDebounceFn = setTimeout(() => {
          setInputValue(inputValue);
          loader(0, inputValue);
        }, asyncDataLoader.debounceMs);
      } else {
        loader(0);
      }
    }

    return () => clearTimeout(delayDebounceFn);
  }, [open, asyncDataLoader, inputValue]);

  const loader = async (page?: number, inputValue?: string) => {
    if (consoleLogData) console.log('loader');
    if (inputValue) setReadOnly(true);
    setLoading(true);
    setOptions([]);
    const pageOfOptions = await asyncDataLoader.loaderFunction(page, asyncDataLoader.pageSize, inputValue);
    setOptions([...pageOfOptions]);
    setLoading(false);
    if (inputValue) setReadOnly(false);
  };

  const loadMoreResults = async () => {
    if (consoleLogData) console.log('loadMoreResults');
    setLoading(true);
    const nextPage = page + 1;
    setPage(nextPage);
    const pageOfOptions = await asyncDataLoader.loaderFunction(nextPage, asyncDataLoader.pageSize);
    setOptions([...options, ...pageOfOptions]);
    setLoading(false);
  };

  const handleScroll = async (event) => {
    setListboxNode(event.currentTarget);
    if (listboxNode && listboxNode.scrollHeight - (listboxNode.scrollTop + listboxNode.clientHeight) <= 1) {
      setPosition(listboxNode.scrollTop);
      loadMoreResults();
    }
  };

  if (consoleLogData) {
    console.log({
      page,
      open,
      options,
      loading,
      position,
      listboxNode: listboxNode && {
        scrollHeight: listboxNode.scrollHeight,
        scrollTop: listboxNode.scrollTop,
        clientHeight: listboxNode.clientHeight,
      },
      inputValue,
      readOnly,
    });
  }

  return (
    <Tooltip {...tooltipProps}>
      <Autocomplete
        open={open}
        onOpen={() => {
          setOpen(true);
        }}
        onClose={() => {
          setOpen(false);
          setPosition(0);
        }}
        loading={loading}
        options={options}
        getOptionLabel={(option: any) => option.label}
        ListboxProps={{
          onScroll: asyncDataLoader && asyncDataLoader.pageSize && handleScroll,
        }}
        inputValue={inputValue}
        onChange={(event, value, reason) => {
          onChange(event, value);
        }}
        onInputChange={(event, newInputValue) => {
          setInputValue(newInputValue);
        }}
        multiple={multiple}
        readOnly={readOnly}
        isOptionEqualToValue={!multiple ? (option, value) => option.id === value.id : undefined}
        renderInput={(params) => (
          <Fragment>
            <TextField
              {...textFieldProps}
              {...params}
              // helperText={`Current page: ${page}`}
              variant={'standard'}
              InputProps={{
                ...params.InputProps,
                endAdornment: (
                  <React.Fragment>
                    {loading ? <CircularProgress color="inherit" size={20} /> : null}
                    {params.InputProps.endAdornment}
                  </React.Fragment>
                ),
              }}
            />
            <Box sx={[sx_config.validationMessage]}>{validationMessage}</Box>
          </Fragment>
        )}
        {...props}
      />
    </Tooltip>
  );
};
