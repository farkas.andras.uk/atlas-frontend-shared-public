import React from 'react';
import {
  MobileDatePicker,
  MobileDateTimePicker,
  DatePicker as MuiDatePicker,
  DateTimePicker,
} from '@mui/x-date-pickers';
import { DatePickerProps } from '@mui/lab';
import { sx_config } from './Datepicker.style';

export interface DatepickerProps extends DatePickerProps<any> {
  type?: 'MobileDatePicker' | 'MobileDateTimePicker' | 'DatePicker' | 'DateTimePicker';
  validationMessage?: string;
}

export const Datepicker = ({ type = 'DatePicker', validationMessage, ...props }: DatepickerProps) => {
  let DatePicker;
  switch (type) {
    case 'MobileDatePicker':
      DatePicker = MobileDatePicker;
      break;
    case 'MobileDateTimePicker':
      DatePicker = MobileDateTimePicker;
      break;
    case 'DatePicker':
      DatePicker = MuiDatePicker;
      break;
    case 'DateTimePicker':
      DatePicker = DateTimePicker;
      break;
    default:
      DatePicker = DateTimePicker;
      break;
  }

  return (
    <>
      <DatePicker
        sx={sx_config.datePicker}
        slotProps={{
          textField: {
            helperText: validationMessage,
          },
        }}
        {...props}
      />
    </>
  );
};
