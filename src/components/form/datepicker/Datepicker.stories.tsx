import React from 'react';
import { Formik, Form, Field } from 'formik';
import moment from 'moment';
import { ThemeProvider, StyledEngineProvider } from '@mui/material/styles';
import { theme } from '../../../theme';
import { Datepicker } from './Datepicker';

export default {
  title: 'Form/Datepicker',
  component: Datepicker,
};

interface FormValue {
  date: string | null;
  dateWithInit: moment.Moment;
}

export const Default = (): React.ReactNode => {
  const onSubmit = () => {
    // Handle submit
  };

  return (
    <StyledEngineProvider injectFirst>
      <ThemeProvider theme={theme}>
        <Formik
          initialValues={{ date: null, dateTime: null, dateWithInit: moment.utc() } as FormValue}
          onSubmit={onSubmit}
        >
          <Form>
            <Field type="MobileDatePicker" name="date" component={Datepicker} />
            <Field
              type="MobileDatePicker"
              label="Date picker for mobile"
              labelColor="default"
              value={moment.utc()}
              component={Datepicker}
            />
            <Field
              type="MobileDateTimePicker"
              label="Date and time picker for mobile"
              value={moment.utc()}
              component={Datepicker}
            />
            <Field
              type="DatePicker"
              label="Date picker for desktop"
              name="dateTime"
              component={Datepicker}
              onChange={(value) => {
                console.log({ value });
              }}
            />
            <Field
              type="DateTimePicker"
              label="Date and time picker for desktop"
              labelColor="default"
              name="dateTime"
              component={Datepicker}
            />
          </Form>
        </Formik>
      </ThemeProvider>
    </StyledEngineProvider>
  );
};
