import { Theme } from '@mui/material/styles';

export const sx_config = {
  datePicker: (theme: Theme) => ({
    width: '100%',
    '& label': {
      backgroundColor: 'rgba(255,255,255, 0.0)',
      left: '-15px',
    },
    '& .MuiInputBase-input': {
      padding: '10px 0 5px',
    },
    '& .MuiOutlinedInput-root': {
      '& fieldset': {
        borderRadius: '0',
        borderWidth: '0px',
        borderBottom: '1px solid rgba(0, 0, 0, 0.42)',
      },
      '&.Mui-focused fieldset': {
        borderWidth: '0px',
        borderBottom: (theme: Theme) => `1px solid ${theme.palette.primary.main}`,
      },
    },
  }),
};
