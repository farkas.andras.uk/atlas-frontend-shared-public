import React, { FC, ReactNode, FocusEvent } from 'react';
import { FieldProps } from 'formik';
import Box from '@mui/material/Box';
import FormLabel from '@mui/material/FormLabel';
import { Checkbox } from '../checkbox/Checkbox';
import { ErrorMessage } from '../../util';
import { inErrorState } from '../../../utils/form';
import { sx_config } from './CheckboxGroup.style';

export interface CheckboxGroupOptionType {
  value: number;
  checked?: boolean;
  label: string | ReactNode;
}

export interface CheckboxGroupProps extends FieldProps {
  label?: ReactNode;
  showError?: boolean;
  options: CheckboxGroupOptionType[];
  disabled?: boolean;
  disabledIcon: ReactNode;
  onChange?: (checked: boolean, value: number) => void;
}

export const CheckboxGroup: FC<CheckboxGroupProps> = ({
  field,
  form,
  label,
  showError,
  options,
  disabled,
  disabledIcon,
  onChange,
}) => {
  const hasError = inErrorState(form, field);

  const onBlur = (e: FocusEvent<HTMLElement>) => {
    field.onBlur(e);
    form.setFieldTouched(field.name, true, form.validateOnBlur);
  };

  const handleChange = (checked: boolean, value: number) => {
    const changedOptions = options
      .map((option) => {
        const fieldOption =
          field.value && Array.isArray(field.value)
            ? field.value.find((item) => item.value === option.value) || option
            : option;

        return {
          ...fieldOption,
          checked: value === fieldOption.value ? checked : fieldOption.checked,
        };
      })
      .filter((option) => option.checked);

    if (form && field) {
      form.setFieldValue(field.name, changedOptions, form.validateOnChange);
    }

    if (onChange) {
      onChange(checked, value);
    }
  };

  return (
    <Box mt={1} onBlur={onBlur}>
      {label ? (
        <Box display="flex" alignItems="center" justifyContent="space-between">
          <FormLabel component="legend" error={hasError} sx={sx_config.label}>
            {label}
          </FormLabel>
          {disabled ? disabledIcon : null}
        </Box>
      ) : null}

      <Box mt={1} ml={2}>
        {options &&
          options.map((option) => {
            const optionChecked =
              field.value && Array.isArray(field.value) && field.value.find(({ value }) => value === option.value);

            return (
              <Checkbox
                key={option.value}
                {...option}
                checked={!!optionChecked && optionChecked.checked}
                theme="dark"
                disabled={disabled}
                onChange={(e, checked) => handleChange(checked, option.value)}
              />
            );
          })}
      </Box>

      {showError ? <ErrorMessage form={form} field={field} /> : null}
    </Box>
  );
};
