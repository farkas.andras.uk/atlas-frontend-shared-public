import React from 'react';
import { Formik, Form, Field } from 'formik';

import { ThemeProvider, StyledEngineProvider } from '@mui/material/styles';

import { theme } from '../../../theme';
import { CheckboxGroup, CheckboxGroupOptionType } from './CheckboxGroup';

export default {
  title: 'Form/CheckboxGroup',
  component: CheckboxGroup,
};

interface FormValue {
  checkboxGroup: CheckboxGroupOptionType[];
}

export const Default = (): React.ReactNode => {
  const onSubmit = () => {
    // Handle submit
  };

  return (
    <StyledEngineProvider injectFirst>
      <ThemeProvider theme={theme}>
        <Formik
          initialValues={{ checkboxGroup: [{ value: 1, label: 'One', checked: true }] } as FormValue}
          onSubmit={onSubmit}
        >
          <Form>
            <Field
              name="checkboxGroup"
              component={CheckboxGroup}
              theme="dark"
              options={[
                { value: 1, label: 'One' },
                { value: 2, label: 'Two' },
                { value: 3, label: 'Three' },
                { value: 4, label: 'Four' },
              ]}
            />
          </Form>
        </Formik>
      </ThemeProvider>
    </StyledEngineProvider>
  );
};
