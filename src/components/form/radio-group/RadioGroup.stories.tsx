import React from 'react';
import { Formik, Form, Field } from 'formik';
import { ThemeProvider, Theme, StyledEngineProvider } from '@mui/material/styles';
import { LocalizationProvider } from '@mui/x-date-pickers';
import Box from '@mui/material/Box';

import { theme } from '../../../theme';
import { RadioGroup } from './RadioGroup';

export default {
  title: 'Form/RadioGroup',
  component: RadioGroup,
};

interface FormValue {
  radioGroup: string | null;
  radioGroupWithLabel: string | null;
}

export const Default = (): React.ReactNode => {
  const onSubmit = (values: FormValue) => {};

  return (
    <StyledEngineProvider injectFirst>
      <ThemeProvider theme={theme}>
        <Formik initialValues={{ radioGroup: null, radioGroupWithLabel: null } as FormValue} onSubmit={onSubmit}>
          <Form>
            <Box>
              <Field
                name="radioGroup"
                component={RadioGroup}
                color="primary"
                options={[
                  { value: 'one', label: 'One' },
                  { value: 'two', label: 'Two' },
                ]}
              />
            </Box>

            <Box mt={2}>
              <Field
                name="radioGroupWithLabel"
                component={RadioGroup}
                label="With label"
                color="primary"
                options={[
                  { value: 'one', label: 'One' },
                  { value: 'two', label: 'Two' },
                  { value: 'three', label: 'Three' },
                  { value: 'four', label: 'Four' },
                ]}
              />
            </Box>
          </Form>
        </Formik>
      </ThemeProvider>
    </StyledEngineProvider>
  );
};
