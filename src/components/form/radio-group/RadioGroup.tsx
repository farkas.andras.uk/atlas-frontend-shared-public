import React, { FC, ReactNode } from 'react';
import { FieldProps } from 'formik';
import FormControl from '@mui/material/FormControl';
import FormLabel from '@mui/material/FormLabel';
import FormControlLabel from '@mui/material/FormControlLabel';
import Radio, { RadioProps } from '@mui/material/Radio';
import MuiRadioGroup, { RadioGroupProps as MuiMuiRadioGroupProps } from '@mui/material/RadioGroup';
import { ErrorMessage } from '../../util/ErrorMessage';
import { sx_config } from './RadioGroup.style';

export interface RadioOptionModel {
  value: string;
  label: ReactNode | string;
  checked?: boolean;
  color?: 'primary' | 'secondary' | 'default';
}

export interface RadioGroupProps extends MuiMuiRadioGroupProps, Partial<FieldProps> {
  label?: ReactNode;
  showError?: boolean;
  options: RadioOptionModel[];
  theme?: 'dark' | 'light';
  radioProps?: RadioProps;
  checked?: boolean;
}

export const RadioGroup: FC<RadioGroupProps> = ({
  label,
  form,
  field,
  name,
  value,
  onChange,
  onBlur,
  showError,
  options,
  radioProps,
  theme = 'dark',
  ...props
}) => {
  const fieldName = field?.name || name;
  const fieldValue = field?.value || value || '';

  return (
    <FormControl component="fieldset">
      {label ? <FormLabel component="legend">{label}</FormLabel> : null}
      <MuiRadioGroup
        name={fieldName}
        value={fieldValue}
        onChange={field?.onChange || onChange}
        onBlur={field?.onBlur || onBlur}
        {...props}
        sx={[props.row && sx_config.formGroupRow, props.className && { ...(props.className as React.CSSProperties) }]}
      >
        {options &&
          options.map(({ value, label, checked, color }) => (
            <FormControlLabel
              sx={[theme && sx_config[theme], props.row && sx_config.row]}
              key={value}
              value={value}
              control={<Radio color={color} {...radioProps} />}
              label={label}
              checked={checked ?? fieldValue === value}
            />
          ))}
      </MuiRadioGroup>
      {showError ? <ErrorMessage form={form} field={field} /> : null}
    </FormControl>
  );
};

RadioGroup.defaultProps = {
  showError: false,
};
