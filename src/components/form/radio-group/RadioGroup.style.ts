import { Theme } from '@mui/material/styles';

export const sx_config = {
  formGroupRow: {
    flexWrap: 'nowrap',
  },
  dark: (theme: Theme) => ({
    '& .MuiRadio-root': {
      color: theme.palette.common.black,
    },
    '& .MuiRadio-root.Mui-checked:not(.MuiRadio-colorSecondary)': {
      color: theme.palette.primary.main,
    },
    '& .MuiRadio-root.Mui-checked.MuiRadio-colorSecondary': {
      color: theme.palette.secondary.main,
    },
  }),
  light: {
    '& .MuiRadio-root': {
      color: (theme: Theme) => theme.palette.common.white,
    },
    '& .MuiRadio-root.Mui-checked': {
      color: (theme: Theme) => theme.palette.common.white,
    },
  },
  label: {
    color: (theme: Theme) => theme.palette.common.black43,
  },
  row: {
    margin: 0,
  },
};
