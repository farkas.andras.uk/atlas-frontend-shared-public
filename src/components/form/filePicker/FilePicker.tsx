import React, { FC, ReactNode, useState } from 'react';
import { FieldProps } from 'formik';
import Dropzone, { DropEvent, DropzoneProps, FileRejection } from 'react-dropzone';
import { FileErrorLabelMap, FileType } from '../file/FileUpload';
import Box from '@mui/material/Box';
import Typography from '@mui/material/Typography';
import FormHelperText from '@mui/material/FormHelperText';
import CircularProgress from '@mui/material/CircularProgress';
import InputLabel from '@mui/material/InputLabel';
import Button from '@mui/material/Button';
import VisibilityIcon from '@mui/icons-material/Visibility';
import AddIcon from '@mui/icons-material/Add';
import ClearIcon from '@mui/icons-material/Clear';
import { ErrorMessage } from '../../util/ErrorMessage';
import { ImagePreview } from '../../image-preview/ImagePreview';
import { sx_config } from './FilePicker.style';

enum FilePickerError {
  UNSUPPORTED_FILE_TYPE = 'UNSUPPORTED_FILE_TYPE',
}

export interface FilePickerProps extends DropzoneProps, Partial<FieldProps> {
  label?: ReactNode;
  showError?: boolean;
  dropInformation?: ReactNode | string;
  accept?: any;
  baseImageUrl?: string;
  fileName?: string;
  downloadFile: () => void;
  uploadFile: (files: File[], event: DropEvent) => Promise<void>;
  deleteFile: () => Promise<void>;
}

export const FilePicker: FC<FilePickerProps> = ({
  multiple = false,
  onDropRejected,
  form,
  field,
  label,
  showError = true,
  dropInformation,
  accept = FileType.FILE,
  baseImageUrl = '',
  fileName,
  downloadFile,
  uploadFile,
  deleteFile,
  ...props
}) => {
  const [error, setError] = useState<FilePickerError | null>(null);
  const [imagePreviews, setImagePreviews] = useState<string[]>([]);
  const [imagePreviewsLoading, setImagePreviewsLoading] = useState<boolean>(false);
  const [isUploading, setIsUploading] = useState(false);
  const [isDeleteLoading, setIsDeleteLoading] = useState(false);

  const getPreview = (file: File): Promise<string> => {
    return new Promise((resolve) => {
      const reader = new FileReader();
      reader.readAsDataURL(file);
      reader.onloadend = () => {
        resolve(reader.result as string);
      };
    });
  };

  const getAllPreviews = (files: File[]): Promise<string[]> => {
    const allPreviews: string[] = [];

    return new Promise((resolve) => {
      files.forEach(async (file) => {
        const preview = await getPreview(file);
        allPreviews.push(preview);

        if (files.length === allPreviews.length) {
          resolve(allPreviews);
        }
      });
    });
  };

  const setFilesPreview = async (files: File[]) => {
    setImagePreviewsLoading(true);
    const allPreviews = await getAllPreviews(files);

    setImagePreviewsLoading(false);
    setImagePreviews(allPreviews);
  };

  const onRejected = (files: FileRejection[], event: DropEvent) => {
    setError(FilePickerError.UNSUPPORTED_FILE_TYPE);

    if (onDropRejected) {
      onDropRejected(files, event);
    }
  };

  const onAccepted = async (files: File[], event: DropEvent) => {
    setIsUploading(true);
    setError(null);
    setFilesPreview(files);

    if (uploadFile) {
      await uploadFile(files, event);
    }

    if (form && field) {
      form.setFieldValue(field.name, files);
    }
    setIsUploading(false);
  };

  const delFile = async () => {
    setIsDeleteLoading(true);
    await deleteFile();
    form.setFieldValue(field.name, undefined);
    setIsDeleteLoading(false);
  };

  const getValue = () => {
    if (!field?.value) {
      return null;
    }

    const values = Array.isArray(field.value) ? field.value : [field.value];

    if (accept === FileType.IMAGE && !!values.length && !imagePreviews.length) {
      setImagePreviews(values.map((value) => `${baseImageUrl}/${(value && value.name) || ''}`));
    }

    return values;
  };

  const value = getValue();

  const images = imagePreviews.map((src, index) => {
    const alt = (value && value[index] && value[index].name) || '';
    return { src, alt };
  });

  return (
    <div>
      {label ? (
        <InputLabel sx={sx_config.label} shrink>
          {label}
        </InputLabel>
      ) : null}
      <Dropzone multiple={multiple} onDropRejected={onRejected} onDropAccepted={onAccepted} accept={accept} {...props}>
        {({ getRootProps, getInputProps }) => {
          return (
            <Box
              display={multiple ? 'block' : 'flex'}
              alignItems={'center'}
              sx={{
                minHeight: '42px',
                borderRadius: '0',
                borderWidth: '0px',
                borderBottom: '1px solid rgba(0, 0, 0, 0.42)',
              }}
            >
              <Box sx={{ width: '100%' }}>
                {value && accept === FileType.FILE
                  ? value.map((val: File) =>
                      val.type ? (
                        <Typography key={val.name}>{val.name}</Typography>
                      ) : (
                        <a
                          key={val.name}
                          target="_blank"
                          rel="noopener noreferrer"
                          href={`${baseImageUrl}/${(val && val.name) || ''}`}
                        >
                          <Typography>{val.name}</Typography>
                        </a>
                      )
                    )
                  : fileName}
              </Box>

              <Button
                color="primary"
                sx={sx_config.icon}
                disabled={!fileName || fileName === '' || isUploading || isDeleteLoading}
                onClick={downloadFile}
              >
                <VisibilityIcon />
              </Button>
              <Box
                {...getRootProps()}
                sx={[sx_config.root, props.disabled ? sx_config.disabled : sx_config.enabled]}
                /* className={classNames(classes.root, {
                  active: isDragActive,
                  isUploading,
                  disabled: props.disabled,
                })} */
              >
                {isUploading ? (
                  <CircularProgress size={22} />
                ) : (
                  <Button sx={sx_config.icon} disabled={props.disabled}>
                    <input {...getInputProps()} />
                    {dropInformation}
                    <AddIcon />
                  </Button>
                )}
              </Box>
              {accept === FileType.IMAGE ? (
                <ImagePreview images={images} multiple={multiple} loading={imagePreviewsLoading} />
              ) : null}
              {isDeleteLoading ? (
                <CircularProgress size={22} />
              ) : (
                <Button
                  color="primary"
                  sx={sx_config.icon}
                  disabled={!fileName || fileName === '' || isDeleteLoading || props.disabled}
                  onClick={delFile}
                >
                  <ClearIcon />
                </Button>
              )}
            </Box>
          );
        }}
      </Dropzone>
      {showError ? <ErrorMessage form={form} field={field} /> : null}
      <FormHelperText error>{error && FileErrorLabelMap[error] ? FileErrorLabelMap[error]() : null}</FormHelperText>
    </div>
  );
};
