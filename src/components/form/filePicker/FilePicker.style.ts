import { Theme } from '@mui/material/styles';

export const sx_config = {
  root: (theme: Theme) => ({
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    width: '43px',
    transition: theme.transitions.create(['border', 'background-color'], {
      duration: theme.transitions.duration.complex,
    }),
    cursor: 'pointer',
    textAlign: 'center',
    '&:focus': {
      outline: 'none',
    },
    '&.active.accept': {
      backgroundColor: 'red',
      '& .drop': {
        display: 'block',
      },
    },
    '&.active.reject': {
      borderColor: theme.palette.error.main,
      '& .reject': {
        display: 'block',
      },
    },
    '@keyframes float': {
      '0%': {
        transform: 'translateY(0px)',
      },
      '50%': {
        transform: 'translateY(-4px)',
      },
      '100%': {
        transform: 'translateY(0px)',
      },
    },
  }),
  disabled: {
    cursor: 'default',
  },
  enabled: {
    '&:hover': {
      borderColor: (theme: Theme) => theme.palette.primary.main,
    },
  },
  label: {
    marginBottom: (theme: Theme) => theme.spacing(1),
  },
  spinner: {
    padding: '5px',
  },
  icon: {
    color: (theme: Theme) => theme.palette.primary.main,
    borderRadius: '30px',
    minWidth: '1px',
    maxWidth: '40px',
    '&:hover': {
      animationName: 'float',
      animationDuration: '1s',
      animationTimingFunction: 'ease-out',
      animationFillMode: 'forwards',
      animationIterationCount: 'infinite',
      cursor: 'pointer',
      borderColor: (theme: Theme) => theme.palette.primary.main,
    },
  },
};
