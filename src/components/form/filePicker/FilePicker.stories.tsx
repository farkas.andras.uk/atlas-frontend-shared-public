import React from 'react';
import { Meta, Story } from '@storybook/react/types-6-0';

import { FilePicker, FilePickerProps } from './FilePicker';
import { FileType } from '../file/FileUpload';

export default {
  title: 'Form/FilePicker',
  component: FilePicker,
} as Meta;

export const Interactive: Story<FilePickerProps> = (args) => <FilePicker {...args} />;

Interactive.args = {
  fileName: '',
  accept: FileType.DOCUMENT,
};
