import React from 'react';
import { Meta, Story } from '@storybook/react/types-6-0';
import { Field, Form, Formik } from 'formik';

import { TextField, TextFieldProps } from './TextField';

export default {
  title: 'Form/TextField',
  component: TextField,
} as Meta;

export const Interactive: Story<TextFieldProps> = (args) => (
  <Formik initialValues={{}} onSubmit={undefined}>
    <Form>
      <Field
        name="text"
        label="Foo"
        component={(p) => {
          return <TextField {...p} tooltipProps={{ title: 'TextField tooltip' }} />;
        }}
        {...args}
      />
    </Form>
  </Formik>
);
