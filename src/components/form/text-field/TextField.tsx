import React, { FC, ReactNode, ChangeEvent, useMemo } from 'react';
import { FieldProps } from 'formik';
import { Tooltip, TooltipProps } from '../../tooltip/Tooltip';
import MuiTextField, { TextFieldProps as MuiTextFieldProps } from '@mui/material/TextField';
import Box from '@mui/material/Box';
import { ErrorMessage } from '../../util';
import { inErrorState } from '../../../utils';
import { sx_config } from './TextField.style';

interface PartialTextFieldProps extends FieldProps {
  showError?: boolean;
  readOnly?: boolean;
  disabledIcon?: ReactNode;
  onChanged?: (value: string) => void;
  isProtected?: boolean;
  protectedIcon?: ReactNode;
  disabled?: boolean;
  tooltipProps?: TooltipProps;
}

export type TextFieldProps = PartialTextFieldProps & MuiTextFieldProps;

export const TextField: FC<TextFieldProps> = ({
  field,
  form,
  value,
  variant,
  showError,
  readOnly,
  margin,
  onChanged,
  disabled,
  disabledIcon,
  isProtected,
  protectedIcon,
  tooltipProps,
  ...props
}) => {
  const inputVariant = variant;
  const inputMargin = margin;

  const onChange = (event: ChangeEvent<HTMLInputElement>) => {
    if (form && field) {
      form.setFieldValue(field.name, event.target.value);
    }

    if (onChanged) {
      onChanged(event.target.value);
    }
  };

  const endAdornment = useMemo(() => {
    if (props.InputProps?.endAdornment) {
      return props.InputProps.endAdornment;
    }

    if (isProtected) {
      return protectedIcon;
    }

    if (disabled) {
      return disabledIcon;
    }
  }, [isProtected, protectedIcon, disabled, disabledIcon, props.InputProps?.endAdornment]);

  const component = (
    <Box sx={[sx_config.root, props.fullWidth && sx_config.fullWidth, isProtected && sx_config.protected]}>
      <MuiTextField
        {...field}
        {...props}
        onChange={onChange}
        margin={inputMargin}
        value={value || field?.value || ''}
        variant={inputVariant}
        error={inErrorState(form, field)}
        disabled={disabled || isProtected}
        InputProps={{
          ...(props.InputProps || {}),
          readOnly: (props.InputProps || {}).readOnly || readOnly || false,
          endAdornment: endAdornment,
        }}
      />
      {showError ? <ErrorMessage variant={inputVariant} form={form} field={field} /> : null}
    </Box>
  );

  return tooltipProps ? <Tooltip {...tooltipProps}>{component}</Tooltip> : <>{component}</>;
};

TextField.defaultProps = {
  showError: true,
  fullWidth: true,
};
