import { Theme } from '@mui/material/styles';

export const sx_config = {
  fullWidth: {
    width: '100%',
  },
  root: {
    '& .Mui-disabled .MuiSelect-icon': {
      display: 'none',
    },
    '& label': {
      backgroundColor: 'rgba(255,255,255, 0.0)',
    },
    '& .MuiOutlinedInput-root': {
      '& fieldset': {
        borderRadius: '0',
        borderWidth: '0px',
        borderBottom: '1px solid rgba(0, 0, 0, 0.42)',
      },
      '&.Mui-focused fieldset': {
        borderWidth: '0px',
        borderBottom: (theme: Theme) => `1px solid ${theme.palette.primary.main}`,
      },
    },
  },
  protected: {
    '& .MuiInputBase-root.Mui-disabled ': {
      color: 'inherit',
    },
  },
};
