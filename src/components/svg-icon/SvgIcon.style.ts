import { Theme } from '@mui/material/styles';

export const sx_config = {
  root: {
    userSelect: 'none',
    width: '1em',
    height: '1em',
    display: 'inline-block',

    flexShrink: 0,
    fontSize: (theme: Theme) => theme.typography.pxToRem(18),
    transition: (theme: Theme) =>
      theme.transitions.create('fill', {
        duration: theme.transitions.duration.shorter,
      }),
  },
  fillCurrentColor: {
    fill: 'currentColor',
    '& *': {
      fill: 'currentColor !important',
    },
  },
  strokeCurrentColor: {
    '& *': {
      stroke: 'currentColor !important',
    },
  },
  primary: {
    color: (theme: Theme) => theme.palette.primary.main,
  },
  secondary: {
    color: (theme: Theme) => theme.palette.secondary.main,
  },
  error: {
    color: (theme: Theme) => theme.palette.error.main,
  },
  black: {
    color: (theme: Theme) => theme.palette.common.black,
  },
  inherit: {
    color: 'inherit',
    fontSize: 'inherit',
  },
  small: {
    fontSize: (theme: Theme) => theme.typography.pxToRem(23),
  },
  medium: {
    fontSize: (theme: Theme) => theme.typography.pxToRem(34),
  },
  large: {
    fontSize: (theme: Theme) => theme.typography.pxToRem(40),
  },
  xLarge: {
    fontSize: (theme: Theme) => theme.typography.pxToRem(120),
  },
  xxLarge: {
    fontSize: (theme: Theme) => theme.typography.pxToRem(220),
  },
};
