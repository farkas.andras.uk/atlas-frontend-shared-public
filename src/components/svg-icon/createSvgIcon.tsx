import React, { ComponentType } from 'react';
import { Coloring, SvgIcon, SvgIconComponentProps } from './SvgIcon';

export const createSvgIcon = (icon: ComponentType, coloring?: Coloring) => {
  return React.forwardRef((props: SvgIconComponentProps, ref) => {
    return <SvgIcon component={icon} coloring={coloring} {...props} ref={ref} />;
  });
};
