import React, { ComponentType } from 'react';
import { sx_config } from './SvgIcon.style';

export type Coloring = 'fill' | 'stroke';

export interface SvgIconComponentProps {
  className?: string;
  useOriginalColors?: boolean;
  coloring?: Coloring;
  fontSize?: 'small' | 'medium' | 'large' | 'xLarge' | 'xxLarge' | 'inherit';
  color?: 'primary' | 'override' | 'secondary' | 'inherit' | 'error' | 'black';
}

export interface SvgIconProps extends SvgIconComponentProps {
  component: ComponentType<any>;
}

export const SvgIcon = React.forwardRef(
  (
    {
      component: Component,
      className,
      useOriginalColors,
      fontSize = 'small',
      color = 'inherit',
      coloring = 'fill',
      ...props
    }: SvgIconProps,
    ref
  ) => {
    return (
      <Component
        focusable="false"
        sx={{
          ...sx_config.root,
          ...(!useOriginalColors && coloring === 'fill' && { ...sx_config.fillCurrentColor }),
          ...(useOriginalColors && coloring === 'stroke' && { ...sx_config.strokeCurrentColor }),
          ...(className && { ...(className as React.CSSProperties) }),
          ...sx_config[color],
          ...sx_config[fontSize],
        }}
        {...props}
        ref={ref}
      />
    );
  }
);
