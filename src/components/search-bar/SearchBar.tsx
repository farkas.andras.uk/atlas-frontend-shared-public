import React, { Component, ChangeEventHandler } from 'react';
import debounce from 'lodash/debounce';
import TextField, { TextFieldProps } from '@mui/material/TextField';
import { sx_config } from './SearchBar.style';

import { ClearFieldAdornment, ClearFieldAdornmentIconProps } from './ClearFieldAdornment';
import { SearchConditions } from '../table/TableQueryParams';
import { compose, getFilterValue } from '../../utils';

interface ISearchBarProps /* extends WithStyles<typeof styles> */ {
  name: string;
  onSearch: (searchConditions: SearchConditions) => void;
  initialValue?: string[] | null;
  delay?: number;
  clearFieldAdornmentIconProps: ClearFieldAdornmentIconProps;
}

export interface SearchBarState {
  value: string;
}

export type SearchBarProps = ISearchBarProps & TextFieldProps;

class SearchBar extends Component<SearchBarProps, SearchBarState> {
  readonly state: SearchBarState = {
    value: getFilterValue(this.props.initialValue) || '',
  };

  handleChange: ChangeEventHandler<HTMLInputElement> = (e) => {
    this.setState({ value: e.target.value }, () => {
      this.onSearch();
    });
  };

  handleSearch() {
    this.props.onSearch({ name: this.props.name, value: this.state.value });
  }

  onSearch = debounce(() => {
    this.handleSearch();
  }, this.props.delay || 500);

  onClear = () => {
    this.setState({ value: '' }, () => {
      this.onSearch();
    });
  };

  onIconClick = () => {
    if (this.state.value) {
      this.onClear();
    }
  };

  render() {
    const { classes, onChange, onSearch, initialValue, delay, variant, clearFieldAdornmentIconProps, ...props } =
      this.props;
    const { value } = this.state;

    return (
      <TextField
        sx={sx_config.searchBar}
        onChange={this.handleChange}
        value={value}
        variant={variant}
        fullWidth
        InputProps={{
          endAdornment: (
            <ClearFieldAdornment
              onClick={this.onIconClick}
              value={value}
              showSearchIcon
              {...clearFieldAdornmentIconProps}
            />
          ),
        }}
        {...props}
      />
    );
  }
}

export default compose()(SearchBar);
