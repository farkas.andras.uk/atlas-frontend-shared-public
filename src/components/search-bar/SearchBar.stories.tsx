import React, { useState } from 'react';
import { Router } from 'react-router-dom';
import { createBrowserHistory } from 'history';

import { ThemeProvider, StyledEngineProvider } from '@mui/material/styles';
import Search from '@mui/icons-material/Search';

import { theme } from '../../theme';

import SearchBar from './SearchBar';

const history = createBrowserHistory();

export default {
  title: 'SearchBar',
  component: SearchBar,
};

export const Default = (): React.ReactNode => {
  const [searchText, setSearchText] = useState('');

  return (
    <Router history={history}>
      <StyledEngineProvider injectFirst>
        <ThemeProvider theme={theme}>
          <SearchBar
            name="searchText"
            onSearch={setSearchText}
            initialValue={searchText}
            clearFieldAdornmentIconProps={{ searchIcon: <Search /> }}
          />
        </ThemeProvider>
      </StyledEngineProvider>
    </Router>
  );
};
