export const sx_config = {
  icon: {
    cursor: 'pointer',
  },
  focus: {
    pointerEvents: 'none',
  },
};
