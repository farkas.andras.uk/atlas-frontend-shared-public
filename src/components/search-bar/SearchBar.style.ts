import { Theme } from '@mui/material/styles';

export const sx_config = {
  searchBar: (theme: Theme) => ({
    background: theme.palette.background.paper,
    maxWidth: 600,
    '& .MuiFormLabel-root': {
      fontWeight: 300,
      backgroundColor: 'rgba(0, 0, 0, 0)',
      paddingLeft: '0!important',
      paddingRight: '0!important',
      top: -5,
      left: -10,
    },
    '& label.Mui-focused': {
      left: -10,
    },
    '& .MuiOutlinedInput-root': {
      '& fieldset': {
        borderRadius: '0',
        borderWidth: '0px',
        borderBottom: '1px solid rgba(0, 0, 0, 0.42)',
      },
      '&.Mui-focused fieldset': {
        borderWidth: '0px',
        borderBottom: `2px solid ${theme.palette.primary.main}`,
      },
    },
  }),
};
