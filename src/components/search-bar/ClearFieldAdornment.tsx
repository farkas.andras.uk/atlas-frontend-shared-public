import React, { MouseEventHandler, ReactElement } from 'react';
import InputAdornment from '@mui/material/InputAdornment';
import ClearIcon from '@mui/icons-material/Clear';
import { sx_config } from './ClearFieldAdornment.style';

export interface ClearFieldAdornmentIconProps {
  searchIcon: ReactElement;
  overrideIconStyle?: boolean;
}

export interface ClearFieldAdornmentProps extends ClearFieldAdornmentIconProps {
  value: string;
  onClick: MouseEventHandler;
  showSearchIcon?: boolean;
}

export const ClearFieldAdornment = ({
  value,
  onClick,
  showSearchIcon,
  searchIcon,
  overrideIconStyle,
}: ClearFieldAdornmentProps) => {
  return (
    <InputAdornment
      position="end"
      onClick={onClick}
      sx={{
        ...(!value && { ...sx_config.focus }),
      }}
    >
      {value ? <ClearIcon color="primary" sx={sx_config.icon} /> : null}
      {!value && showSearchIcon
        ? React.cloneElement(
            searchIcon,
            !overrideIconStyle ? { color: 'primary', style: { ...sx_config.icon }, useOriginalColors: true } : null
          )
        : null}
    </InputAdornment>
  );
};
