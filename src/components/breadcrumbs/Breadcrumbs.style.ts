import { Theme } from '@mui/material/styles';

export const sx_config = {
  linkContainer: (theme: Theme) => ({
    display: 'flex',
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    paddingLeft: theme.mixins.breadcrumbs.paddingLeft,
    paddingRight: theme.mixins.breadcrumbs.paddingRight,
  }),
  linkIcon: (theme: Theme) => ({
    fontSize: 16,
    color: theme.mixins.breadcrumbs.iconColor,
    marginRight: theme.spacing(1),
  }),
  otherLink: {
    display: 'flex',
    height: (theme: Theme) => theme.mixins.breadcrumbs.height,
    color: 'rgba(0, 0, 0, 0.54)',
    textDecoration: 'none',
    cursor: 'pointer',
    userSelect: 'none',
  },
  currentPage: {
    color: 'rgba(0, 0, 0, 0.87)',
  },
  separator: (theme: Theme) => ({
    height: theme.mixins.breadcrumbs.height,
    alignItems: 'center',
    '& .MuiBreadcrumbs-ol': {
      height: theme.mixins.breadcrumbs.height,
    },
    '& .MuiBreadcrumbs-separator': {
      margin: 0,
    },
  }),
};
