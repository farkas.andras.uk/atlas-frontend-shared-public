import React from 'react';
import { KeyboardArrowRight, Home } from '@mui/icons-material';

import { Breadcrumbs } from './Breadcrumbs';

export default {
  title: 'Breadcrumbs',
  component: Breadcrumbs,
};

export const Default = (): React.ReactNode => (
  <Breadcrumbs
    homeLabel="Home"
    icon={<KeyboardArrowRight sx={{ fontSize: 18, color: 'red' }} />}
    routes={[
      {
        path: '/',
        title: () => 'Users',
        icon: Home,
        breadcrumbs: [{ title: () => 'Partners', path: '/partners' }, { title: () => 'Users' }],
      },
    ]}
  />
);
