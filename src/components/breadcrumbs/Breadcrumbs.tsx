import React, { useMemo, ReactNode } from 'react';
import { Link, useRouteMatch } from 'react-router-dom';
import { Home } from '@mui/icons-material';
import { Box, Breadcrumbs as MuiBreadcrumbs } from '@mui/material';
import { v4 as uuidv4 } from 'uuid';
import { Typography } from '../typography/Typography';
import { RouteConfigurationModel, RouteChildrenConfigurationModel } from '../../models';
import { sx_config } from './Breadcrumbs.style';
import './Breadcrumbs.css';

export interface BreadcrumbsProps {
  homeLabel: ReactNode | string;
  routes: RouteConfigurationModel[];
  icon?: ReactNode;
  currentLabel?: ReactNode | string;
}

interface IsLinkMatchModel {
  matchUrl: string;
  link?: string;
  partialIncludeToMatch?: boolean;
  children?: RouteChildrenConfigurationModel[];
}

interface IsPathMatchModel {
  matchPath: string;
  path?: string;
  children?: RouteChildrenConfigurationModel[];
}

interface IsMatchModel extends IsLinkMatchModel, IsPathMatchModel {}

export const isLinkPartialMatch = ({ matchUrl, link }: IsLinkMatchModel) => link && matchUrl.includes(link);

export const isLinkMatchEqual = ({ matchUrl, link }: IsLinkMatchModel) => link && link === matchUrl;

export const isPathMatch = ({ matchPath, path, children }: IsPathMatchModel): boolean =>
  path === matchPath ||
  (!!children && !!children.find((childrenRoute) => isPathMatch({ matchPath, path: childrenRoute.path })));

export const isLinkMatch = ({ matchUrl, link, partialIncludeToMatch, children }: IsLinkMatchModel) =>
  partialIncludeToMatch
    ? isLinkPartialMatch({ matchUrl, link }) ||
      (children && children.find((childrenRoute) => isLinkPartialMatch({ matchUrl, link: childrenRoute.link })))
    : isLinkMatchEqual({ matchUrl, link }) ||
      (children && children.find((childrenRoute) => isLinkMatchEqual({ matchUrl, link: childrenRoute.link })));

export const isMatch = ({ matchUrl, matchPath, link, path, partialIncludeToMatch, children }: IsMatchModel) =>
  link ? isLinkMatch({ matchUrl, link, partialIncludeToMatch, children }) : isPathMatch({ matchPath, path, children });

export const Breadcrumbs = ({ homeLabel, routes, icon, currentLabel }: BreadcrumbsProps) => {
  const match = useRouteMatch();

  const route = useMemo(() => {
    let foundRoute = routes.find(({ path, partialIncludeToMatch, link, children }) => {
      return isMatch({ matchUrl: match.url, link, partialIncludeToMatch, children, matchPath: match.path, path });
    });

    if (
      foundRoute &&
      !isMatch({
        matchUrl: match.url,
        link: foundRoute.link,
        partialIncludeToMatch: foundRoute.partialIncludeToMatch,
        matchPath: match.path,
        path: foundRoute.path,
      }) &&
      foundRoute.children
    ) {
      foundRoute = foundRoute.children.find((childrenRoute) => {
        return isMatch({
          matchUrl: match.url,
          link: childrenRoute.link,
          partialIncludeToMatch: childrenRoute.partialIncludeToMatch,
          matchPath: match.path,
          path: childrenRoute.path,
        });
      });
    }

    return foundRoute;
  }, [match.path, match.url, routes]);

  const RouteIcon = route && route.icon;

  return (
    <MuiBreadcrumbs separator={icon} aria-label="breadcrumb" sx={sx_config.separator}>
      <Link color="inherit" to="/" style={sx_config.otherLink}>
        <Box sx={sx_config.linkContainer}>
          <Home sx={sx_config.linkIcon} />
          <Typography color="inherit" variant="body1">
            {homeLabel}
          </Typography>
        </Box>
      </Link>
      {route &&
        route.breadcrumbs &&
        route.breadcrumbs.map(({ title, path, onClick }) => {
          const content = (
            <Box sx={sx_config.linkContainer}>
              {RouteIcon && <RouteIcon sx={sx_config.linkIcon} />}
              <Typography color="inherit" variant="body1">
                {title()}
              </Typography>
            </Box>
          );
          return (
            <Box key={path || uuidv4()} sx={[!path && sx_config.currentPage]}>
              {path ? (
                <Link color="inherit" to={path} style={sx_config.otherLink}>
                  {content}
                </Link>
              ) : onClick ? (
                <Box onClick={onClick} sx={sx_config.otherLink}>
                  {content}
                </Box>
              ) : (
                currentLabel || content
              )}
            </Box>
          );
        })}
    </MuiBreadcrumbs>
  );
};
